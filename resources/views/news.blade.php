@extends('layouts.main_layout')
@section('content')

  <div class="container">
  <div id="content_left">
    <div class="box_left">
        <div class="title_box_left">Danh mục tin tức</div>

        <div class="content_box_left list_cat_news">
            <ul class="ul">
                @foreach($list_cate as $name => $value)
                <li><a href="{{URL::route('category.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">{!!$name!!}</a></li>
                @endforeach
            </ul>
        </div>
    </div><!--box_left-->
    <div class="box_left">
        <div class="title_box_left">Tin xem nhiều nhất</div>
        <div class="content_box_left list_hot_news">
            <div class="bg_gradient_bottom_title"></div>
            <ul class="ul">
              @foreach($news_count as $index => $value)
                <li>
                    <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">
                        {!! Html::image('images/news/'.$value->thumbnail, $value->title , ['width' => '193px']) !!}
                        <span>{!!$value->title!!}</span>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div><!--box_left-->
    <div class="banner_left">

  </div>
</div><!--content_left-->
  <div id="content_news_page">

<script type="text/javascript">
    $(window).load(function(){
      $('#banner_pro_list ul').carouFredSel({
        auto: {
        play: true,
        pauseOnHover: true
        },
        prev: '#banner_pro_list .prev',
        next: '#banner_pro_list .next',
        'direction'   : 'left',
        mousewheel: true,
        scroll:2,
        items:2,
        swipe: {
        onMouse: true,
        onTouch: true
        }
      });

    });
  </script>




    @foreach($news_new as $index => $value)
      <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}" class="top_newspage" style="margin-left:0;">
        {!! Html::image('images/news/'.$value->thumbnail, $value->title) !!}
        <span>{!!$value->title!!}</span>
      </a><!--top_newspage-->
    @endforeach
    <div id="hot_news_home" class="float_r">
      <div class="title_box_right"><h2 class="cufon h_title">Tin tức nổi bật</h2> </div>
      <div class="content_box" style="height:255px;">
        <div class="bg_gradient_bottom_title"></div>

        @foreach($news_new as $index => $value)
          @if($index == 0)
            <div class="top_news">
                <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">{!! Html::image('images/news/'.$value->thumbnail, $value->title) !!}</a>
                <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}"> &raquo; {!!$value->title!!}</a>
            </div>
            @if(count($news_new) > 1)
              <ul>
            @endif
          @else
            <li><a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}"> &raquo; {!!$value->title!!}</a></li>
            @if($index == count($news_new))
              </ul>
            @endif
          @endif
        @endforeach
    </div>
  </div><!--hot_news_home-->


  <div class="clear space2"></div>
  @foreach($list_cate as $parent => $child)
    <div class="title_box_center">
        <h2 class="h_title cufon" style="font-size:22px !important;">{!!$parent!!}</h2>
        <a href="{{URL::route('category.news', ['alias'=>$child['alias'],'id'=>$child['id']])}}" class="view_cate_news">Xem tất cả <span>&raquo;</span></a>
    </div>
    <div class="space2"></div>
    <div class="top_news_on_page">
      <!-- {!!var_dump($child['child']['news'])!!} -->
         @foreach($child['child']['news'] as $index => $news)
         @if($index == 0)
           <div class="first">
           <a href="{{URL::route('details.news', ['alias'=>$news->alias,'id'=>$news->id])}}">
               {!! Html::image('images/news/'.$news->thumbnail, $news->title) !!}
           </a>
           <span class="container">
               <a href="{{URL::route('details.news', ['alias'=>$value->alias,'id'=>$value->id])}}" class="name">{!!$news->title!!}</a>
               <span class="view">{!!$news->created_at!!} - Lượt xem: {!!$news->counter!!}</span>
               <span class="summary">{!!$news->desc!!}</span>
           </span>
           </div>
           @if(count($child['child']['news']))
            <ul class="ul">
           @endif
         @else
               <li>
                   <a href="{{URL::route('details.news', ['alias'=>$news->alias,'id'=>$news->id])}}">{!! Html::image('images/news/'.$news->thumbnail, $news->title, ['width' => '100px']) !!}</a>
                   <div style="width:270px; float:right;">
                   <a href="{{URL::route('details.news', ['alias'=>$news->alias,'id'=>$news->id])}}" class="name">{!!$news->title!!}</a>
                   <span class="view">{!!$news->created_at!!} - Lượt xem: {!!$news->counter!!}</span>
                   </div>
               </li>
             @if($index == count($child['child']['news']))
                </ul>
             @endif

         @endif



          @endforeach
    </div><!--top-->

  <div class="clear space2"></div>
    @endforeach

</div><!--content_news-->

</div><!--container-->

<div class="clear"></div>
@endsection
