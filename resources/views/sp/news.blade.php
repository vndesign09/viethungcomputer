@extends('layouts.sp_main_layout')
@section('content')

<script> 
$(function(){
    $("#select_filter .title_box_right").click(function(){
      if($(this).parent().children(".content_box").is(":visible"))  $(".content_box").hide();
      else{
        $(".content_box").hide();
        $(this).parent().children(".content_box").show();
      }
    });
  })
</script>
<div class="clear"></div>
<div id="select_filter">
    <div class="box_right filter">
        <div class="title_box_right black" style="float:none; margin-right:0;">
            <h2>Chọn danh mục tin tức</h2><span class="icon_drop"></span>
        </div>
        <div class="content_box" style="display: none;">
          <div style="border:solid 1px #d00; padding:10px; line-height:20px;">
            <ul class="ul">
                
                @foreach($list_cate as $name => $value)
                <li><a href="{{URL::route('category.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">{!!$name!!}</a></li>
                @endforeach
                
            </ul>
          </div>
        </div>
    </div><!--box_right-->
</div><!--select_filter-->

<div class="m_pro_list">
  <ul class="ul">
    @foreach($list_news_sp as $index => $value)
    <li>
      <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}" class="p_img" style="height:auto; border:none;">{!! Html::image('images/news/'.$value->thumbnail, $value->title) !!}</a>
      <div class="p_right">
        <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}" class="news_name"><b>{!!$value->title!!}</b></a>
        <span class="news_time">{!!$value->created_at!!} - Lượt xem: {!!$value->counter!!}</span>
        <p class="news_summary"></p>
      </div>
    </li> 
    @endforeach
  </ul>
  <div class="clear"></div>
</div>
<div class="clear"></div>

@stop