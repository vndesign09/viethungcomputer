@extends('layouts.sp_main_layout')
@section('content')
<div id="location">
    <a href="/">Trang chủ</a>
    
    <span>»</span><a href="/laptop-pc-mtb/c159.html">{!!$cat->name!!}</a>
    
</div>
<h1 style="font-size:18px; margin-bottom:0px; color:#e00;">{!!$cat->name!!}</h1>
<div id="list_sub_category">
  @if(count($list_child) > 0)
  @foreach($list_child as $key=>$child)
     <a href="{!!URL::route('search', ['cat'=>$child['id']])!!}">{!!$child['name']!!}</a>
  @endforeach
  @endif
</div>
<script> 
$(function(){
    $("#select_filter .title_box_right").click(function(){
      if($(this).parent().children(".content_box").is(":visible"))  $(".content_box").hide();
      else{
        $(".content_box").hide();
        $(this).parent().children(".content_box").show();
      }
    });
  })
</script>
<div class="clear"></div>
<div class="action_pro_list">
    <select onchange="window.location=value">

            <option value="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'new'])!!}">Mới nhất</option>
          
            <option value="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'price-asc'])!!}">Giá: thấp -> cao</option>
          
            <option value="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'price-desc'])!!}">Giá: cao -> thấp</option>
          
            <option value="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'view'])!!}">Xem nhiều nhất</option>
          
    </select>
</div>

<div class="product_list">
<ul class="ul">
    @foreach($list_products as $key => $product)  
      <li>
        <div class="p_container">
            <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_img">
              <span class="p_sku">Mã SP: {!!$product->code!!}</span>
              {!!Html::image('images/products/'.$product->thumbnail, $product->name)!!}
            </a>
            <div class="p_info">
              <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
              <div class="p_price">{!!adddotNumber($product->main_price)!!}</div>
                <div class="container_old_price">
                    @if($product->save_money >0 )<div class="p_old_price">{!!adddotNumber($product->price)!!}</div>@endif
                    @if($product->sale >0 )<div class="price_off">-{!!$product->sale!!}%</div>@endif
                </div>
              <div class="clear"></div>
            </div>
        </div><!--wrap_pro-->
    </li>
    <div class="clear"></div>
    @endforeach
    </ul>
  
    <div class="paging">
      {!! $list_products->render() !!}
    </div><!--paging-->
</div>
@stop