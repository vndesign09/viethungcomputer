@extends('layouts.sp_main_layout')
@section('content')

<h1 style="font-size:18px; margin-bottom:0px; color:#e00;">TỪ KHÓA: {!!$keyword!!}</h1>
<script> 
$(function(){
    $("#select_filter .title_box_right").click(function(){
      if($(this).parent().children(".content_box").is(":visible"))  $(".content_box").hide();
      else{
        $(".content_box").hide();
        $(this).parent().children(".content_box").show();
      }
    });
  })
</script>
<div class="clear"></div>
@if(isset($cat))
<div class="action_pro_list">
    <?php 
        $base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
        $url = $base_url . $_SERVER["REQUEST_URI"];
        $url = str_replace(['&sort=new', '&sort=price-asc', '&sort=price-desc', '&sort=view'], '', $url);
    ?>
    <select onchange="window.location=value">
            <option value="{!! $url !!}&sort=new">Mới nhất</option>
          
            <option value="{!! $url !!}&sort=price-asc">Giá: thấp -> cao</option>
          
            <option value="{!! $url !!}&sort=price-desc">Giá: cao -> thấp</option>
          
            <option value="{!! $url !!}&sort=view">Xem nhiều nhất</option>
    </select>
</div>
@endif
<div class="product_list">
<ul class="ul">
    @foreach($list_products as $key => $product)  
      <li>
        <div class="p_container">
            <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_img">
              <span class="p_sku">Mã SP: {!!$product->code!!}</span>
              {!!Html::image('images/products/'.$product->thumbnail, $product->name)!!}
            </a>
            <div class="p_info">
              <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
              <div class="p_price">{!!adddotNumber($product->main_price)!!}</div>
                <div class="container_old_price">
                    @if($product->save_money >0 )<div class="p_old_price">{!!adddotNumber($product->price)!!}</div>@endif
                    @if($product->sale >0 )<div class="price_off">-{!!$product->sale!!}%</div>@endif
                </div>
              <div class="clear"></div>
            </div>
        </div><!--wrap_pro-->
    </li>
    <div class="clear"></div>
    @endforeach
    </ul>
  
    <div class="paging">
      {!! $list_products->appends(Request::except('page'))->links() !!}
    </div><!--paging-->
</div>
@stop