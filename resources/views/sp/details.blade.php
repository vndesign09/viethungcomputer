@extends('layouts.sp_main_layout')
@section('content')
<div id="m_pro_detail" style="margin-top:0">
<div class="fb-like" data-href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
  <h1 style="font-size:15px; margin-bottom:0;">{!! $product->name !!}</h1>
  <div class="clear"></div>
      <div class="fb-like" data-href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" data-layout="standard" data-action="like" data-show-faces="false" data-share="
  <div class="clear"></div>
  <p class="float_l red">Mã SP: {!! $product->code !!}</p>
  <div class="clear"></div>
  <div id="m_img_detail">
     <img src="{!!asset('images/products/'.$product->thumbnail.'')!!}" alt="{!! $product->name !!}">
  </div><!--m_img_detail-->
  <!--div id="social_top">
    <span class="so">
      <div class="fb-like" data-href="https://www.facebook.com/congtymaytinhhanoi" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
    </span>
    <script src="https://apis.google.com/js/platform.js" async defer>
      {lang: 'vi'}
    </script>
    <span class="so"><div class="g-plusone so" data-annotation="inline" data-width="120" data-size="medium"></div></span>
  </div-->
  <div class="clear"></div>
  <div id="m_overview">
    <table>
      <tbody><tr>
        <td width="100">
          <b>Kho hàng: </b>
        </td>
        <td>
          {!! $product->warehouse !!}
        </td>
      </tr>
      @if($product->brand_id > 0)
      <tr>
        <td><b>Hãng SX:</b></td>
        <td>
          {!!$product->brand->name!!}
        </td>
      </tr>
      @endif
      @if($product->guarantee_info != '')
      <tr>
        <td><b>Bảo hành:</b></td>
        <td>
          {!!$product->guarantee_info!!}
        </td>
      </tr>
      @endif
      <tr>
        <td><b>Lượt xem:</b></td><td> {!!$product->counter!!}  lượt</td>
      </tr>

      <tr>
        <td><b>Giao hàng:</b></td>
        <td>
          {!!$product->ship_info!!} 
        </td>
      </tr>
    </tbody></table>
  </div><!--overview-->

    <div id="price_deal_detail_2">
      <div class="img_price_full">{!!adddotNumber($product->main_price)!!} </div>
      <div class="clear"></div>

    </div><!--price_detail-->
  <script>
     $(document).ready(function(){
    	$(".m_title_tab").click(function(){
    		$(".m_title_tab").removeClass("current");
    		$(".m_title_tab span").text("+");
    		//$(this).addClass("current");
			$(this).children("span").text("-");

    		if($($(this).children("a").attr("href")).is(":visible")) $($(this).children("a").attr("href")).slideUp();
    		else $($(this).children("a").attr("href")).slideDown();
    	});
     });
  </script>
  <div id="tab_info_pro">


    <div class="m_title_tab"><a href="#tab3">Mô tả chi tiết</a> <span>+</span></div>
    <div class="m_content_tab" id="tab3" style="display: none;">
      {!!$product->desc_main!!} 
    </div>








  </div><!--tab_info_pro-->

    <h2 class="h2_title">Ý kiến người dùng</h2>
    <div class="ykien">
        <div class="fb-comments" data-href="{{URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])}}" data-width="100%" data-numposts="10" ></div>
    </div><!--ykien-->

    <h2 class="h2_title">Sản phẩm liên quan</h2>
    <div class="product_list">
          <ul class="ul">
            @if(count($related_product) >0 )
            @foreach($related_product as $key => $product)
            <li>
              <div class="p_container">
                <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_img">
                  <span class="p_sku">Mã SP: NUOC044</span>
                  <img src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}">
                </a>
                <div class="p_info">
                  <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
                  <div class="p_price">{!!adddotNumber($product->main_price)!!} đ</div>
                  <div class="container_old_price">
                      @if($product->sale > 0)
                        <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                        <div class="price_off">-{!!$product->sale!!}%</div>
                      @endif
                  </div>
                </div>
              </div>
            </li>
            @endforeach
            @endif
          </ul>
    </div><!--m_content_tab-->
  <div class="clear"></div>
</div>
@endsection