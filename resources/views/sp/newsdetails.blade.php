@extends('layouts.sp_main_layout')
@section('content')

<script> 
$(function(){
    $("#select_filter .title_box_right").click(function(){
      if($(this).parent().children(".content_box").is(":visible"))  $(".content_box").hide();
      else{
        $(".content_box").hide();
        $(this).parent().children(".content_box").show();
      }
    });
  })
</script>
<div class="clear"></div>
<div id="select_filter">
    <div class="box_right filter">
        <div class="title_box_right black" style="float:none; margin-right:0;">
            <h2>Chọn danh mục tin tức</h2><span class="icon_drop"></span>
        </div>
        <div class="content_box">
          <div style="border:solid 1px #d00; padding:10px; line-height:20px;">
            <ul class="ul">
                @foreach($categories_new as $key => $value)
                <li><a href="{{URL::route('category.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">{!!$value->name!!}</a></li>
                @endforeach
                
            </ul>
          </div>
        </div>
    </div><!--box_right-->
</div><!--select_filter-->
<div id="m_news_detail">
    <h1>{{$news->title}}</h1>
    <span class="news_time">{{$news->created_at}}</span>
    {!!$news->contents!!}
  <div class="clear"></div>
</div>

<div id="comment_news">
    <h2 class="h2_title">Nhận xét bài viết</h2>
    <div class="space2"></div>
    <div class="fb-comments" data-href="{{URL::route('details.news', ['alias'=>$news->alias, 'id'=>$news->id])}}" data-width="100%" data-numposts="10" ></div>
    <div class="space2"></div>
</div><!--comment_news-->

<div id="other_news">
  <div class="box_other_news">
    <h2 class="h2_title">Tin liên quan</h2>
    <ul class="ul">
      @foreach($related_news as $key => $new)
        <li>» <a href="{{URL::route('details.news', ['alias'=>$new->alias,'id'=>$new->id])}}">{{$new->title}}</a> </li>              
      @endforeach      
    </ul>
  </div><!--float_l-->
  <div class="box_other_news">
    <h2 class="h2_title">Tin mới cập nhật</h2>
    <ul class="ul">
      @foreach($new_news as $key => $new)
        <li>» <a href="{{URL::route('details.news', ['alias'=>$new->alias,'id'=>$new->id])}}">{{$new->title}}</a> </li>              
      @endforeach
    </ul>
  </div><!--float_r-->
</div><!--other_news-->
@stop