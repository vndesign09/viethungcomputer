@extends('layouts.sp_main_layout')
@section('content')

<script> 
$(function(){
    $("#select_filter .title_box_right").click(function(){
      if($(this).parent().children(".content_box").is(":visible"))  $(".content_box").hide();
      else{
        $(".content_box").hide();
        $(this).parent().children(".content_box").show();
      }
    });
  })
</script>
<div class="clear"></div>
<div id="select_filter">
    <div class="box_right filter">
        <div class="title_box_right black" style="float:none; margin-right:0;">
            <h2>Chọn danh mục tin tức</h2><span class="icon_drop"></span>
        </div>
        <div class="content_box">
          <div style="border:solid 1px #d00; padding:10px; line-height:20px;">
            <ul class="ul">
                @foreach($list_cate as $name => $value)
                <li><a href="{{URL::route('category.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">{!!$name!!}</a></li>
                @endforeach
                
            </ul>
          </div>
        </div>
    </div><!--box_right-->
</div><!--select_filter-->
<div id="m_news_detail">
    <h1>{{$page->title}}</h1>
    <div class="clear"></div>
    {!!$page->contents!!}
  <div class="clear"></div>
</div>

</div><!--other_news-->
@stop