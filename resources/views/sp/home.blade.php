@extends('layouts.sp_main_layout')
@section('content')

<div class="clear"></div>

<script>
    $(document).ready(function(){
        $(".title_tab a").click(function(){
            $(".title_tab a").removeClass("current");
            $(this).addClass("current");

            $(".mcf").fadeOut();
            $($(this).attr("href")).fadeIn();
            return false;
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){


      $('#tab1 ul').carouFredSel({
        width:"100%",
    	scroll:1,
    	prev: '.prev1',
        next: '.next1',
    	swipe: {
          onMouse: true,
          onTouch: true
          }
      });
  	  $('#tab2 ul').carouFredSel({
        width:"100%",
    	scroll:1,
    	prev: '.prev2',
        next: '.next2',
    	swipe: {
          onMouse: true,
          onTouch: true
          }
      });
  	  $('#tab3 ul').carouFredSel({
        width:"100%",
    	scroll:1,
    	prev: '.prev3',
        next: '.next3',
    	swipe: {
          onMouse: true,
          onTouch: true
          }
      });
  	  $('#tab4 ul').carouFredSel({
        width:"100%",
    	scroll:1,
    	prev: '.prev4',
        next: '.next4',
    	swipe: {
          onMouse: true,
          onTouch: true
          }
      });
    });
  </script>
<div id="box_pro_special_home">
<div class="title_tab">
    <a href="#tab1" class="a_tab current">Khuyến mãi</a>
    <a href="#tab2" class="a_tab">Bán chạy</a>
    <a href="#tab3" class="a_tab">Mới</a>
</div><!--title_tab-->
<div class="clear"></div>
<div class="content_tab" style="height:100px; overflow:hidden;">
<div class="product_list">

<div id="tab1" class="mcf current">
  <div class="btn_carousel_mobile prev prev1"></div>
	<div class="btn_carousel_mobile next next1"></div>
  <div class="caroufredsel_wrapper">
  <ul class="ul">
  @foreach($product_km as $key=> $product)
      <li class="nomar_l" style="margin-right: 5px;">
          <div class="p_container">
              <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_img"><img src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}"></a>
            	<div class="p_info">
                <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
                <div class="p_price">{!!adddotNumber($product->main_price)!!}</div>
                <div class="container_old_price">
                    @if($product->sale > 0)
                    <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                    <div class="price_off">-{!!$product->sale!!}%</div>
                    @endif
                </div>
            	</div>
          </div><!--wrap_pro-->
      </li>
  @endforeach
  </ul></div>
</div>
<div id="tab2" class="mcf">
 	 <div class="btn_carousel_mobile prev prev2" style="display: block;"></div>
	<div class="btn_carousel_mobile next next2" style="display: block;"></div>
<div class="caroufredsel_wrapper">
<ul class="ul">

@foreach($product_best_sale as $key=> $product)
    <li class="nomar_l" style="margin-right: 5px;">
        <div class="p_container">
            <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_img"><img src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}"></a>
            <div class="p_info">
              <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
              <div class="p_price">{!!adddotNumber($product->main_price)!!}</div>
              <div class="container_old_price">
                  @if($product->sale > 0)
                  <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                  <div class="price_off">-{!!$product->sale!!}%</div>
                  @endif
              </div>
            </div>
        </div><!--wrap_pro-->
    </li>
@endforeach
</ul></div>
  </div>
<div id="tab3" class="mcf">
  <div class="btn_carousel_mobile prev prev3" style="display: block;"></div>
	<div class="btn_carousel_mobile next next3" style="display: block;"></div>
<div class="caroufredsel_wrapper">
<ul class="ul">

@foreach($product_new as $key=> $product)
    <li class="nomar_l" style="margin-right: 5px;">
        <div class="p_container">
            <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_img"><img src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}"></a>
            <div class="p_info">
              <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
              <div class="p_price">{!!adddotNumber($product->main_price)!!}</div>
              <div class="container_old_price">
                  @if($product->sale > 0)
                  <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                  <div class="price_off">-{!!$product->sale!!}%</div>
                  @endif
              </div>
            </div>
        </div><!--wrap_pro-->
    </li>
@endforeach
</ul></div>
  </div>

</div><!--prouduct_list-->
</div><!--content_tab-->
</div><!--box_pro_special_home-->

<div class="clear space3"></div>
<div class="menu-home">
  <ul class="ul ul_menu">
     @foreach($categories as $key => $cat)
          <li>
            <a href="{!!URL::route('category.product', ['alias'=>$cat->alias, 'id'=>$cat->id])!!}" class="root">
              @if($cat->thumbnail != '')
              <img src="{!!URL::asset('images/categories/'.$cat->thumbnail)!!}" alt="{!!$cat->name!!}" style="max-width: 26px;">
              @else
              <img src="{!!URL::asset('images/categories/default.png')!!}" alt="{!!$cat->name!!}" style="max-width: 26px;">
              @endif
              <span>{!!$cat->name!!}</span>
            </a>
          </li>
        @endforeach
  </ul>
  <div class="clear"></div>
</div><!--menu-home-->

@foreach($list_cate as $parent => $child)
<?php $id = 'box_home'.$child['id']; ?>
<div class="box_pro_home" id="{!!$id!!}" style="position:relative;">
  <a class="title_box_home" href="{!!URL::route('category.product', ['alias'=> $child['alias'], 'id'=> $child['id']])!!}" style="display:block">{!!$parent!!}</a>
  <div class="product_list" style="height:110px; overflow:hidden; padding:10px 0;">
    <div class="btn_carousel_mobile prev" style="display: block;"></div>
	<div class="btn_carousel_mobile next" style="display: block;"></div>
    <div class="caroufredsel_wrapper">
    <ul class="ul">
    @foreach($child['child']['products'] as $product)
      <li style="margin-right: 2px;">
        <div class="p_container">
            <a href="{!!URL::route('details.product', ['alias'=> $product->alias, 'id'=>$product->id])!!}" class="p_img">
              <span class="p_sku">Mã SP: {!!$product->code!!}</span>
              <img src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}">
            </a>
            <div class="p_info">
              <a href="{!!URL::route('details.product', ['alias'=> $product->alias, 'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
              <div class="p_price">{!!adddotNumber($product->main_price)!!} đ</div>
              <div class="container_old_price">
                @if($product->sale > 0)
                  <div class="p_old_price">{!!adddotNumber($product->price)!!} đ</div>
                  <div class="price_off">-{!!$product->sale!!}%</div>
                @endif
              </div>
              <div class="clear"></div>
            </div>
        </div><!--wrap_pro-->
    </li>
    @endforeach
    </ul></div>
  </div>
</div><!--box_pro_home-->
<div class="clear"></div>

<script type="text/javascript">
    $(document).ready(function(){
      $("#{!!$id!!} ul").carouFredSel({
        auto: {
          play: false,
          timeoutDuration: 4000,
          pauseOnHover: true
        },
      width:"100%",
      scroll:1,
      prev: "#{!!$id!!} .prev",
        next: "#{!!$id!!} .next",
      swipe: {
          onMouse: true,
          onTouch: true
          }
      });
  });
</script>
@endforeach

@endsection

