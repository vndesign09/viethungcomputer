@extends('layouts.main_layout')

@section('content')

<div class="container"> 



<script type="text/javascript">

  $(window).load(function(){

    $('#banner_pro_list ul').carouFredSel({

      auto: {

      play: true,

      pauseOnHover: true

      },

      prev: '#banner_pro_list .prev',

      next: '#banner_pro_list .next',

      'direction'		: 'left',

      mousewheel: true,

      scroll:2,

  	  items:2,

      swipe: {

      onMouse: true,

      onTouch: true

      }

    });

  

  });

</script>



<div class="clear space"></div>



<div id="content_center">



<div class="clear space2"></div>

<script type="text/javascript">

  $(window).load(function(){

    $('#brand_list ul').carouFredSel({

      auto: {

      play: true,

      pauseOnHover: true

      },

      prev: '#brand_list .prev',

      next: '#brand_list .next',

      'direction'		: 'left',

      mousewheel: true,

      scroll:2,

      swipe: {

      onMouse: true,

      onTouch: true

      }

    });

  

  });

</script>



<input type="hidden" id="product_compare_list" value="" />

<div class="top_area_list_page">

    <h1>{!!$cat->name!!}</h1>

    

    <!-- <div class="sort_style">

        <span>Lựa chọn</span>

        <a href="?display=list" class="bg list_style "></a>

      	<a href="?display=grid" class="bg grid_style active"></a>

    </div> -->

  	<!--script>

  	$(document).ready(function(){

    	  current_url = $(location).attr('href');

      	  $(".sort_pro li a").each(function(){

      		href = $(this).attr("href");

      		if(current_url.indexOf("?") > 0){

      			$(this).attr("href","&"+href);

      		}else $(this).attr("href","?"+href);

      	  });

      });

  	</script-->

    <div class="sort_pro">

        <span>Sắp xếp sản phẩm <span class="bg icon_drop"></span> </span>

        <ul>

          

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'new'])!!}" rel="nofollow">Mới nhất</a> </li>

          

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'price-asc'])!!}" rel="nofollow">Giá: thấp -> cao</a> </li>

          

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'price-desc'])!!}" rel="nofollow">Giá: cao -> thấp</a> </li>

          

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'view'])!!}" rel="nofollow">Xem nhiều nhất</a> </li>



          

        </ul>

    </div>

    

        <div class="paging">

            {!! $list_products->render() !!}

        </div><!--paging-->

    

</div><!--top_area_list_page-->



<div class="clear"></div>



  <div class="product_list page_inside">

  @if(count($list_products) > 0)

<ul class="ul">

        @foreach($list_products as $key => $product)

        <li>  

            <!-- <div class="bg iconNew"></div>

          	<div class="icon-km"><span>KM</span></div> -->

            <div class="p_container">

                <div class="p_sku">Mã SP: {!!$product->code!!}</div>

                <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_img">{!!Html::image('images/products/'.$product->thumbnail, $product->name)!!}</a>

                <div class="p_price">@if(($product->main_price) == 1) 

                              Liên hệ 

                              @elseif($product->main_price > 1)

                                {!!adddotNumber($product->main_price)!!} 

                              @else

                                {!!adddotNumber($product->price)!!} 

                              @endif</div>

               <div class="container_old_price">

                    @if($product->sale > 0)

                    <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>

                    <div class="price_off">-{!!$product->sale!!}%</div>

                    @endif

                </div>

                <div class="clear"></div>

                <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>

                <div class="p_quantity">

                        <i class="bg icon_in_stock"></i>

                </div>

            </div><!--wrap_pro-->

            <div class="hover_content_pro tooltip">

            <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="hover_name">{!!$product->name!!}</a>

          	

            <div class="hori_line"></div>

            <table>

                <tr>

                    <td><b>Giá bán:</b></td>

                    <td>

                        <span class="img_price_full">@if(($product->main_price) == 1) 

                              Liên hệ 

                              @elseif($product->main_price > 1)

                                {!!adddotNumber($product->main_price)!!} 

                              @else

                                {!!adddotNumber($product->price)!!} 

                              @endif</span>

                    </td>

                </tr>

                @if($product->brand_id > 0)

                    <tr>

                      <td><b>Hãng sản xuất:</b></td>

                      <td>{!!$product->brand->name!!}</td>

                    </tr>

                    @endif

                @if($product->guarantee_info != '')<tr><td><b>Bảo hành:</b></td><td>{!!$product->guarantee_info!!}</td></tr>@endif

                <tr>

                  <td><b>Kho hàng:</b></td>

                  

                  

                  

                  <td>{!!$product->warehouse_info!!}</td>

                  

                  

                  

              	</tr>

            </table>

          	

            <div class="hori_line"></div>

            <div class="hover_offer">

                

                <b>Mô tả tóm tắt:</b><br/>

                

                {!!$product->desc_short!!}

                

            </div>

          	

        </div><!--hover_content_pro-->

        </li>

        @endforeach

</ul>

@else

  <h1>Đang cập nhật</h1>

@endif

</div><!--prouduct_list-->

  

<div class="clear"></div>

<div class="top_area_list_page">



    <!-- <div class="sort_style">

        <span>Lựa chọn</span>

        <a href="?display=list" class="bg list_style "></a>

      	<a href="?display=grid" class="bg grid_style active"></a>

    </div> -->

    <div class="sort_pro">

        <span>Sắp xếp sản phẩm <span class="bg icon_drop"></span> </span>

        <ul>

            

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'new'])!!}" rel="nofollow">Mới nhất</a> </li>

          

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'price-asc'])!!}" rel="nofollow">Giá: thấp -> cao</a> </li>

          

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'price-desc'])!!}" rel="nofollow">Giá: cao -> thấp</a> </li>

          

            <li><a href="{!!URL::route('category.product', [$cat->alias,$cat->id,'sort'=>'view'])!!}" rel="nofollow">Xem nhiều nhất</a> </li>



          

        </ul>

    </div>

        <div class="paging">

          {!! $list_products->render() !!}  

        </div><!--paging-->

    

</div><!--top_area_list_page-->

</div><!--content_center-->

<div id="content_right">

    <div class="box_right">

        <div class="title_box_right">

            <h2>{!!$cat->name!!}</h2>

        </div>

        <div class="content_box">

            <div class="bg_gradient_bottom_title"></div>

            <ul class="ul">

                @if(count($list_child) > 0)

                    @foreach($list_child as $key=>$child)

                      <li>&raquo; <a href="{!!URL::route('category.product', ['alias'=>$child['alias'],'cat'=>$child['id']])!!}">{!!$child['name']!!}</a> </li>

                    @endforeach

                @endif

            </ul>

        </div>

    </div><!--box_right-->

 

    

        <div class="box_right filter">

            <div class="title_box_right black">

                <h2>Khoảng giá</h2>

            </div>

            <div class="content_box">

                <div class="bg_gradient_bottom_title"></div>

                <ul class="ul">

                      @for($i=8; $i<= 16; $i++)

                          @if($i==8)

                          <li><input class="" type="checkbox"  onclick="location.href='{!!URL::route('search', ['cat'=>$cat->id, 'max'=> $i*1000000])!!}'"/> <a href="{!!URL::route('search', ['cat'=>$cat->id, 'max'=> $i*1000000])!!}">Dưới {!!$i!!} triệu</a></li>  

                          @elseif($i==16)

                          <li><input class="" type="checkbox"  onclick="location.href='{!!URL::route('search', ['cat'=>$cat->id, 'min'=> $i*1000000])!!}'"/> <a href="{!!URL::route('search', ['cat'=>$cat->id, 'min'=> $i*1000000])!!}">Trên {!!$i!!} triệu</a></li>  

                          @else

                          <li><input class="" type="checkbox"  onclick="location.href='{!!URL::route('search', ['cat'=>$cat->id, 'min'=>($i-1)*1000000,'max'=> $i*1000000])!!}'"/> <a href="{!!URL::route('search', ['cat'=>$cat->id, 'min'=>($i-1)*1000000,'max'=> $i*1000000])!!}">{!!($i-1)!!} triệu - {!!$i!!} triệu</a></li>  

                          @endif

                        @endfor

                </ul>

            </div>

        </div><!--box_right-->    

</div><!--content_right-->

</div>

<div class="clear"></div>

@stop