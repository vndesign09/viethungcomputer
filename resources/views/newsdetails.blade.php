@extends('layouts.main_layout')
@section('content')
<div class="container">
  <div id="content_left">
    <div class="box_left">
        <div class="title_box_left">Danh mục tin tức</div>
        <div class="content_box_left list_cat_news">
            <ul class="ul">
                @foreach($categories_new as $name => $value)
                <li><a href="{{URL::route('category.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">{!!$value->name!!}</a></li>
                @endforeach
            </ul>
        </div>
    </div><!--box_left-->
    <div class="box_left">
        <div class="title_box_left">Tin xem nhiều nhất</div>
        <div class="content_box_left list_hot_news">
            <div class="bg_gradient_bottom_title"></div>
            <ul class="ul">
                @foreach($news_count as $index => $value)
               <li>
                   <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">
                       {!! Html::image('images/news/'.$value->thumbnail, $value->title , ['width' => '193px']) !!}
                       <span>{!!$value->title!!}</span>
                   </a>
               </li>
              @endforeach
            </ul>
        </div>
    </div><!--box_left-->
  	<div class="banner_left">
    
  </div>
</div><!--content_left-->
<div id="content_news_page">
    <div id="detail_news">
        <h1>{{$news->title}}</h1>
        <div class="time">{{$news->created_at}}</div>
        <div class="content_detail">
        {!!$news->contents!!}
        </div>
      <div>  
    </div>
        <div class="clear"></div>
        <div id="share">
            <div class="float_l"><b class="float_l">Chia sẻ bài viết này: </b>
              <div class="float_l">
          		<!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style" style="margin-top:10px;">
                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                <a class="addthis_button_tweet"></a>
                <a class="addthis_counter addthis_pill_style"></a>
                </div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5148003b01a03b86"></script>
                <!-- AddThis Button END -->
              </div>
          	</div>
            <a href="http://www.freetellafriend.com/tell/?heading=Share+This+Article&bg=1&option=email&url={{URL::route('details.news', ['alias'=>$news->alias,'id'=>$news->id])}}" class="bg icon_send_mail float_r" target="_blank"></a>
            <a href="javascript:window.print();" target="_blank" class="bg icon_print float_r"></a>
            <div class="clear"></div>
            <div class="fb-comments" data-href="{{URL::route('details.news', ['alias'=>$news->alias, 'id'=>$news->id])}}" data-width="100%" data-numposts="10" ></div>
            <div class="clear"></div>
        </div><!--share-->
      
        <div id="other_news">
            <div class="box_other_news">
                <h2 class="cufon">Tin liên quan</h2>
                <ul class="ul">
                @foreach($related_news as $key => $new)
                  	 <li>&raquo; <a href="{{URL::route('details.news', ['alias'=>$new->alias,'id'=>$new->id])}}">{{$new->title}}</a> </li>            	
                @endforeach
                </ul>
            </div><!--float_l-->
            <div class="box_other_news">
                <h2 class="cufon">Tin mới cập nhật</h2>
                <ul class="ul">
                @foreach($new_news as $key => $new)
                     <li>&raquo; <a href="{{URL::route('details.news', ['alias'=>$new->alias,'id'=>$new->id])}}">{{$new->title}}</a> </li>              
                @endforeach
                </ul>
            </div><!--float_r-->
        </div><!--other_news-->
    </div><!--detail_news-->
</div><!--content_news-->
</div>
<div class="clear"></div>
@endsection