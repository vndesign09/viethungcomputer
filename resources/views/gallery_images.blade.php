<link type="text/css" rel="stylesheet" href="{{URL::asset('resources/assets/css/bootstrap.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{URL::asset('resources/assets/datatable/css/dataTables.bootstrap4.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{URL::asset('resources/assets/admin/styles/main.css')}}">
<script src="{{URL::asset('resources/assets/admin/script/jquery-1.10.2.min.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/script/jquery.form.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $('#images').on('change',function(){
	        $('#upload').ajaxForm({
	            //display the uploaded images
	            beforeSubmit:function(e){
	                $('.loading').show();
	            },
	            success:function(e){
	                $('.loading').hide();
	                location.reload();
	            },
	            error:function(e){
	            }
	        }).submit();
	    });
	});
</script>
<div class="container" style="background:#fff;width:100%;">
	<form class="form-horizontal" id="upload" enctype="multipart/form-data" method="post" action="{{URL::route('post.filemanager', ['action'=>'insert_file'])}}" autocomplete="off">
	{!! csrf_field() !!}
	<!-- @if(isset($folders))
		<div class="form-horizontal">
		<div style="margin-top: 20px; width: 100px; float: left; margin-left: 20px;">
			<div class="form-group">
				<select name="folders" id="inputFolders" class="form-control">
					<option value="">Tất cả</option>
					@foreach($folders as $folder)
						<option value="{{$folder->alias}}">{{$folder->name}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="pull-right">{!! $media->appends(Request::except('page'))->links() !!}</div>
	@endif -->

		<table class="table table-stripper" style="font-size:12px;border-bottom: 1px solid #EFEFEF;margin-bottom: 35px;">
			<thead>
				<tr>
					<th>#</th>
					<th class="col-sm-2">File</th>
					<th class="col-sm-4">Tên</th>
					<th class="col-sm-5">Ngày chỉnh sửa</th>
					<th class="col-sm-1"></th>
				</tr>
			</thead>
			<tbody>
				@if(isset($media))
					@foreach($media as $file)
						<tr>
							<td><div class="checkbox">
								<label>
									<input type="checkbox" name="files[]" id="checkbox" value="{{URL::asset('public/images/'.$file->file)}}">
								</label>
							</div></td>
							<td>{{Html::image('public/images/'.$file->file, '', ['style'=>'width:68px;height:68px;'])}}</td>
							<td>{{$file->name}}</td>
							<td>{{$file->updated_at}}</td>
							<td><i class="glyphicon glyphicon-trash delete_file" data-file="{{$file->id}}" style="cursor: pointer;"></i></td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
		<div class="clearfix"></div>
		<div class="pull-left">{!! $media->appends(Request::except('page'))->links() !!}</div>
		<div class="pull-right">
			<div class="fileUpload btn btn-default" style="width: auto;">
				<span><i class="glyphicon glyphicon-open"></i></span>
				<input type="file" name="images[]" id="images" class="upload" multiple="true" accepts="image/*" />
			</div>
			<button type="button" class="btn btn-default insert" style="border-radius: 4px !important;"><!-- <i class="glyphicon glyphicon-plus"></i> -->Áp dụng</button>
		</div>
	</form>
	<!-- <a class="btn btn-success" data-toggle="modal" style="border-radius: 4px !important;" href='#modal-id'>Tạo thư mục</a> -->
</div>
<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tạo thư mục</h4>
			</div>
			{!! csrf_field() !!}
			<div class="modal-body">
				<div class="alert alert-danger hidden"></div>
				<div class="alert alert-success hidden"></div>
					<div class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-sm-4">Tên Thư Mục <abbr>*</abbr></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="folder" name="folder">
						</div>
					</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
				<button type="button" class="create_folder btn btn-primary">Lưu</button>
			</div>
		</div>
	</div>
</div>
<div class="loading" style="width: 100px; height: 100px; position: fixed; top: 40%; left: 50%;margin-left: -100px; display: none;"><img src="{{URL::asset('images/web/progress.gif')}}" style="max-width: 100%"></div>


<script src="{{URL::asset('resources/assets/admin/script/jquery-ui.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/script/main.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/script/bootstrap.min.js')}}"></script>
<script type="text/javascript">
	// Tạo Folder
	$('.create_folder').click(function(){
		$.ajax({
			url: "{{URL::route('post.filemanager', ['action'=>'create_folder'])}}",
			type: 'POST',
			dataType: 'json',
			data: {folder: $('#folder').val(), _token: $('input[name="_token"]').val()},
			success : function(json){
	            if(! json.has)
	            {	
	            	var j = $.parseJSON(json.message);
	            	var error = [];
	            	$.each(j, function(index, val) {
            			error.push("<li>"+val+"</li>");
            		});
            		$('.alert-danger').removeClass('hidden').html("<ul>"+error.join('')+"</ul>");
	            }else{
            		$('#inputFolders').append('<option value="'+json.alias+'">'+json.name+'</option>');
	            }
	        }
		});
	});
	// Đẩy dữ liệu xuống
	    $('.insert').click(function(){
	    	var allVals = [];
	    	var galleries = [];
		    $('#checkbox:checked').each(function() {
		       	allVals.push('<div><img class="thumbnail" src="'+$(this).val()+'"></div>');
		       	galleries.push($(this).val());
		    });

			var gallery = window.opener.$("#gallery");
			var preview = window.opener.$("#result");
			//var preview = window.opener.document.getElementById("blah");
			gallery.val('['+galleries+']');
			preview.find('div').remove();
            preview.append(allVals.join(''));

			window.top.close();
		});
	// Xóa file
	$('.delete_file').on('click', function(){
		var id = $(this).attr('data-file');
		console.log(id);
		$.ajax({
			url: "{{URL::route('post.filemanager', ['action'=>'delete_file'])}}",
			type: 'POST',
			dataType: 'json',
			data: {id: id, _token: $('input[name="_token"]').val()},
			success : function(json){
	            if(json.has)
	            {		
	            	$('.delete_file').each(function(){
	            		if($(this).attr('data-file') == json.id){
	            			$(this).closest('tr').remove();	
	            		}
	            	});
	            }
	        }
		});
	});
</script>