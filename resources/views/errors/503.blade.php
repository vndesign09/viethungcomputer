<html class="no-js"><head>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->

    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Page Not Found</title>
    
    <meta name="description" content="The page you are looking for was moved, removed, renamed or might never existed.">
    
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon-retina-ipad.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon-retina-iphone.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon-standard-ipad.png">
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon-standard-iphone.png">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{URL::asset('resources/assets/css/error.css')}}" media="all">
    
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <header class="header-primary">
                <a href="{{URL::route('home')}}" rel="home">{{$options['title']}}</a>
            </header><!-- header-primary -->
            
            <div class="content-primary">
                <h1 class="title">KHÔNG TÌM THẤY</h1>
                <p class="description">Trang bạn đang truy cập không tồn tại hoặc không còn khả dụng</p>
                <div class="section-footer">
                    <a href="{{URL::route('home')}}" class="button button-primary">Trang chủ</a>
                    <a href="#" class="button button-default">Báo lỗi</a>
                </div>
            </div><!-- content-primary -->
        </div><!-- container -->

        <div class="overlay"></div>
    </div><!-- wrapper -->

</body></html>