<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@if(isset($options['title'])){{$options['title']}}@endif</title>

    <!-- Fonts -->
    <link href="{{URL::asset('resources/assets/css/font-awesome.min.css')}}" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{URL::asset('resources/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('resources/assets/css/login.css')}}" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">

    @yield('content')

    <!-- JavaScripts -->
    <script src="{{URL::asset('resources/assets/js/jquery.min.js')}}"></script>
    <script src="{{URL::asset('resources/assets/js/bootstrap.min.js')}}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>


