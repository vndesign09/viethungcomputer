<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <title>{!!$options['title']!!}</title>
  	<meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="{!!$options['keywords']!!}">
    <meta name="description" content="{!!$options['descriptions']!!}">
    <meta content="DOCUMENT" name="RESOURCE-TYPE">

  	<meta name="robots" content="noindex,nofollow,nosnippet,noodp,noarchive,noimageindex">

    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  	<link type="text/css" rel="stylesheet" href="{{URL::asset('resources/assets/sp/css/mobile_style.css')}}">
     <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
<style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}.fb_link img{border:none}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_reset .fb_dialog_legacy{overflow:visible}.fb_dialog_advanced{padding:10px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}.fb_dialog_content{background:#fff;color:#333}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;_background-image:url(https://static.xx.fbcdn.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif);cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{top:5px;left:5px;right:auto}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent;_background-image:url(https://static.xx.fbcdn.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif)}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent;_background-image:url(https://static.xx.fbcdn.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif)}.fb_dialog_loader{background-color:#f6f7f8;border:1px solid #606060;font-size:24px;padding:20px}.fb_dialog_top_left,.fb_dialog_top_right,.fb_dialog_bottom_left,.fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}.fb_dialog_top_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 0;left:-10px;top:-10px}.fb_dialog_top_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -10px;right:-10px;top:-10px}.fb_dialog_bottom_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -20px;bottom:-10px;left:-10px}.fb_dialog_bottom_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -30px;right:-10px;bottom:-10px}.fb_dialog_vert_left,.fb_dialog_vert_right,.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}.fb_dialog_vert_left,.fb_dialog_vert_right{width:10px;height:100%}.fb_dialog_vert_left{margin-left:-10px}.fb_dialog_vert_right{right:0;margin-right:-10px}.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{width:100%;height:10px}.fb_dialog_horiz_top{margin-top:-10px}.fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #3a5795;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{width:auto;height:auto;min-height:initial;min-width:initial;background:none}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{color:#fff;display:block;padding-top:20px;clear:both;font-size:18px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .45);position:absolute;bottom:0;left:0;right:0;top:0;width:100%;min-height:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_content .dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6), color-stop(.5, #355492), to(#2A4887));border:1px solid #2f477a;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset, rgba(255, 255, 255, .167969) 0 1px 0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f6f7f8;border:1px solid #555;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v2/yD/r/t-wz8gw1xG1.png);background-repeat:no-repeat;background-position:50% 50%;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_hide_iframes iframe{position:relative;left:-10000px}.fb_iframe_widget_loader{position:relative;display:inline-block}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}.fb_iframe_widget_loader iframe{min-height:32px;z-index:2;zoom:1}.fb_iframe_widget_loader .FB_Loader{background:url(https://static.xx.fbcdn.net/rsrc.php/v2/y9/r/jKEcVPZFk-2.gif) no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}</style>
</head>
@if (Auth::check())
<body @yield('class_body') style="padding-top: 44px;">
<div id="cssmenu">
      <ul>
          <li><a href="{{URL::route('home.admin')}}"><i class="fa fa-dashboard"></i>  Tổng quan</a></li>
          <li class='has-sub'><a href="{!! URL::route('cat_pro.index') !!}"><i class="fa fa-sitemap fa-fw"></i>  Danh mục sản phẩm</a>
              <ul>
                <li>
                  <a href="{!! URL::route('cat_pro.add') !!}"><i class="fa fa-plus"></i>  Thêm Danh mục sản phẩm</a>
                </li>
              </ul>
          </li>
          <li class='has-sub'><a href="{!! URL::route('prod.index') !!}"><i class="fa fa-gift fa-fw"></i>  Sản phẩm</a>
              <ul>
                <li class='has-sub'>
                  <a href="{!! URL::route('prod.add') !!}"><i class="fa fa-plus"></i>  Thêm sản phẩm</a>
                </li>
              </ul>
          </li>
          <li class='has-sub'><a href="{!! URL::route('news.index') !!}"><i class="fa fa-file fa-fw"></i>  Tin tức</a>
              <ul>
                <li>
                  <a href="{!! URL::route('prod.add') !!}"><i class="fa fa-plus"></i>  Thêm tin tức</a>
                </li>
              </ul>
          </li>
          <li class='has-sub'><a href="{!! URL::route('news.index') !!}"><i class="fa fa-file-o fa-fw"></i>  Trang</a>
              <ul>
                <li>
                  <a href="{!! URL::route('pages.add') !!}"><i class="fa fa-plus"></i>  Thêm trang</a>
                </li>
              </ul>
          </li>
          <li><a href="{!! URL::route('options.index') !!}"><i class="fa fa-cogs fa-fw"></i>  Cài đặt</a></li>
          <li><a href="{{URL::action('Auth\AuthController@logout')}}">Thoát</a></li>
      </ul>
</div>
@else
<body>
@endif
  
  <script src="{{URL::asset('resources/assets/index/js/jquery.min.js')}}"></script>
  <script src="{{URL::asset('resources/assets/index/js/common.js')}}"></script>
  <script src="{{URL::asset('resources/assets/index/js/carousel.js')}}"></script>
  <script src="{{URL::asset('resources/assets/sp/includes/js/swipe.js')}}"></script>
  <script src="{{URL::asset('resources/assets/sp/includes/js/m_slider.js')}}"></script>
  <script src="{{URL::asset('resources/assets/sp/js/script.js')}}"></script>
  <div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div><iframe name="fb_xdm_frame_http" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" id="fb_xdm_frame_http" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="http://staticxx.facebook.com/connect/xd_arbiter.php?version=42#channel=f17da6d2325d718&amp;origin=http%3A%2F%2Fwww.hanoicomputer.vn" style="border: none;"></iframe><iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="https://staticxx.facebook.com/connect/xd_arbiter.php?version=42#channel=f17da6d2325d718&amp;origin=http%3A%2F%2Fwww.hanoicomputer.vn" style="border: none;"></iframe></div></div><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div></div></div></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
  <div class="m_container">
    <div style="margin:0 5px">
    <span id="header_area">
<script>
$(function(){
	$("#support_top").click(function(){
  		$(".hOnline").toggle();
  	});
  })
</script>
<div id="top">
  {!!Html::image('resources/assets/sp/images/m-icon-phone.png')!!}<a href="#" style="color:red; font-size:11px;">@if(isset($options['customer_care']))Liên hệ ngay: {{$options['customer_care']}} @endif</a>

</div><!--top-->
<div id="header">
  <a href="{!!URL::route('home')!!}"><img src="{{URL::asset('images/web/'.$options['logo'])}}" alt="{{$options['title']}}" height="50"></a>
</div>
<div id="nav">
  <div id="m_menu">Menu<img src="{!!URL::asset('resources/assets/sp/images/m_icon_menu.png')!!}" alt="menu"></div><!--m_menu-->
  <div id="sub_menu">
    <div style="border:solid 1px #e00">
      <div class="menu-home" style="background:white; padding:5px;">
        <ul class="ul ul_menu">
        @foreach($categories as $key => $cat)
          <li>
            <a href="{!!URL::route('category.product', ['alias'=>$cat->alias, 'id'=>$cat->id])!!}" class="root">
              @if($cat->thumbnail != '')
              <img src="{!!URL::asset('images/categories/'.$cat->thumbnail)!!}" alt="{!!$cat->name!!}" style="max-width: 26px;">
              @else
              <img src="{!!URL::asset('images/categories/default.png')!!}" alt="{!!$cat->name!!}" style="max-width: 26px;">
              @endif
              <span>{!!$cat->name!!}</span>
            </a>
          </li>
        @endforeach
        </ul>
        <div class="clear"></div>
      </div><!--menu-home-->
      <div class="clear"></div>
    </div>
  </div><!--sub_menu-->
  <a href="{{URL::route('news')}}" id="sale_link">Tin tức</a>
  <div id="search">
    <form method="get" action="{!!URL::route('search')!!}" enctype="multipart/form-data">
      <input type="text" class="cssText" name="k">
      <input type="image" src="{!!URL::asset('resources/assets/sp/images/m_icon_search.png')!!}" class="cssSubmit" value="">
    </form>
  </div><!--search-->
</div><!--nav-->
<script>
	$(function(){
  		$("#m_menu").click(function(){
  			$("#sub_menu").toggle();
  		});
  	})
</script>
</span>
    <script>
  $(document).ready(function(){
    $("#slider").responsiveSlides({
      auto: true,
      pager: true,
      speed: 300,
      maxwidth: 800
  });
  });
</script>

@Yield("content")

    <span id="footer_area">
<div class="clear"></div>
 @foreach($positons_footer as $name => $box_footer)
    <div class="box_footer">
            <h3>{{$name}}</h3><span class="icon_compress">+</span>
            <ul>
               @foreach($box_footer['pages'] as $k => $page)
                <li><a href="{{URL::route('details.pages', $page->alias)}}">{{$page->title}}</a></li>
               @endforeach
            </ul>
        </div>
       
@endforeach

<script>
$(function(){
  $(".box_footer h3").click(function(){
      $(this).parent().children("ul").toggle();
      if($(this).parent().children(".icon_compress").text()=='+') $(this).parent().children(".icon_compress").text("-");
      else $(this).parent().children(".icon_compress").text("+");
  });

  $(".footer_address").click(function(){
      $(this).children("ul").toggle();
      if($(this).children(".icon_compress").text()=='+') $(this).children(".icon_compress").text("-");
      else $(this).children(".icon_compress").text("+");
  });
  })
</script>

<div class="clear space2"></div>

<script>
  $(document).ready(function(){
      $(".footer_address ul").fadeOut();
      $(".footer_address ul.current").fadeIn();
  });
</script>
<div id="copyright">
  {!!$options['footer']!!}
</div>
<table class="tbl_footer">
  <tbody><tr>
    <td align="left"><a href="/">Trang chủ</a></td>
    <td align="right"><a href="#">Về đầu trang</a></td>
  </tr>
</tbody></table>
</span>
</body></html>
