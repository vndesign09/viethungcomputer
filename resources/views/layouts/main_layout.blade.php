<!DOCTYPE html>

<html lang="vi">

<head>

  <title>@if(isset($options['title'])){{$options['title']}}@endif</title>

	<meta http-equiv="refresh" content="1000" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <meta content="DOCUMENT" name="RESOURCE-TYPE" />

	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />

  @if(isset($options['keywords']))

  <meta name="keywords" content="{{$options['keywords']}}">

  @endif

  @if(isset($options['description']))

  <meta name="description" content="{!!$options['descriptions']!!}">

  @endif

  <link type="text/css" rel="stylesheet" href="{{URL::asset('resources/assets/index/css/style.css')}}">

  <link type="text/css" rel="stylesheet" href="{{URL::asset('resources/assets/admin/styles/font-awesome.min.css')}}">

	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,vietnamese' rel='stylesheet' type='text/css' />

  	<!--[if lt IE 9]>

 	<style>.p_img{display:block;}</style>

	<![endif]-->

  <script src="{{URL::asset('resources/assets/index/js/jquery.min.js')}}"></script>

  <script src="{{URL::asset('resources/assets/fancybox/jquery.easing-1.3.pack.js')}}"></script> 

<script type="text/javascript" src="{{URL::asset('resources/assets/fancybox/jquery.mousewheel-3.0.4.pack.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('resources/assets/fancybox/jquery.fancybox-1.3.4.pack.js')}}"></script>

<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>

<link rel="stylesheet" type="text/css" href="{{URL::asset('resources/assets/fancybox/jquery.fancybox-1.3.4.css')}}" media="screen" />

  <script src="{{URL::asset('resources/assets/index/js/common.js')}}"></script>

  <script src="{{URL::asset('resources/assets/index/js/carousel.js')}}"></script>

  <script src="{{URL::asset('resources/assets/index/js/lazyload.js')}}"></script>

    <script type="text/javascript">

      $(document).ready(function(e) {

          var lazyloader = new LazyLoad({

          elements: ".lazy"

          });

      });



    </script>

    <script>(function(d, s, id) {

        var js, fjs = d.getElementsByTagName(s)[0];

        if (d.getElementById(id)) return;

        js = d.createElement(s); js.id = id;

        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";

        fjs.parentNode.insertBefore(js, fjs);

      }(document, 'script', 'facebook-jssdk'));
    </script>

</head>

@if (Auth::check())

<body @yield('class_body') style="padding-top: 44px;">

<div id="admin_menu">

    <div class="container">

      <ul>

          <li><a href="{{URL::route('home.admin')}}"><i class="fa fa-dashboard"></i>  Tổng quan</a></li>

          <li><a href="{!! URL::route('cat_pro.index') !!}"><i class="fa fa-sitemap fa-fw"></i>  Danh mục sản phẩm</a>

              <ul>

                <li>

                  <a href="{!! URL::route('cat_pro.add') !!}"><i class="fa fa-plus"></i>  Thêm Danh mục sản phẩm</a>

                </li>

              </ul>

          </li>

          <li><a href="{!! URL::route('prod.index') !!}"><i class="fa fa-gift fa-fw"></i>  Sản phẩm</a>

              <ul>

                <li>

                  <a href="{!! URL::route('prod.add') !!}"><i class="fa fa-plus"></i>  Thêm sản phẩm</a>

                </li>

              </ul>

          </li>

          <li><a href="{!! URL::route('news.index') !!}"><i class="fa fa-file fa-fw"></i>  Tin tức</a>

              <ul>

                <li>

                  <a href="{!! URL::route('prod.add') !!}"><i class="fa fa-plus"></i>  Thêm tin tức</a>

                </li>

              </ul>

          </li>

          <li><a href="{!! URL::route('news.index') !!}"><i class="fa fa-file-o fa-fw"></i>  Trang</a>

              <ul>

                <li>

                  <a href="{!! URL::route('pages.add') !!}"><i class="fa fa-plus"></i>  Thêm trang</a>

                </li>

              </ul>

          </li>

          <li><a href="{!! URL::route('options.index') !!}"><i class="fa fa-cogs fa-fw"></i>  Cài đặt</a></li>

          <li class="last"><a href="{{URL::action('Auth\AuthController@logout')}}">Thoát</a></li>

      </ul>

    </div>

</div>

@else

<body @yield('class_body')>

@endif

  <!--Array

(

    [name] => home

    [view] =>

    [view_id] => 0

    [view_url] =>

)

1-->



    <script type="text/javascript">

  /*

  $(document).mousemove(function(e){

      	var x_out = parseInt($("#search").offset().left) + parseInt($("#search").width());

      	var y_out = parseInt($("#search").offset().top) + parseInt($(".autocomplete-suggestions").height()+50);

        if(e.pageX > x_out || e.pageY > y_out || e.pageX < parseInt($("#search").offset().left))

        	$(".autocomplete-suggestions").hide();

      });

  */

     $(document).ready(function(){

      	var curr_text = "";

      	var count_select = 0;

      	var curr_element="";





      	$("#text_search").keyup(function(b){



  			if (b.keyCode != 38 && b.keyCode != 40) {

                inputString = $(this).val();

                if(inputString.trim() !=''){

                  $(".autocomplete-suggestions").show();

                  $(".autocomplete-suggestions").load("/ajax/get_product_list.php?template=header&q="+encodeURIComponent(inputString));

                }else  {

                  $(".autocomplete-suggestions").hide();

                  count_select=0;

                }

              }



      		if (b.keyCode == 40) {

      			count_select++;

      			curr_element = $(".autocomplete-suggestion:nth-child("+count_select+")");



      			curr_text = curr_element.find(".suggest_link").text();

      			$("#text_search").val(curr_text);

      			$(".autocomplete-suggestion").removeClass("selected");

      			$(curr_element).addClass("selected");



      		}



  			if (b.keyCode == 38 && count_select > 1) {

      			count_select--;

      			curr_element = $(".autocomplete-suggestion:nth-child("+count_select+")");

      			curr_text = curr_element.find(".suggest_link").text();

      			$("#text_search").val(curr_text);

      			$(".autocomplete-suggestion").removeClass("selected");

      			$(curr_element).addClass("selected");

      		}

      	});





  		$('body').click(function(){

  			$(".autocomplete-suggestions").hide();

  		});

      });

      

  </script>

<!--code google dich

<div id="google_translate_element"></div><script type="text/javascript">

function googleTranslateElementInit() {

  new google.translate.TranslateElement({pageLanguage: 'vi', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');

}

</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

-->
<div id="boxtop">
  <ul class="container">
    <li class="hotline">Hotline:  <span>0986.982.492</span></li>
    <li class="baogia"><a href="#">Tải báo giá, hợp đồng</a></li>
    <li class="baogia"><a href="{{URL::route('build.computer')}}">Xây dựng cấu hình</a></li>
    <li class="tintuc"><a href="{{URL::route('news')}}">Tin tức</a></li>
  </ul>
</div>
<div class="container">
    <div id="header">

        <a href="{{URL::route('home')}}" id="logo"><img src="{{URL::asset('images/web/'.$options['logo'])}}" alt="{{$options['title']}}" /> </a>

        <div id="banner_header"><img border=0 src="{{URL::asset('images/web/'.$options['banner'])}}" width='1050' height='133' alt=""/></div>

<!--header_right-->

    </div><!--header-->



    <div id="nav_vertical">

        <div class="bg title_nav_verticle"></div>

      	<ul class="ul ul_menu" >

        

        @foreach($categories as $key => $cat)

        @if($cat->thumbnail != '')

        <li style="background-image: url('{{URL::asset("images/categories/".$cat->thumbnail)}}'); background-size:10%;">

        @else

        <li style="background-image: url('{{URL::asset("images/categories/default.png")}}'); background-size:10%;">

        @endif

        <a href="{!!URL::route('category.product', ['alias'=>$cat->alias, 'id'=> $cat->id])!!}" class="root">{!!$cat->name!!}</a>

          {!!getMenu($cat_arr, $cat->id)!!}    

        </li>

        @endforeach

        

        </ul>

        <div class="clear"></div>

    </div><!--nav_vertical-->



  	<script type="text/javascript">

  	$(document).ready(function(){

      	$(".sub_nav").each(function(){

      		//sort_sub_nav(0,16,$(this).find("li"),$(this));

      	});



        $("#nav_vertical li").hover(function(){

            $(this).children(".sub_nav").show();

        },function(){

            $(this).children(".sub_nav").hide();

        });

    });

  	</script>

  	<script type="text/javascript">

  	$(document).ready(function(){

      $(window).scroll(function(){

      	t = $(window).scrollTop();

      	if(t > 200) $("#nav_horizontal").addClass("fixed");

      	else $("#nav_horizontal").removeClass("fixed");

      	});

      });

  	</script>

    @if(Auth::check())

    <div id="nav_horizontal" style="top:28px;">

    @else

    <div id="nav_horizontal">

    @endif

        <div id="search" class="bg">

            <form method="get" action="{!!URL::route('search')!!}" enctype="multipart/form-data">

              	<select name="cat" id="sel_cat" style="display: none;">

                  <option value="0">Tất cả</option>

                  @foreach($categories as $key => $cat)

                  <option value="{!!$cat->id!!}">{!!$cat->name!!}</option>

                  @endforeach

  



                </select>

                <div id="ul_cat" class="ul">

                    <div class="selected"><span>Tất cả</span> <i class="bg icon_drop"></i> </div>

                    <ul style="display: none;">

                        <li title="0">Tất cả</li>

                        @foreach($categories as $key => $cat)

                        <li title="{!!$cat->id!!}">{!!$cat->name!!}</option>

                        @endforeach





                    </ul>

                </div>

                <input type="text" class="text" id="text_search" name="k" placeholder="Gõ từ khóa tìm kiếm..."   autocomplete="off"/>

                <input type="submit" id="submit_search" value="Search" />

            </form>

          	<div class="autocomplete-suggestions"></div>

        </div><!--search-->

        <ul class="ul">

           <!--<li><a href="/deal" class="bg icon_deal_header"></a></li>-->


            <li style="padding:0px 5px 0px 5px; color:#000"><b>@if(isset($options['customer_care']))Liên hệ: {{$options['customer_care']}} @endif</b></li>

          

          <li style="padding:0px 5px 0px 5px"><a href="{{URL::route('news')}}">Tin tức</a></li>

           @foreach($positons_menu as $name => $page)

          <li style="padding:0px 5px 0px 5px"><a href="{{URL::route('details.pages', $page->alias)}}">{{$page->title}}</a></li>

            @endforeach

        </ul>

    </div><!--nav-->

    <div class="clear"></div>

    </div><!--container-->

@Yield("content")

<div id="footer">

   <div class="container">

    <div class="box_footer cam_ket">
    <h3>Cam kết</h3>
    <ul>
      <li class="c1">Sản phẩm, hàng hóa chính hãng đa dạng phong phú.</li>
      <li class="c2">Luôn luôn giá rẻ & khuyến mại không ngừng.</li>
      <li class="c3">Dịch vụ chăm sóc khách hàng tốt nhất.</li>
    </ul>
    </div>
   @foreach($positons_footer as $name => $box_footer)

    <div class="box_footer">

            <h3>{{$name}}</h3>

            <ul>

               @foreach($box_footer['pages'] as $k => $page)

                <li><a href="{{URL::route('details.pages', $page->alias)}}">{{$page->title}}</a></li>

               @endforeach

            </ul>

        </div>

       

    @endforeach
    <div class="box_footer">
      <div class="fb-page" data-href="https://www.facebook.com/viethungcomputer/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/viethungcomputer/"><a href="https://www.facebook.com/viethungcomputer/">Công Ty Tnhh Thương Mại Và Dịch Vụ Tin Học Việt Hưng</a></blockquote></div></div>
    </div>

     <div class="clear"></div>

    </div><!--container-->

</div><!--footer-->

<div class="container" style="padding:10px 0;">

    <div id="copyright">{!!$options['footer']!!}</div>

</div><!--container-->



<div id="order_popup">

  <h2 style="float: left;color: White;font-size: 12px;white-space: nowrap;margin: 0;line-height: 25px;padding-left: 5px;">

    <p style="float: left;color: Yellow;font-size: 12px;white-space: nowrap;margin: 0;line-height: 20px;padding-left: 5px;">@if(isset($options['customer_care']))Liên hệ ngay: {{$options['customer_care']}} @endif</p>

    </h2>

  <a href="" class="btn_popup show" style="display: inline-block;"></a>

  <a href="" class="btn_popup hide" style="display: none;"></a>

  <div class="clear"></div>

  <div id="content_popup_order" style="display: none;">

    <div class="list_number">

      <table>

        <tr>

          <td colspan="2"><img src="{{URL::asset('images/web/goi-mua-hang.png')}}" alt="" width="247" height="40" style="float:left;"/></td>

        </tr>

        <tr>

          <td id="list_support_footer">



            <ul>

            @if(count($supports) >0)

            @foreach($supports as $k => $supporter)

            <?php

                $info = json_decode($supporter); 

            ?>

              <li><b>{{$info->phone}}</b><a href="ymsgr:sendIM?{{$info->yahoo}}" rel="nofollow"><span>{{$info->fullname}}</span>

              @if($info->yahoo != '')

              <img src="http://opi.yahoo.com/online?u={{$info->yahoo}}&m=g&t=1&l=us" width="64" height="16"/>

              @endif

            </a>

            @if($info->skype != '')<a href="skype:{{$info->skype}}?chat"> <img style="width: 15px;" src="{{URL::asset('images/web/icon-skype.jpg')}}"></a>

            @endif

            </li>

            @endforeach

            @endif

        </ul>

          </td>

        </tr>

      </table>



    </div>

  </div>

</div><!--order_popup-->



</div>

  <script>

    $(document).ready(function(){

    IS_IPAD = navigator.userAgent.match(/iPad/i) != null;

    IS_IPHONE = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);



    if (IS_IPAD || IS_IPHONE) {

    $(".tooltip").remove();

    }

    });

  </script>



  <!-- Begin subizChat -->



  <!-- ket thuc subiz -->



<script>

  $('.box_cate.hide').hide();

  </script>

@yield('scripts')

<script type='text/javascript'>function init_map(){var myOptions = {zoom:13,center:new google.maps.LatLng(20.9930588,105.79319750000002),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(20.9930588,105.79319750000002)});infowindow = new google.maps.InfoWindow({content:'<strong>Công Ty Tnhh Thương Mại Dịch Vụ Tin Học Việt Hưng</strong><br>Số 47 Lương Thế Vinh, Trung Văn,  Từ Liêm, Hà Nội<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
<script lang="javascript">(function() {var pname = ( (document.title !='')? document.title : document.querySelector('meta[property="og:title"]').getAttribute('content') );var ga = document.createElement('script'); ga.type = 'text/javascript';ga.src = '//live.vnpgroup.net/js/web_client_box.php?hash=6902619cd4aaea4ace67ee584a566a00&data=eyJzc29faWQiOjE3NjkzNTMsImhhc2giOiJjNTI0ODFlYjVlOTYyMjdhMmMwYTVkYjVjNzhmMTI3NyJ9&pname='+pname;var s = document.getElementsByTagName('script');s[0].parentNode.insertBefore(ga, s[0]);})();</script><noscript><a href="http://www.vatgia.com" title="vatgia.com" target="_blank">Tài trợ bởi vatgia.com</a></noscript><noscript><a href="http://vchat.vn" title="vchat.vn" target="_blank">Phát triển bởi vchat.vn</a></noscript>  
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=372719696231514";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78252214-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>

  <!-- Load time: 0.25 seconds-->

  <!-- Powered by HuraStore 6.0 / Website: www.HuraSoft.vn -->

