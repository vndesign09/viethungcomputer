@if ($errors->has())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif 
		@if($success = Session::get('success'))
			<div class="alert alert-success">
				{{$success}}
		    </div>
		@endif