@extends('layouts.main_layout')
@section('content')
@section('class_body')class="build_computer"@stop
<div class="container">
    <div class="clear space"></div>
    <div class="buid-title"><h1>Xây dựng máy tính</h1></div>
    <input type="hidden" id="pcbuilder_step" value="0">
    <input type="hidden" id="pc_cate" value="{{$cate->name}}">
    <div class="col-sm-9">
        <div class="steps">
            Bước 1 - chọn {{$cate->name}} - <a href="javascript:pcbuilder_next_step()">Bỏ qua</a>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-3 process-filter">
              <h2>Khoảng giá</h2>
              <ul>
                
                    <li>
                      <input type="checkbox" onclick="loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=1000000')"><a href="javascript:loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=1000000')"><span class="symbol">&nbsp;</span> Dưới 1 triệu</a> (1)<br>
                    </li>
                
                    <li>
                      <input type="checkbox" onclick="loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=2000000&amp;min=1000000')"><a href="javascript:loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=2000000&amp;min=1000000')"><span class="symbol">&nbsp;</span> 1 triệu - 2 triệu</a> (49)<br>
                    </li>
                
                    <li>
                      <input type="checkbox" onclick="loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=3000000&amp;min=2000000')"><a href="javascript:loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=3000000&amp;min=2000000')"><span class="symbol">&nbsp;</span> 2 triệu - 3 triệu</a> (25)<br>
                    </li>
                
                    <li>
                      <input type="checkbox" onclick="loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=4000000&amp;min=3000000')"><a href="javascript:loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;max=4000000&amp;min=3000000')"><span class="symbol">&nbsp;</span> 3 triệu - 4 triệu</a> (16)<br>
                    </li>
                
                    <li>
                      <input type="checkbox" onclick="loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;min=4000000')"><a href="javascript:loadAjaxContent('pc_part_process','http://www.hanoicomputer.vn/ajax/pcbuilder_product_selection.php?holder=pc_part_process&amp;pc_part_id=%2C&amp;step=5&amp;min=4000000')"><span class="symbol">&nbsp;</span> Trên 4 triệu</a> (26)<br>
                    </li>

                </ul>                                
        </div>
       
        <div class="col-sm-9">
             <p>
                Sắp xếp sản phẩm
                <select id="sort" onchange="loadSortContent()">
                    <option value="sort_new">Sản phẩm mới nhất</option>
                    <option value="sort_min">Giá thấp -&gt; cao</option>
                    <option value="sort_max">Giá cao -&gt; thấp</option>
                </select>
            </p>
            <div class="clearfix"></div>
              <table border="0" cellpadding="5" cellspacing="0" style="width:100%;" id="list_producs">
                        <thead>
                          <tr class="bTitle">
                            <td class="bTitle-cols1">STT</td>
                            <td class="bTitle-cols2">Ảnh</td>
                            <td class="bTitle-cols3">Sản phẩm</td>
                            <td class="bTitle-cols4">Lựa chọn</td>
                          </tr>
                        </thead>
                        <tbody>
                          
                            @foreach($list_products as $key => $product)
                            <?php if(($product->main_price) == 1) {
                                $price = 'Liên hệ';
                            }else if($product->main_price > 1){
                                $price = adddotNumber($product->main_price);
                            }else{
                                $price = adddotNumber($product->price);
                            }
                            ?>
                            <tr class="bList">
                                <td class="bList-cols1">{{$key+1}}</td>
                                <td class="bList-cols2 cssImg">
                                      {!!Html::image('images/products/'.$product->thumbnail, $product->name)!!}
                                </td>
                                <td class="bList-cols3">
                                    <p class="cssName">
                                        <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" target="_blank">{{$product->name}}</a>
                                    </p>
                                    <p class="cssSummary"></p> 
                                    <p>Mã sản phẩm: {!!$product->code!!}</p>
                                    <p>Bảo hành: {!!$product->guarantee_info!!}</p>
                                
                                </td>
                                <td class="bList-cols4">
                                    <p class="cssPrice">{{$price}}</p>
                                    <p class="cssSelect">
                                        <a href="javascript:pcbuilder_select_part('{{$product->id}}', '{{$price}}', '{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}', '{{$product->name}}')">Chọn sản phẩm</a>
                                    </p>
                                </td>
                            </tr>
                          @endforeach
                         
                        
                    </tbody></table>
        </div>
    </div>
    <div class="col-sm-3">
        <b>Danh sách bạn đã chọn</b>
        <div id="pcbuilder_box" class="view-content" style="top: 230px;">                
                
            <div id="pc_part_select">
                  
                    <h2 onclick="pcbuilder_go_step(0)">1:BO MẠCH CHỦ:</h2><div id="part_selected_0"></div>
  
                    <h2 onclick="pcbuilder_go_step(1)">2: VI XỬ LÝ</h2><div id="part_selected_1"></div>
                    
                    <h2 onclick="pcbuilder_go_step(2)">3: RAM:</h2><div id="part_selected_2"></div>
                    
                    <h2 onclick="pcbuilder_go_step(3)">4: HDD:</h2><div id="part_selected_3"></div>
                
                    <h2 onclick="pcbuilder_go_step(4)">5: VGA:</h2><div id="part_selected_4"></div>
                    
                    <h2 onclick="pcbuilder_go_step(5)">6: NGUỒN</h2><div id="part_selected_5"></div>
                    
                    <h2 onclick="pcbuilder_go_step(6)">7: Ổ QUANG</h2><div id="part_selected_6"></div>
                    
                    <h2 onclick="pcbuilder_go_step(7)">8: VỎ CASE</h2><div id="part_selected_7"></div>
                    
                    <h2 onclick="pcbuilder_go_step(8)">9: MÀN HÌNH</h2><div id="part_selected_8"></div>
                    
                    <h2 onclick="pcbuilder_go_step(9)">10: BÀN PHÍM</h2><div id="part_selected_9"></div>
                    
                    <h2 onclick="pcbuilder_go_step(10)">11: CHUỘT</h2><div id="part_selected_10"></div>
                    
                    <h2 onclick="pcbuilder_go_step(11)">12: LOA</h2><div id="part_selected_11"></div>
                    
                </div>
            </div>
            <div class="view-title">
                <p>Cấu hình - <a href="javascript:pcbuilder_viewpc()">Xem &amp; In</a></p>
                <div id="pc_part_total_price"></div>
            </div>
    </div>
</div><!--container-->

<div class="clear"></div>
@section('scripts')
  <script type="text/javascript">
    var url = '{!!URL::route("home")!!}';
    function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
          val = val.toString().replace(/(\d+)(\d{3})/, '$1'+'.'+'$2');
        }
        return val;
      }
    function pcbuilder_next_step() {
        var a = parseInt($("#pcbuilder_step").val());
        if (a > 11) {    
            $("#pcbuilder_step").val(0);
        }else {
            $("#pcbuilder_step").val(a+1);
        }
        process($("#pcbuilder_step").val());
        $('#sort').prop('selectedIndex',0);
        endProcess(a);
    }

    

    function pcbuilder_select_part(id, price, alias, name){
        var cat_id = $("#pcbuilder_step").val();
        var step = $('#part_selected_'+cat_id).prev('h2').attr('onclick');
        $('#part_selected_'+cat_id).attr('data-id', id).html('<p class="cssName"><a href="'+alias+'" target="_blank">'+name+'</a></p><p class="cssSelect"><b>'+price+'</b><a href="javascript:'+step+'">Chọn lại</a> - <a href="javascript:pcbuilder_remove_part('+"'"+cat_id+"'"+')">Xóa bỏ</a></p>');
        pcbuilder_next_step();
        total();
        endProcess(cat_id);
    }
    

    function pcbuilder_go_step(step){
        $("#pcbuilder_step").val(step);
        process(step);
        $('#sort').prop('selectedIndex',0);
    }

    function process(val){
        $.ajax({
            url: '{{URL::route("build.computer")}}',
            method: 'GET',
            dataType: 'json',
            data: {id: val, sort: $('#sort').val()},
            success : function(json){
                var res = JSON.parse(json.list_products);
                if(res.length > 0){
                    var arr = new Array();
                    $.each(res, function(i, v){
                         var price = '';
                        if(v.main_price == 1) {
                           price = 'Liên hệ';
                        }
                        else if(v.main_price > 1){
                            price = commaSeparateNumber(v.main_price);
                        }else{
                            price = commaSeparateNumber(v.price);
                        }
                       product = '<tr class="bList"><td class="bList-cols1">'+(i+1)+'</td><td class="bList-cols2 cssImg"><img src="'+url+"/images/products/"+v.thumbnail+'"/></td><td class="bList-cols3"><p class="cssName"><a href="'+url+"/"+v.alias+"/"+v.id+'" target="_blank">'+v.name+'</a></p><p>Mã sản phẩm: '+v.code+'</p><p>Bảo hành: '+v.guarantee_info+'</p></td><td class="bList-cols4"><p class="cssPrice">'+price+'</p><p class="cssSelect"><a href="javascript:pcbuilder_select_part('+"'"+v.id+"'"+', '+"'"+commaSeparateNumber(v.price)+"'"+', '+"'"+url+"/"+v.alias+"/"+v.id+"'"+', '+"'"+v.name+"'"+')">Chọn sản phẩm</a></p></td></tr>';

                       
                       arr.push(product);
                    });
                   $('#list_producs tbody').html(arr.join(','));
                   
               }else{
                    $('#list_producs tbody').html('<tr><td colspan="3">Hiện chưa có sản phẩm. <a href="javascript:pcbuilder_next_step()">Tiếp tục</a></td></tr>');
               }
               $('#pc_cate').val(JSON.parse(json.cate).name);
               var a = parseInt($('#pcbuilder_step').val());
               $('.steps').html('Bước '+(a+1)+' - chọn '+$('#pc_cate').val()+' - <a href="javascript:pcbuilder_next_step()">Bỏ qua</a>');
            }
        });
    }

    function pcbuilder_remove_part(val){
        $('#part_selected_'+val).removeAttr('data-id').html('');
        total();
    }

    


    sumjq = function(selector) {
        var sum = 0;
        $(selector).each(function() {
            total_price = $(this).text();
            order = total_price.replace(/\./g, '');
            sum += Number(order);
        });
        return sum;
    }

    function total(){
       $('#pc_part_total_price').html("Tổng giá:  <span>"+commaSeparateNumber(sumjq('#pc_part_select b'))+" VND</span>");
    }


    function endProcess(index){
        if( index == 11){
            $('#list_producs tbody').html('<tr><td colspan="3"><p>Bạn đã xây dựng xong. Vui lòng <a href="javascript:pcbuilder_viewpc();">Click vào đây</a> để xem và in cấu hình</p></td></tr>');
            $('.steps').html('<p>Bạn đã xây dựng xong. Vui lòng <a href="javascript:pcbuilder_viewpc();">Click vào đây</a> để xem và in cấu hình</p>');
        }
    }

    function loadSortContent(){
        process($("#pcbuilder_step").val());
    }

    function pcbuilder_viewpc(){
        var arr = new Array();
        $('#pc_part_select div').each(function(){
            var attr = $(this).attr('data-id');
            if (typeof attr !== typeof undefined && attr !== false) {
              var id = $(this).attr('data-id');
                arr.push(id);
            }
        });
        location.href = url+'/xem-may-tinh-da-tao?parts='+arr.toString();
    }
  </script>
@stop
@stop
