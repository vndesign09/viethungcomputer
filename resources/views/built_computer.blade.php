@extends('layouts.main_layout')
@section('content')
@section('class_body')class="build_computer"@stop
<style type="text/css"> 
body{margin:0px;font-family:Tahoma; font-size:12px;}
img{border:0px;}
p{ margin:0px;}
h1{ margin:0px; font-size:12px;}
.clear{ clear:both;}
.error{ color:Red; font-weight:bold;}
#imageThumnailPanel ul{ list-style:none; margin:0; padding-left:0px}
#imageThumnailPanel ul li{ float:left; margin-right:2px; padding:3px; border:1px solid #CCC}
.cssHiddenSend{ float:left; padding-top:10px; padding-bottom:20px; border:solid 1px #C0C0C0; background-color:#fcfcf1}
.imageThumnail{border:1px solid #CCC; padding:1px; vertical-align:middle; text-align:center; display:inline-block; margin-right:2px; width:50px; height:50px;}

ul, li { list-style: none outside none;  margin: 0 auto;   padding: 0;   position: relative;}
.ulFrom2_Temp1{  float:left; width:100%; padding-top:5px; padding-bottom:5px;}
.ulFrom2_Temp1 .liCols1{     float:left; width:20%; text-align:right; padding-right:25px;}
.ulFrom2_Temp1 .liCols2{     float:left;}

.pcConfig-header{ float:left; width:100%; border-bottom:solid #666666 1px;}
.pcConfig-logo{ float:left;}
.pcConfig-logo img {height:90px;}
.pcConfig-address{ float:right; line-height:18px; border-bottom:1px sold #333333; text-align:right;}
.pcConfig-address b{ color:Blue; font-size:16px; display:inline-block; padding-bottom:10px; font-family:Times New Roman;}
.pcConfig-title{  text-align:center; border-top:1px solid #666666; margin-top:1px; display:block; padding-top:5px;}
.pcConfig-title .cssH1{ font-weight:bold; font-size:22px; font-family:Times New Roman;}

.pcConfig-customers{ float:left; width:70%; }
.pcConfig-date{ float:left; width:30%; font-weight:bold; padding-top:10px;}
.pcConfig-customers .ulCustomers{ float:left; width:100%; padding-bottom:5px;}
.pcConfig-customers .ulCustomers .liCols1{ float:left; padding-top:5px;}
.pcConfig-customers .ulCustomers .liCols2{ float:left; padding-left:5px;}
.pcConfig-customers input{ border:0px; border-bottom:1px dotted #C0C0C0; font-weight:bold;}
.pcConfig-customers .ulCustomers-temp2{ float:left; width:100%; padding-bottom:5px;}
.pcConfig-customers .ulCustomers-temp2 .liCols1{ float:left; padding-top:5px;}
.pcConfig-customers .ulCustomers-temp2 .liCols2{ float:left;}
.pcConfig-customers .ulCustomers-temp2 .liCols3{ float:left; padding-top:5px;}
.pcConfig-customers .ulCustomers-temp2 .liCols4{ float:left;}
.pcConfig-hello{ display:block; text-align:center; font-weight:bold; padding-bottom:5px; margin-top: 10px;}
.pcConfig-customers-content{ padding-top:5px; padding-bottom:5px;}
.pcConfig-customers-content .liCols1{  padding-top:5px; display:inline-block;}
.pcConfig-customers-content input{ border:0px; border-bottom:1px dotted #C0C0C0; font-weight:bold;}

.pcConfig-footer{ display:inline-block; padding-bottom:10px; width:100%;}
.pcConfig-footer .cssCols1{ float:left; width:15%;}
.pcConfig-footer .cssCols2{ float:left; width:85%; line-height:18px;}
.cssUpdate{ width:100px; height:25px; border:0px; background-image:url("/Images/Common/pc-send.gif"); cursor:pointer;}

.pcConfig-customer{ padding-top:10px;}
.pcConfig-customer .customerLeft{ float:left; width:50%;}
.pcConfig-customer .customerRight{ float:left}
.pcConfig-customer p{ padding:1px 0px;}
.footer-left{ float:left; width:75%;}
.footer-right{ float:left; font-weight:bold;}
.footer-left p{ padding:1px 0px;}
.footer-right p{ padding:1px 0px;}
.currencyVND{ display:none;}
.pcSKU{ padding:3px; color:#666666;}
.pcOffer{ padding-top:5px; color:Red;}
#tb_config { width:100%; border-collapse:collapse;}
#tb_config td { padding:6px;}
#row_config { background-color:#EEEEEE; font-weight:bold;}
.btn_custom {background: #d2c70a;padding: 5px 23px;font-size: 17px;color: #fff;text-decoration: none;border-radius: 5px;border: 2px solid #ececec;text-shadow: 0 0 1px #989898;}
</style>

<div class="container">
  {!! Form::open(array(
      'route' => 'save.order',
      'class' => 'form-horizontal',
      'novalidate' => 'novalidate'))!!}
    {!! csrf_field()!!}
        <table width="800px" align="center">        
            <tbody><tr>
                <td style="padding:10px;">
                    <div class="pcConfig-header">
                        <p class="pcConfig-logo">
                            <a href="{{URL::route('home')}}"><img src="{{URL::asset('images/web/'.$options['logo'])}}" alt="{{$options['title']}}" /> </a>
                        </p>
                        <p class="pcConfig-address">
                            <b>CÔNG TY TNHH THƯƠNG MẠI VÀ DỊCH VỤ TIN HỌC VIỆT HƯNG</b><br>
                            47 Lương Thế Vinh - Trung Văn - Từ Liêm - Hà Nội<br>
                            Điện thoại: 098 698 492 - 097 930 9809.<br>
                            Website: http://viethungcomputer.vn/
                        </p>
                    </div>
                    <div id="pnSendFlase">
	
                    <div class="clear"></div>
                    <div class="pcConfig-title">
                        <div>
                            <h1 class="cssH1">MUA HÀNG</h1></div>
                          <div style="font-size:11px;" class="time"></div>
                    </div>                    
                    <div class="clear"></div>
                    <div class="pcConfig-hello">
                        Thông tin của quý khách:
                    </div>
                    <div class="clear"></div>
                    <div class="pcConfig-customer">
                        <div class="customerLeft">
                            <p><lable style="width: 70px; display:inline-block">Ông/bà: </lable><input type="text" name="c_full_name" id="input" class="form-control" value="" required="required" pattern="" title=""></p>
                            <p><lable style="width: 70px; display:inline-block">Địa chỉ: </lable><input type="text" name="c_address" id="input" class="form-control" value="" required="required" pattern="" title=""></p>
                            <p><lable style="width: 70px; display:inline-block">Đơn vị: </lable><input type="text" name="c_company" id="input" class="form-control" value="" required="required" pattern="" title=""></p>
                        </div>
                        <div class="customerRight">
                            <p><lable style="width: 70px; display:inline-block">Email: </lable><input type="text" name="c_email" id="input" class="form-control" value="" required="required" pattern="" title=""></p></p>
                            <p><lable style="width: 70px; display:inline-block">Điện thoại: </lable><input type="text" name="c_phone" id="input" class="form-control" value="" required="required" pattern="" title=""></p>
                            <p><lable style="width: 70px; display:inline-block">Fax: </lable><input type="text" name="c_fax" id="input" class="form-control" value="" required="required" pattern="" title=""></p></p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="pcConfig-hello">
                        CÔNG TY TNHH THƯƠNG MẠI VÀ DỊCH VỤ TIN HỌC VIỆT HƯNG xin trân trọng gửi tới quý khách hàng bảng chào giá sau :
                    </div>
                    

                   

                    <table id="tb_config" cellpading="0" cellspacing="0" border="1" bordercolor="#999999">
                    <tbody><tr id="row_config">
                    	<td style="text-align:center; width:12px; ">STT</td>
                        <td>Tên sản phẩm</td>
                        <td style="text-align:center; width:20px;">Số lượng</td>
                        <td style="text-align:center; width:80px;">Đơn giá (VNĐ)</td>
                        <td style="text-align:center; width:80px;">Thành tiền (VNĐ)</td>
                        <td style="text-align:center; width:30px;">Xóa</td>
                     </tr>
                    @if(isset($list_products) && count($list_products) > 0)
                        @foreach($list_products as $key => $product)
                        <?php if(($product->main_price) == 1) {
                                $price = 'Liên hệ';
                            }else if($product->main_price > 1){
                                $price = $product->main_price;
                            }else{
                                $price = $product->price;
                            }
                        ?>
                        <tr class="parent_{{$product->id}}">
                            <td>{{$key+1}}</td>
                            <td>
                                <p class="pcName"><a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" target="_blank">{{$product->name}}</a></p>
                                <p class="pcSKU"><b>Mã SP: </b>{{$product->name}}</p>
                                <p class="pcSummary"></p>                                
                            </td>
                            <td><input type="text" value="1" style="width:50px" name="Product[{{$product->id}}]" rel="{{$price}}" relclass="cssTotal{{$key}}" class="cssCount quantity_confiq_{{$key}}"> </td>
                            <td style="text-align:right; padding-right:5px;">
                                {{adddotNumber($price)}}
                             </td>
                            <td style="text-align:right; padding-right:5px;"><span class="cssTotal{{$key}}" relbefore="{{$price}}">{{adddotNumber($price)}}</span></td>
                            <td><img src="{!! URL::asset('images/web/trash.png') !!}" width="20" onclick='delete_product({{$product->id}})' /></td>
                       </tr>
                       @endforeach
                    @else


                    @endif
              
                        <tr>
                            <td colspan="4" align="right"><b>Tổng tiền : </b></td>
                            <td colspan="2" style=" font-weight:bold;">
                            	<span id="TotalPrice" rel="0">0</span> VNĐ
                             </td>
                        </tr>
                       </tbody></table></div>    
                    
                    <div align="center" style="padding:10px 0px;">
                        &nbsp;
                        <button type="submit" class="btn_custom">Đặt mua</button>
                        <button type="button" class="btn_custom" onclick="javascript:window.print();">In cấu hình</button>         
                    </div>                    

                    <div class="clear"></div>
                    <div class="footer-left">
                        <p><b>Cung cấp:</b> Tại Hà Nội ngay sau khi khách hàng thanh toán</p>
                        <p><b>Thanh toán:</b> Tiền mặt, séc hoặc chuyển khoản</p><br>
                    </p></div>
                </td>
            </tr>
        </tbody></table>
    {!! Form::close() !!}
</div><!--container-->

<div class="clear"></div>
@section('scripts')
    <script type="text/javascript">
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " - "
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
        $('.time').html(datetime);

        $(".cssCount").change(function(){
                var _count = $(this).val(); // Số lượng được thay đổi
                if(parseInt(_count) > 0 && parseInt(_count) < 99)
                {
                    //Thành tiền cho từng sản phẩm
                    var _totalitem = parseFloat($(this).attr("rel")) * parseFloat(_count);
                    var _relclass = $(this).attr("relclass");
                    $("."+_relclass+"").html(commaSeparateNumber(_totalitem));
                    
                    var _totalitembefore = $("."+_relclass+"").attr("relBefore"); // Thành tiền sản phẩm trước
                    $("."+_relclass+"").attr("relBefore",_totalitem);
                    
                    var _totalpriceold = parseFloat($("#TotalPrice").attr("rel")); //tong gia tien ban dau cu
                    
                    //tong tien da duoc xu ly(tong tien cu + (tong tien cua 1 san pham sau - tong tien san pham truoc))
                    var _totalpricenew;
                    if(parseFloat(_totalitem) > parseFloat(_totalitembefore))
                        _totalpricenew = parseFloat(_totalpriceold) + (parseFloat(_totalitem) - parseFloat(_totalitembefore));
                    else
                    {
                        _totalpricenew = parseFloat(_totalpriceold) - (parseFloat(_totalitembefore) - parseFloat(_totalitem));
                    }
                    
                    $("#TotalPrice").html(commaSeparateNumber(_totalpricenew));
                    
                    $("#TotalPrice").attr("rel",_totalpricenew);
                }
                else
                    alert('Bạn vui lòng 99 < nhập số > 0!');
                
                
            });


        function commaSeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
              val = val.toString().replace(/(\d+)(\d{3})/, '$1'+'.'+'$2');
            }
            return val;
          }

        sumjq = function(selector) {
            var sum = 0;
            $(selector).each(function() {
                total_price = $(this).attr('rel');
                //order = total_price.replace(/\./g, '');
                sum += Number(total_price);
            });
            return sum;
        }
        $('#TotalPrice').attr('rel', sumjq(".cssCount")).html(commaSeparateNumber(sumjq(".cssCount")));

        function delete_product(id){
            $('tr.parent_'+id).remove();
            $('#TotalPrice').attr('rel', sumjq(".cssCount")).html(commaSeparateNumber(sumjq(".cssCount")));
        }
    </script>
@stop
@stop