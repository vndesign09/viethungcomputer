@extends('layouts.main_layout')
@section('content')
<div class="container">
 <div id="content_left">
   <div class="box_left">
       <div class="title_box_left">Danh mục tin tức</div>
       <div class="content_box_left list_cat_news">
            <ul class="ul">
                @foreach($categories_new as $name => $value)
                <li><a href="{{URL::route('category.news', ['alias'=>$value->alias,'id'=>$value->id])}}">{!!$value->name!!}</a></li>
                @endforeach
            </ul>
        </div>
   </div><!--box_left-->
   <div class="box_left">
       <div class="title_box_left">Tin xem nhiều nhất</div>
       <div class="content_box_left list_hot_news">
           <div class="bg_gradient_bottom_title"></div>
           <ul class="ul">
             @foreach($news_count as $index => $value)
               <li>
                   <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">
                       {!! Html::image('images/news/'.$value->thumbnail, $value->title , ['width' => '193px']) !!}
                       <span>{!!$value->title!!}</span>
                   </a>
               </li>
              @endforeach
           </ul>
       </div>
   </div><!--box_left-->
   <div class="banner_left">

 </div>
</div><!--content_left-->
<div id="content_news_page">
   <div class="title_box_center">
       <h1 class="h_title cufon" style="font-size:22px !important;">{{$cat->name}}</h1>

       <div class="paging">
           {{$list_news->render()}}
       </div><!--paging-->

   </div>
   <div class="space2"></div>
   <div id="top_news_on_page">

        @foreach($list_news as $index => $value)
          @if($index < 5)
            @if($index == 0)
               <a href="{{URL::route('details.news', ['alias'=>$value->alias,'id'=>$value->id])}}" class="first">
                   {!! Html::image('images/news/'.$value->thumbnail, $value->title) !!}
                   <span class="container">
                       <span class="name">{!!$value->title!!}</span>
                       <span class="view">{!!$value->created_at!!} - Lượt xem: {!!$value->counter!!}</span>
                   </span>
               </a>
               @if(count($list_news) > 1)
                 <ul class="ul">
               @endif
             @else
                 <li>
                     <a href="{{URL::route('details.news', ['alias'=>$value->alias,'id'=>$value->id])}}">
                       {!! Html::image('images/news/'.$value->thumbnail, $value->title) !!}
                     </a>
                     <div>
                     <a href="{{URL::route('details.news', ['alias'=>$value->alias,'id'=>$value->id])}}" class="name">{!!$value->title!!}</a>
                     <span class="view">{!!$value->created_at!!} - Lượt xem: {!!$value->counter!!}</span>
                     <span class="summary">{!!$value->desc!!}</span>
                     </div>
                 </li>
               @if($index == count($list_news))
                  </ul>
               @endif
             @endif
          @endif
        @endforeach
   </div><!--top-->
   <div class="clear space2"></div>
   <div id="list_news">
       <ul class="ul">

         @foreach($list_news as $index => $value)
           @if($index > 5)
           <li>
               <a href="{{URL::route('details.news', ['alias'=>$value->alias,'id'=>$value->id])}}">
                 {!! Html::image('images/news/'.$value->thumbnail, $value->title) !!}
               </a>
               <a href="{{URL::route('details.news', ['alias'=>$value->alias,'id'=>$value->id])}}" class="name">CÙNG DELL CHẮP CÁNH ƯỚC MƠ</a>
               <span class="view">21-03-2016, 3:02 pm - Lượt xem: 84</span>
               <div class="summary">
                   <a href="{{URL::route('details.news', ['alias'=>$value->alias,'id'=>$value->id])}}">Xem chi tiết &raquo;</a>
               </div>
           </li>
           @endif
        @endforeach

       </ul>
   </div><!--list_news-->

   <div class="paging">

     {{$list_news->render()}}

   </div><!--paging-->

</div><!--content_news-->
</div><!--container-->

<div class="clear"></div>
@stop
