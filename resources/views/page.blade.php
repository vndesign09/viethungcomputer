@extends('layouts.main_layout')
@section('content') 
<div class="container">
  <div id="content_center">
  <div class="title_box_center">
        <h1 class="h_title cufon" style="font-size:22px !important;">
          {{$page->title}}
        </h1>
    </div>
    {!!$page->contents!!}
    </div>
    <div id="content_right">
      <div class="box_right filter">
            <div class="title_box_right">
                <h2>Sản phẩm bán chạy</h2>
            </div>
            <ul class="ul best_sale">
                  @if(count($product_best_sale) >0 )
                  @foreach($product_best_sale as $key => $product)
                      <li class="nomar_l" style="position: relative; border: 1px solid #dcdcdc !important; text-align: center; margin: 0; padding: 0 !important">
                        <div class="p_container">
                          <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" style="font-size: 14px !important; text-decoration: none;">{!!$product->name!!}</a>
                          <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}"><img src="{!!URL::asset('images/products/'.$product->thumbnail)!!}"></a>
                          @if($product->sale > 0)
                          <div class="p_price" style="top: inherit;bottom: 35px;left: 70px; font-size: 18px;">{!!adddotNumber($product->main_price)!!}</div>
                          <div class="container_old_price">
                            <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                            <div class="price_off">-{!!$product->sale!!}%</div>
                          </div>
                          @else
                          <div class="p_price" style="top: inherit;bottom: 10px;left: 70px; font-size: 18px;">{!!adddotNumber($product->main_price)!!}</div>
                          @endif
                          <div class="clear"></div>
                          <div class="p_quantity">
                              
                                  <i class="bg icon_in_stock"></i>
                              
                          </div>
                          <!--div class="p_compare">
                            <input type="checkbox" name="/media/product/50_31530_ideacentre_all_in_one_c20_00.jpg" class="p_check" id="compare_box_31530" onclick="add_compare_product(31530);" />
                          </div-->
                      </div><!--wrap_pro-->
                      <div class="hover_content_pro tooltip">
                        <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="hover_name">{!!$product->name!!}</a>
                        
                        <div class="hori_line"></div>
                        <table>
                            <tr>
                                <td><b>Giá bán:</b></td>
                                <td>
                                    <div class="img_price_full">{!!$product->main_price!!}</div>
                                    <div class="container_old_price">
                                        @if($product->sale > 0)
                                        <div class="p_old_price">{!!$product->price!!}</div>
                                        <div class="price_off">-{!!$product->sale!!}%</div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @if($product->brand_id > 0)<tr><td><b>Hãng sản xuất:</b> {!!$product->brand->name!!}</td></tr>@endif
                            @if($product->guarantee_info != '')
                            <tr><td><b>Bảo hành:</b></td><td>{!!$product->guarantee_info!!}</td></tr>
                            @endif
                            <tr>
                              <td><b>Kho hàng:</b></td>
                              <td>{!!$product->warehouse_info!!}</td>
                            </tr>
                        </table>
                        <div class="hori_line"></div>
                        <div class="hover_offer">
                            <b>Mô tả tóm tắt:</b><br/>
                            {!!$product->desc_short!!}
                        </div>
                        
                    </div><!--hover_content_pro-->
                  </li>
                @endforeach
                @endif
          </ul>
        </div>
    </div>
</div>
<div class="clear"></div>
@stop