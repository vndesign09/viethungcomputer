@extends('layouts.main_layout')
@section('content')
<div class="container">
<div id="pro_detail_page">
    <div id="product_detail">
       <div id="wrap_scroll" style="width:350px; float:left;">
        <div id="img_detail">
            <div id="img_large">
             
              <?php $size = getimagesize('images/products/'.$product->thumbnail.'');?>
              @if($size[0] < 500)
               <a class="MagicZoom" data-options="zoomPosition: inner"  id="Zoomer" rel="selectors-effect-speed: 600" href="{!!asset('images/products/large-'.$product->thumbnail.'')!!}" >
              <img src="{!!asset('images/products/large-'.$product->thumbnail.'')!!}" alt="{!!$product->name!!}" />
              @else
               <a class="MagicZoom" data-options="zoomPosition: inner"  id="Zoomer" rel="selectors-effect-speed: 600" href="{!!asset('images/products/'.$product->thumbnail.'')!!}" >
              <img src="{!!asset('images/products/'.$product->thumbnail.'')!!}" alt="{!!$product->name!!}" />
              @endif
              </a>
            </div>
            <?php $galleries = json_decode($product->gallery);?>
            @if(isset($galleries) && count($galleries) > 0)
              <ul class="gallery_img">
                <?php $size = getimagesize('images/products/'.$product->thumbnail.'');?>
                @if($size[0] < 500)
                <li style="display: none;"><a class="img_thumb" data-options="zoomPosition: inner" rel="zoom-id:Zoomer;" href="{!!asset('images/products/large-'.$product->thumbnail.'')!!}" rev="{!!asset('images/products/large-'.$product->thumbnail.'')!!}"><img class='thumbnail' src="{!!asset('images/products/large-'.$product->thumbnail.'')!!}" width="100%" /></a></li>
                @else
                <li style="display: none;"><a class="img_thumb" data-options="zoomPosition: inner" rel="zoom-id:Zoomer;" href="{!!asset('images/products/'.$product->thumbnail.'')!!}" rev="{!!asset('images/products/'.$product->thumbnail.'')!!}"><img class='thumbnail' src="{!!asset('images/products/'.$product->thumbnail.'')!!} width="100%" "/></a></li>
                @endif
              @foreach($galleries as $image)

                <?php $size = getimagesize('images/products/'.$image.'');?>
                @if($size[0] < 500)
                <li><a class="img_thumb" data-options="zoomPosition: inner" rel="zoom-id:Zoomer;" href="{!! URL::asset('images/products/large-'.$image) !!}" rev="{!! URL::asset('images/products/large-'.$image) !!}"><img class='thumbnail' src="{!! URL::asset('images/products/large-'.$image) !!}" width="100%" /></a></li>
                @else
                <li><a class="img_thumb" data-options="zoomPosition: inner" rel="zoom-id:Zoomer;" href="{!! URL::asset('images/products/'.$image) !!}" rev="{!! URL::asset('images/products/'.$image) !!}"><img class='thumbnail' src="{!! URL::asset('images/products/'.$image) !!}" width="100%" /></a></li>
                @endif
              @endforeach
              </ul>
            @endif
            <div class="name_show" style="display:block">{!! $product->name !!}</div>
            <div class="clear"></div>
            <div id="price_deal_detail_2">
                  <div class="img_price_full">
                    @if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                      @if($product->sale > 0)<p>Giá chính hãng: <span class="line_through">{!!adddotNumber($product->price)!!} đ</span>
                      <span class="percent_off">-{!!$product->sale!!}%</span></p>@endif
                      @if($product->save_money > 0)<p class="red">Tiết kiệm: {!!adddotNumber($product->save_money)!!} đ</p>@endif
                  
              </div>
              <div class="clear"></div>
        </div><!--img_detail-->
         
      </div><!--wrap_scroll-->
        <div id="overview">
            <h1>{!! $product->name !!}</h1>
            <div class="clear"></div>
          <div class="fb-like" data-href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div> 
          <div class="clear"></div>
          <p class="float_l red">Mã SP: {!!$product->code!!}</p>
          <p class="float_r">Lượt xem: {!!$product->counter!!}</p>
            <div class="hori_line"></div>
            <div class="table_div">
                <div class="cell">
                    <table>
                        <b>Mô tả sản phẩm: </b>
                        <tr>
                          <td>
                              <div id="summary_detail">
                                 {!!$product->desc_short!!}                                  
                              </div><!--summary_detail-->
                          </td>
                        </tr>
                      
                        <tr>
                        <td>
                          <b>Kho hàng: </b>
                              {!!$product->warehouse_info!!}    
                        </td>
                        </tr>
                         @if($product->brand_id > 0)
                          <tr>
                            <td><b>Hãng sản xuất:</b></td>
                            <td>{!!$product->brand->name!!}</td>
                          </tr>
                          @endif
                        @if($product->guarantee_info != '')
                        <tr>
                          <td><b>Bảo hành:</b> {!!$product->guarantee_info!!}<br/> </td>
                        </tr>
                        @endif
                        <tr>
                          <td><b>Giao hàng:</b> <br/> 
                              {!!$product->ship_info!!}
                          </td>
                        </tr>
                    </table>
                </div><!--cell-->
                <div class="space2"></div>
              <div id="price_detail">
                    <div class="img_price_full" style="float:left;">
                      @if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif
                    </div>
                    <div class="clear"></div>
                     @if($product->sale > 0)<p>Giá chính hãng: <span class="line_through">{!!adddotNumber($product->price)!!} đ</span>
                     <span class="percent_off">-{!!$product->sale!!}%</span></p>@endif
                      @if($product->save_money > 0)<p class="red">Tiết kiệm: {!!adddotNumber($product->save_money)!!} đ</p>@endif
              </div><!--price_detail-->
            </div><!--table_div-->
            <div class="space"></div>
            <!-- AddThis Button BEGIN -->

            <script>
            $(document).ready(function(){
                $("#title_tab_scroll_pro a").click(function(){
                    $("#title_tab_scroll_pro a").removeClass("current");
                    $(this).addClass("current");
                
                    //$(".content_scroll_tab").hide();
                    //$($(this).attr("href")).show();
                
                $('body,html').animate({scrollTop:$($(this).attr("href")).offset().top - 70},800);
                    return false;
                });
              var get_top = 0;
              if(get_top == 0) get_top = $("#title_tab_scroll_pro").offset().top;
              
              $(window).scroll(function(){
                if($(window).scrollTop() > get_top - 80) $("#title_tab_scroll_pro").addClass("fixed");
                else $("#title_tab_scroll_pro").removeClass("fixed");
              });
          
              $(".btn_image_link").click(function(){
                $('body,html').animate({scrollTop:$("#tab2").offset().top - 40},800);
                return false;
              });
              $(".btn_video_link").click(function(){
                $('body,html').animate({scrollTop:$("#tab6").offset().top - 40},800);
                return false;
              });
        $("#go_comment").click(function(){
                $('body,html').animate({scrollTop:$("#tab5").offset().top - 40},800);
                return false;
              });
            });
        </script>
        <div id="title_tab_scroll_pro">
           <!-- <a href="#tab1">Thông số</a> -->
            <!-- <a href="#tab2">Hình ảnh</a> -->
            <a href="#tab4" class="current">Mô tả chi tiết</a>
            <!-- <a href="#tab5">Video</a> -->
            <a href="#tab3">Driver & Software</a>
            <a href="#tab6">Đánh giá</a>
            <a href="#tab7">Sản phẩm liên quan</a>
        </div><!--title_tab_scroll_pro-->
        <div class="clear"></div>
        <div id="tab1" class="content_scroll_tab">
            <p> </p>
<!--?php echo stripslashes($spec);?-->
            <div class="clear"></div>
        </div><!--content_scroll_tab-->

        <div id="tab4" class="content_scroll_tab" style="display:block;">
            <h2 class="cufon title_box_scroll">Mô tả chi tiết</h2>
            {!!$product->desc_main!!}
            <div class="clear"></div>
        </div><!--content_scroll_tab-->
        <div id="tab3" class="content_scroll_tab">
            <h2 class="cufon title_box_scroll">Driver & Software</h2>
            
            <div class="clear"></div>
        </div><!--content_scroll_tab-->
        <div id="tab6" class="content_scroll_tab">
            <h2 class="cufon title_box_scroll">Đánh giá</h2>
                <div class="fb-comments" data-href="{{URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])}}" data-width="100%" data-numposts="10" ></div>
            <div class="clear"></div>
        </div><!--content_scroll_tab-->
        <div id="tab7" class="content_scroll_tab related_products">
            <h2 class="cufon title_box_scroll">Sản phẩm liên quan</h2>
      <div class="product_list page_inside">
              <ul class="ul">
                  @if(count($related_product) >0 )
                  @foreach($related_product as $key => $product)
                      <li class="nomar_l" style="width:187px; margin-left:6px;">
                          <div class="p_container">
                              <div class="p_sku">Mã SP: {!!$product->code!!}</div>
                              <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_img"><img src="{!!URL::asset('images/products/'.$product->thumbnail)!!}">
                            </a>
                          <div class="p_price">
                           @if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif
                          </div>
                                    @if($product->sale > 0)
                                    <div class="container_old_price">
                                      
                                        <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                                        <div class="price_off">-{!!$product->sale!!}%</div>
                                        
                                    </div>
                                    @endif
                          <div class="clear"></div>
                          <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
                          <div class="p_quantity">
                              
                                  <i class="bg icon_in_stock"></i>
                              
                          </div>
                          <!--div class="p_compare">
                            <input type="checkbox" name="/media/product/50_31530_ideacentre_all_in_one_c20_00.jpg" class="p_check" id="compare_box_31530" onclick="add_compare_product(31530);" />
                          </div-->
                      </div><!--wrap_pro-->
                      <div class="hover_content_pro tooltip">
                        <a href="{!!URL::route('details.product', ['alias'=>$product->alias,'id'=>$product->id])!!}" class="hover_name">{!!$product->name!!}</a>
                        
                        <div class="hori_line"></div>
                        <table>
                            <tr>
                                <td><b>Giá bán:</b>
                                <td>
                                    <div class="img_price_full">
                                     @if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif
                                </td>
                            </tr>
                             @if($product->brand_id > 0)
                          <tr>
                            <td><b>Hãng sản xuất:</b></td>
                            <td>{!!$product->brand->name!!}</td>
                          </tr>
                          @endif
                            @if($product->guarantee_info != '')
                            <tr><td><b>Bảo hành:</b></td><td>{!!$product->guarantee_info!!}</td></tr>
                            @endif
                            <tr>
                              <td><b>Kho hàng:</b></td>
                              <td>{!!$product->warehouse_info!!}</td>
                            </tr>
                        </table>
                        <div class="hori_line"></div>
                        <div class="hover_offer">
                            <b>Mô tả tóm tắt:</b><br/>
                            {!!$product->desc_short!!}
                        </div>
                        
                    </div><!--hover_content_pro-->
                  </li>
                @endforeach
                @endif
          </ul>
          </div><!--prouduct_list-->
          <div class="clear"></div>
        </div><!--content_scroll_tab-->
        </div><!--overview-->
        
    </div><!--product_detail-->
</div><!--pro_detail_page-->
<div id="right_pro_page">
    
    <div class="box_right filter">
        <div class="title_box_right black">
            <h2>Danh mục</h2>
        </div>
        <div class="content_box">
            <div class="bg_gradient_bottom_title"></div>
            <ul class="ul">
                @foreach($list_category as $key => $cat)
                  <li><input class="" type="checkbox"  onclick="location.href='{!!URL::route('search', ['cat'=>$cat->id])!!}'"/> <a href="{!!URL::route('search', ['cat'=>$cat->id])!!}">{!!$cat->name!!}</a> <span>({!!count($cat->products)!!})</span></li>  
                @endforeach
            </ul>
        </div>
    </div><!--box_right-->
    
    
        <div class="box_right filter">
            <div class="title_box_right black">
                <h2>Khoảng giá</h2>
            </div>
            <div class="content_box">
                <div class="bg_gradient_bottom_title"></div>
                <ul class="ul">
                        
                        @for($i=8; $i<= 16; $i++)
                          @if($i==8)
                          <li><input class="" type="checkbox"  onclick="location.href='{!!URL::route('search', ['cat'=>$product->category_id, 'max'=> $i*1000000])!!}'"/> <a href="{!!URL::route('search', ['cat'=>$product->category_id, 'max'=> $i*1000000])!!}">Dưới {!!$i!!} triệu</a></li>  
                          @elseif($i==16)
                          <li><input class="" type="checkbox"  onclick="location.href='{!!URL::route('search', ['cat'=>$product->category_id, 'min'=> $i*1000000])!!}'"/> <a href="{!!URL::route('search', ['cat'=>$product->category_id, 'min'=> $i*1000000])!!}">Trên {!!$i!!} triệu</a></li>  
                          @else
                          <li><input class="" type="checkbox"  onclick="location.href='{!!URL::route('search', ['cat'=>$product->category_id, 'min'=>($i-1)*1000000,'max'=> $i*1000000])!!}'"/> <a href="{!!URL::route('search', ['cat'=>$product->category_id, 'min'=>($i-1)*1000000,'max'=> $i*1000000])!!}">{!!($i-1)!!} triệu - {!!$i!!} triệu</a></li>  
                          @endif
                        @endfor
                </ul>
            </div>
        </div><!--box_right-->
   
</div><!--right_pro_page-->
</div><!--container-->
<div class="clear"></div>
<link type="text/css" rel="stylesheet" href="{{URL::asset('resources/assets/index/css/magiczoom.css')}}">
@section('scripts')
  <script src="{{URL::asset('resources/assets/index/js/magiczoom.js')}}"></script>
  <script src="{{URL::asset('resources/assets/index/js/scrollToFixed.js')}}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $(".fancybox").fancybox({
        openEffect  : 'none',
        closeEffect : 'none'
      });
    });

  </script>
@stop
@stop