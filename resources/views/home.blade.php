@extends('layouts.main_layout')
@section('content')
@section('class_body')class="home"@stop
<div class="container">
<script>
$(document).ready(function(){
  $("#slider img").click(function(){
    src = $(this).attr("src");
    $("#fake_banner img[src='"+src+"']").hide();
    window.location = $("#fake_banner img[src='"+src+"']").parent("a").attr("href");
});
});
</script>

<div id="container_slider">
<div id="slider">
    @if(count($sliders) > 0)
        @foreach($sliders as $k => $slider)
        <img src="{{URL::asset('images/web/'.$slider)}}"/>
        @endforeach
    @endif
</div>
<div class="clear space"></div>
</div><!--container_slider-->
<div style="width: 980px;overflow: hidden;float: right; position:relative;" id="thumb_slider">
  <a href="" class="prevSlider"><img src="images/icon_prev.png" alt="prev" /></a>
  <a href="" class="nextSlider"><img src="images/icon_next.png" alt="next" /></a>

</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#slider').carouFredSel({
    scroll: {
    fx: 'crossfade',
    onBefore: function( data ) {
    //$('#thumbs').trigger( 'slideTo', [ $('#thumbs img[alt='+ data.items.visible.attr( 'alt' ) +']'), -2 ] );
    $('#thumbs span').removeClass("active");
    $('#thumbs span[rel='+ data.items.visible.attr( 'alt' ) +']').addClass("active");
    }
    }
  });

  $('#thumbs').carouFredSel({
    auto: {
      play: false,
      pauseOnHover: true
      },
    prev: '.prevSlider',
    next: '.nextSlider',
  });

  $('#thumbs span').mousemove(function(e) {
    $('#slider').trigger( 'slideTo', [ $('#slider img[alt='+ $(this).attr( 'rel' ) +']') ] );
    }).css( 'cursor', 'pointer' );
  });
</script>
<div class="clear space"></div>
<div id="banner_cs_home">

</div>
<div class="clear"></div>
<script>
    $(document).ready(function(){
        $(".title_tab a").click(function(){
            $(".title_tab a").removeClass("current");
            $(this).addClass("current");

            $(".cf").hide();
            $($(this).attr("href")).show();
            return false;
        });
    });
</script>
<div id="box_pro_special_home">
<div class="title_tab">
    <a href="#tab1" class="cufon a_tab current">Sản phẩm khuyến mãi</a>
    <a href="#tab2" class="cufon a_tab">Sản phẩm bán chạy</a>
    <a href="#tab3" class="cufon a_tab">Sản phẩm mới</a>
</div><!--title_tab-->
<div class="clear"></div>
<div class="content_tab">
<div class="product_list">
<ul id="tab1" style="display:block;" class="ul cf">
@foreach($product_km as $key=> $product)
    <li>
        
        @if($product->sale >0)<div class="bg iconSaleOff"></div><div class="icon-km"><span>KM</span></div>@endif
        <div class="p_container">
            <div class="p_sku">Mã SP: {!!$product->code!!}</div>
            <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_img"><img class="lazy" src="images/blank.gif" data-src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}"/></a>
            <div class="p_price">@if(($product->main_price) == 1) 
                              Liên hệ 
                            @elseif($product->main_price > 1)
                              {!!adddotNumber($product->main_price)!!} 
                            @else
                              {!!adddotNumber($product->price)!!} 
                            @endif</div>
            @if($product->sale >0)
            <div class="container_old_price">
                <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                <div class="price_off">-{!!$product->sale!!}%</div>
            </div>
            @endif
            <div class="clear"></div>
            <a href="/camera-ip-hikvision-ds-2cd2432f-iw-30mp/p28766.html" class="p_name">{!!$product->name!!}</a>
            <div class="p_quantity">

                    <i class="bg icon_in_stock"></i>

            </div>
        </div><!--wrap_pro-->
        <div class="hover_content_pro tooltip">
             <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="hover_name">{!!$product->name!!}</a>

            <div class="hori_line"></div>
            <table>
                <tr>
                    <td><b>Giá bán:</b></td>
                    <td>
                          <span class="img_price_full">@if(($product->main_price) == 1) 
                              Liên hệ 
                            @elseif($product->main_price > 1)
                              {!!adddotNumber($product->main_price)!!} 
                            @else
                              {!!adddotNumber($product->price)!!} 
                            @endif</span> 
                    </td>
                </tr>
                @if($product->brand_id > 0)
                    <tr>
                      <td><b>Hãng sản xuất:</b></td>
                      <td>{!!$product->brand->name!!}</td>
                    </tr>
                    @endif
                @if($product->guarantee_info != '')
                <tr><td><b>Bảo hành:</b></td><td>{!!$product->guarantee_info!!}</td></tr>
                @endif
                <tr>
                  <td><b>Kho hàng:</b></td>
                  <td>{!!$product->warehouse_info!!}</td>
                 </tr>
            </table>

            <div class="hori_line"></div>
            <div class="hover_offer">
                <b>Mô tả tóm tắt:</b><br/>
                {!!$product->desc_short!!}
            </div>
        </div><!--hover_content_pro-->
    </li>
@endforeach
   
</ul>
<ul id="tab2" class="ul cf">
@foreach($product_best_sale as $key => $product)
    <li class="nomar_l">
        <div class="bg iconNew"></div>
        @if($product->sale > 0)
        <div class="icon-km"><span>KM</span></div>
        @endif
         <div class="p_container">
            <div class="p_sku">Mã SP: {!!$product->code!!}</div>
            <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_img"><img class="lazy" src="images/blank.gif" data-src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}"/></a>
            <div class="p_price">@if(($product->main_price) == 1) 
                              Liên hệ 
                            @elseif($product->main_price > 1)
                              {!!adddotNumber($product->main_price)!!} 
                            @else
                              {!!adddotNumber($product->price)!!} 
                            @endif</div>
            @if($product->sale > 0)
            <div class="container_old_price">
                
                <div class="p_old_price">{!!$product->price!!}</div>
                <div class="price_off">-{!!$product->sale!!}%</div>
                
            </div>
            @endif
            <div class="clear"></div>
            <a href="/camera-ip-hikvision-ds-2cd2432f-iw-30mp/p28766.html" class="p_name">{!!$product->name!!}</a>
            <div class="p_quantity">

                    <i class="bg icon_in_stock"></i>

            </div>
        </div><!--wrap_pro-->
        <div class="hover_content_pro tooltip">
             <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="hover_name">{!!$product->name!!}</a>

            <div class="hori_line"></div>
            <table>
                <tr>
                    <td><b>Giá bán:</b></td>
                    <td class="img_price_full">
                          <span class="img_price_full">
                              @if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif
                          </span> 
                    </td>
                </tr>
                @if($product->brand_id > 0)
                    <tr>
                      <td><b>Hãng sản xuất:</b></td>
                      <td>{!!$product->brand->name!!}</td>
                    </tr>
                    @endif
                @if($product->guarantee_info != '')
                <tr><td><b>Bảo hành:</b></td><td>{!!$product->guarantee_info!!}</td></tr>
                @endif
                <tr>
                  <td><b>Kho hàng:</b></td>
                  <td>{!!$product->warehouse_info!!}</td>
                 </tr>
            </table>

            <div class="hori_line"></div>
            <div class="hover_offer">
                <b>Mô tả tóm tắt:</b><br/>
                {!!$product->desc_short!!}
            </div>
        </div><!--hover_content_pro-->
    </li>
@endforeach

</ul>
<ul id="tab3" class="ul cf">
@foreach($product_new as $key => $product)
    <li class="nomar_l">
        <div class="bg iconNew"></div>
        @if($product->sale > 0)
        <div class="icon-km"><span>KM</span></div>
        @endif
         <div class="p_container">
            <div class="p_sku">Mã SP: {!!$product->code!!}</div>
            <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="p_img"><img class="lazy" src="images/blank.gif" data-src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}"/></a>
            <div class="p_price">
              @if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif
            </div>
            @if($product->sale > 0)
            <div class="container_old_price">
                
                <div class="p_old_price">{!!$product->price!!}</div>
                <div class="price_off">-{!!$product->sale!!}%</div>
                
            </div>
            @endif
            <div class="clear"></div>
            <a href="/camera-ip-hikvision-ds-2cd2432f-iw-30mp/p28766.html" class="p_name">{!!$product->name!!}</a>
            <div class="p_quantity">

                    <i class="bg icon_in_stock"></i>

            </div>
        </div><!--wrap_pro-->
        <div class="hover_content_pro tooltip">
             <a href="{!!URL::route('details.product', ['alias'=>$product->alias, 'id'=>$product->id])!!}" class="hover_name">{!!$product->name!!}</a>

            <div class="hori_line"></div>
            <table>
                <tr>
                    <td><b>Giá bán:</b></td>
                    <td class="img_price_full">
                          <span class="img_price_full">
                            @if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif
                          </span> 
                    </td>
                </tr>
                @if($product->brand_id > 0)
                <tr>
                  <td><b>Hãng sản xuất:</b></td>
                  <td>{!!$product->brand->name!!}</td>
                </tr>
                @endif
                @if($product->guarantee_info != '')
                <tr><td><b>Bảo hành:</b></td><td>{!!$product->guarantee_info!!}</td></tr>
                @endif
                <tr>
                  <td><b>Kho hàng:</b></td>
                  <td>{!!$product->warehouse_info!!}</td>
                 </tr>
            </table>

            <div class="hori_line"></div>
            <div class="hover_offer">
                <b>Mô tả tóm tắt:</b><br/>
                {!!$product->desc_short!!}
            </div>
        </div><!--hover_content_pro-->
    </li>
@endforeach


</div><!--prouduct_list-->
</div><!--content_tab-->
</div><!--box_pro_special_home-->
<div id="hot_news_home" class="float_r">
      <div class="title_box_right"><h2 class="cufon h_title">Tin tức nổi bật</h2> </div>
      <div class="content_box" style="height:255px;">
        <div class="bg_gradient_bottom_title"></div>

        @foreach($news_new as $index => $value)
          @if($index == 0)
            <div class="top_news">
                <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}">{!! Html::image('images/news/'.$value->thumbnail, $value->title) !!}</a>
                <a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}"> &raquo; {!!$value->title!!}</a>
            </div>
            @if(count($news_new) > 1)
              <ul>
            @endif
          @else
            <li><a href="{{URL::route('details.news', ['alias'=>$value['alias'],'id'=>$value['id']])}}"> &raquo; {!!$value->title!!}</a></li>
            @if($index == count($news_new))
              </ul>
            @endif
          @endif
        @endforeach
    </div>
  </div><!--hot_news_home-->
<div class="clear"></div>
<!--SHOW LIST CATEGORY-->
@foreach($list_cate as $parent => $child)
<div class="title_box_center">
    <h2 class="cufon h_title">{!!$parent!!}</h2>
    <div class="sub_cat_title">
        @foreach($child['child']['cat_child'] as $child_n)
             <a href="{!!URL::route('category.product', ['alias' => $child_n['alias'], 'id' => $child_n['id']])!!}">{!!$child_n['name']!!}</a>
        @endforeach
    </div>
    <a href="{!!URL::route('category.product', ['alias'=> $child['alias'], 'id'=> $child['id']])!!}" class="viewall">Xem tất cả</a>
</div><!--title_box_center-->
<div class="bg_gradient_bottom_title"></div>
<div class="banner_top_pro"></div>
<div class="clear space"></div>
<div class="product_list float_l">
    <ul class="ul">
    @foreach($child['child']['products'] as $product)
        <li>
            @if($product->sale > 0)<div class="icon-km"><span>KM</span></div>@endif
            <div class="p_container">
                <div class="p_sku">Mã SP: {!!$product->code!!}</div>
                <a href="{!!URL::route('details.product', ['alias'=> $product->alias, 'id'=>$product->id])!!}" class="p_img"><img class="lazy" src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" data-src="{!!URL::asset('images/products/'.$product->thumbnail)!!}" alt="{!!$product->name!!}"></a>
                <div class="p_price">@if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif</div>
                
                <div class="container_old_price">
                    @if($product->sale > 0)
                    <div class="p_old_price">{!!adddotNumber($product->price)!!}</div>
                    <div class="price_off">-{!!$product->sale!!}%</div>
                    @endif
                </div>
                
                <div class="clear"></div>
                <a href="{!!URL::route('details.product', ['alias'=> $product->alias, 'id'=>$product->id])!!}" class="p_name">{!!$product->name!!}</a>
                <div class="p_quantity"><i class="bg icon_in_stock"></i></div>
            </div><!--wrap_pro-->
            <!--0-->
            <div class="hover_content_pro tooltip">
                
            <a href="$" class="hover_name">{!!$product->name!!}</a>
                <div class="hori_line"></div>
                <table>
                    <tr>
                        <td><b>Giá bán:</b></td>
                        <td>
                            <span class="img_price_full">@if(($product->main_price) == 1) 
                              Liên hệ 
                              @elseif($product->main_price > 1)
                                {!!adddotNumber($product->main_price)!!} 
                              @else
                                {!!adddotNumber($product->price)!!} 
                              @endif</span>
                        </td>
                    </tr>
                    @if($product->brand_id > 0)
                    <tr>
                      <td><b>Hãng sản xuất:</b></td>
                      <td>{!!$product->brand->name!!}</td>
                    </tr>
                    @endif
                    @if($product->guarantee_info != '')
                    <tr><td><b>Bảo hành:</b></td><td>{!!$product->guarantee_info!!}</td></tr>
                    @endif
                    <tr>
                      <td><b>Kho hàng:</b></td>
                       <td> {!!$product->warehouse_info!!}</td>
                    </tr>
                </table>

                <div class="hori_line"></div>
                <div class="hover_offer">
                    <b>Mô tả tóm tắt:</b><br/>
                    {!!$product->desc_short!!}
                </div>
            </div><!--hover_content_pro-->
        </li>
    @endforeach
    </ul>
</div><!--prouduct_list-->
<div class="clear space"></div>
<div class="clear"></div>
@endforeach

<!--END SHOW LIST CATEGORY-->


</div><!--container-->
<div class="clear"></div>
@endsection
