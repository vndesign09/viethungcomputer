@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Thêm sản phẩm @stop
@include('layouts.notifications')
@section('title_page')Thêm Sản phẩm mới @stop
{!! Form::open(
    array(
        'route' => 'prod.add_post', 
        'class' => 'form-horizontal', 
        'novalidate' => 'novalidate', 
        'files' => true)) !!}
		{!! csrf_field() !!}
		<div class="col-sm-8">
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Tên SP<abbr>*</abbr></label>
				<div class="col-sm-8">
					{{Form::text('name', '',['class'=>'form-control'] )}}
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Danh mục<abbr>*</abbr></label>
				<div class="col-sm-8">
					<?php getParentthong($cates,0,"",$list_cats);?>
					{{Form::select('category_id', $list_cats, '',['class'=>'form-control', 'placeholder'=>'Vui lòng chọn'])}}
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Mã SP<abbr>*</abbr></label>
				<div class="col-sm-3">
					{{Form::text('code', '',['class'=>'form-control'] )}}
				</div>
				<div class="col-sm-4">
					<div class="checkbox">
						<label>
							{{Form::checkbox('is_best_sale', '1')}}
							Sản phẩm bán chạy
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Hãng SX</label>
				<div class="col-sm-3">
					{{Form::select('brand_id',$brands, '',['class'=>'form-control', 'placeholder'=>'Vui lòng chọn'])}}
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Bảo hành</label>
				<div class="col-sm-8">
					
					{{Form::text('guarantee_info', '',['class'=>'form-control', 'placeholder'=>'Vd: 1 tháng, 5 tháng, 1 năm, 24 tháng, 30 tháng'] )}}
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Giá niêm yết<abbr>*</abbr></label>
				<div class="col-sm-3">
					{{Form::number('price', '',['class'=>'form-control', 'id'=>"price"] )}}
				</div>
				<label for="input-id" class="col-sm-2 control-label">Giá đã giảm</label>
				<div class="col-sm-3">
					{{Form::number('main_price', '',['class'=>'form-control', 'id'=>"main_price"] )}}
				</div>
			</div>
			<div class="form-group">
				<!-- <label for="input-id" class="col-sm-2 control-label">Tiết kiệm</label>
				<div class="col-sm-3">
					{{Form::number('save_money', '',['class'=>'form-control', 'id'=>"save_money"] )}}
				</div> -->
				<label for="input-id" class="col-sm-2 control-label">Giảm giá (%)</label>
				<div class="col-sm-3">
					{{Form::number('sale', '',['class'=>'form-control', 'id'=>"sale", 'disabled'] )}}
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="product_img" style="height:260px;">
				{!! Html::image('images/web/default.jpeg', 'default_img',['id' => 'blah', 'style'=>'max-height:100%;']) !!}
			</div>
			<div class="fileUpload btn btn-primary">
			    <span>Chọn ảnh đại diện cho SP</span>
			    {{Form::file('thumbnail',['class'=>'upload', 'id'=>"imgInp"] )}}
			</div>
			<div id="result">
				<div><img class='thumbnail' src="{!! URL::asset('images/web/default.jpeg') !!}"/></div>
				<div><img class='thumbnail' src="{!! URL::asset('images/web/default.jpeg') !!}"/></div>
				<div><img class='thumbnail' src="{!! URL::asset('images/web/default.jpeg') !!}"/></div>
			</div>
    		<div class="fileUpload btn btn-primary">
			    <span>Thêm hình ảnh phụ</span>
			    {!! Form::file('images[]', array('class'=>'upload','multiple'=>true, 'id'=>"uploadBtn")) !!}
			</div>
		</div>	
		<div class="clearfix"></div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Mô tả ngắn gọn<abbr>*</abbr></label>
			<div class="col-sm-12">
				{{Form::textarea('desc_short', '',['class'=>'tinymce form-control'] )}}
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Kho hàng</label>
			<div class="col-sm-12">
				{{Form::textarea('warehouse_info', '',['class'=>'tinymce form-control'] )}}
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Giao hàng</label>
			<div class="col-sm-12">
				{{Form::textarea('ship_info', '',['class'=>'tinymce form-control'] )}}
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Mô tả chi tiết<abbr>*</abbr></label>
			<div class="col-sm-12">
				{{Form::textarea('desc_main', '',['class'=>'tinymce form-control'] )}}
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Driver & Software</label>
			<div class="col-sm-12">
				{{Form::textarea('driversoftware_info', '',['class'=>'tinymce form-control'] )}}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
			</div>
		</div>

{!! Form::close() !!}
@section('scripts')
<script type="text/javascript">

	// Tính giá tiền sản phẩm sau khi giảm giá
	/*$('#save_money').keyup(function(event) {
		var price = $('#price').val();
		var save_money = $(this).val();
		$('#sale').val(Math.ceil(save_money*100/price));
		$('#main_price').val(price-save_money);
	});*/

	// Preview Product THumbnail
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
        document.getElementById("uploadFile").value = this.value;
    });


function handleFileSelect() {
    //Check File API support
    $('#result div').remove();
    if (window.File && window.FileList && window.FileReader) {

        var files = event.target.files; //FileList object
        var output = document.getElementById("result");

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            //Only pics
            if (!file.type.match('image')) continue;

            var picReader = new FileReader();
            picReader.addEventListener("load", function (event) {
                var picFile = event.target;
                var div = document.createElement("div");
                div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>";
                output.insertBefore(div, null);
            });
            //Read the image
            picReader.readAsDataURL(file);
        }
    } else {
        console.log("Your browser does not support File API");
    }
}

document.getElementById('uploadBtn').addEventListener('change', handleFileSelect, false);

$(".form-horizontal").ajaxForm({url: '{!! URL::route("prod.add_post") !!}', type: 'post'})
</script>
@stop
@stop