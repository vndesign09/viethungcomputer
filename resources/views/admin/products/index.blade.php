@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Danh sách sản phẩm @stop
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable( {
		    "iDisplayLength": 25,
		     "order": [[ 8, "desc" ]]
		});
	});
</script>
<a href="{!!URL::route('prod.add')!!}" class="btn btn-success">Thêm Sản phẩm</a>
@include('layouts.notifications')
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>#Mã</th>
			<th class="col-sm-1">Hình ảnh</th>
			<th class="col-sm-3">Tên</th>
			<th class="col-sm-2">Giá bán</th>
			<th class="col-sm-1">Giảm giá(%)</th>
			<th class="col-sm-2">Danh mục</th>
			<th class="col-sm-1">Lượt xem</th>
			<th style="display: none;"></th>
			<th class="col-sm-2">#</th>
		</tr>
	</thead>
	<tbody>
		@if(isset($products))
		@foreach($products as $key => $value)
			<tr>
				<td>{!! $value->code !!}</td>
				<td>{!! Html::image('images/products/'.$value->thumbnail, '', ['width' => '90px']) !!}</td>
				<td><a href="{!!URL::route('details.product', ['alias'=>$value->alias, 'id'=>$value->id])!!}" target="_blank">{!! $value->name !!}</a></td>
				<td><strong>{!! adddotNumber($value->main_price)!!}</strong></td>
				<td>@if($value->sale > 0)-{!! $value->sale !!}% @endif</td>
				<td>{!! $value->category->name !!}</td>
				<td>{!! $value->counter !!}</td>
				<td style="display: none;">{!! $value->created_at !!}</td>
				<td><a href='{{URL::route("prod.edit", $value->id)}}' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  
				<a href='{{URL::route("prod.delete", $value->id)}}' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td>
			</tr>
		@endforeach
		@endif
	</tbody>
</table>
@stop