@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Chỉnh sửa sản phẩm @stop
@include('layouts.notifications')
{!! Form::open(array('route' => ['prod.edit_post', $product->id], 'method'=> 'POST','class' => 'form-horizontal', 'novalidate' => 'novalidate', 'files' => true)) !!}
		{!! csrf_field() !!}
		<div class="col-sm-8">
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Tên SP<abbr>*</abbr></label>
				<div class="col-sm-8">
					<input type="text" name="name" id="input" class="form-control" value="{!!$product->name!!}">
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Danh mục<abbr>*</abbr></label>
				<div class="col-sm-8">
					<select name="category_id" id="input" class="form-control" required="required">
						<option value="">Chọn danh mục</option>
						<?php getCategories($cates,0,'',$product->category_id); ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Mã SP<abbr>*</abbr></label>
				<div class="col-sm-3">
					<input type="text" name="code" id="input" class="form-control" value="{!!$product->code!!}">
				</div>
				<div class="col-sm-4">
					<div class="checkbox">
						<label>
							@if($product->is_best_sale == 1)
								<input type="checkbox" name="is_best_sale" value="1" checked="checked">
							@else
								<input type="checkbox" name="is_best_sale" value="1">
							@endif
							
							Sản phẩm bán chạy
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Hãng SX</label>
				<div class="col-sm-3">
					{{Form::select('brand_id',$brands, $product->brand_id,['class'=>'form-control', 'placeholder'=>'Vui lòng chọn'])}}
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Bảo hành</label>
				<div class="col-sm-8">
					
					{{Form::text('guarantee_info', $product->guarantee_info ,['class'=>'form-control', 'placeholder'=>'Vd: 1 tháng, 5 tháng, 1 năm, 24 tháng, 30 tháng'] )}}
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Giá niêm yết<abbr>*</abbr></label>
				<div class="col-sm-3">
					<input type="number" name="price" id="price" class="form-control" value="{!!$product->price!!}">
				</div>
				<label for="input-id" class="col-sm-2 control-label">Giá đã giảm</label>
				<div class="col-sm-3">
					@if($product->main_price > 0)
					<input type="number" name="main_price" id="main_price" class="form-control" value="{!!$product->main_price!!}">
					@else
					<input type="number" name="main_price" id="main_price" class="form-control" value="">
					@endif
				</div>
			</div>
			<div class="form-group">
				<!-- <label for="input-id" class="col-sm-2 control-label">Tiết kiệm</label>
				<div class="col-sm-3">
					<input type="number" name="save_money" id="save_money" class="form-control" value="{!!$product->save_money!!}"> 
				</div> -->
				<label for="input-id" class="col-sm-2 control-label">Giảm giá (%)</label>
				<div class="col-sm-3">
					<input type="number" name="sale" id="sale" class="form-control" value="{!!$product->sale!!}" disabled> 
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="product_img" style="height:260px;">
				{!! Html::image('images/products/'.$product->thumbnail, '',['id' => 'blah', 'style'=>'max-height:100%;']) !!}
			</div>
    		<div class="fileUpload btn btn-primary">
			    <span>Chọn ảnh đại diện cho SP</span>
			    {{Form::file('thumbnail',['class'=>'upload', 'id'=>"imgInp"] )}}
			</div>
			<div id="result">
				<?php if(isset($product->gallery) && $product->gallery !=null){$galleries = json_decode($product->gallery);}else{$galleries = null;}?>
				@if(count($galleries) > 0)
					@foreach($galleries as $image)
						<div><img class='thumbnail' src="{!! URL::asset('images/products/'.$image) !!}"/><span class="rm_gallery">x</span>
						<input type="hidden" name="gallery[]" class="form-control" value="{!!$image!!}">
						</div>
					@endforeach
				@else
				<div><img class='thumbnail null' src="{!! URL::asset('images/web/default.jpeg') !!}"/><span class="rm_gallery">x</span></div>
				<div><img class='thumbnail null' src="{!! URL::asset('images/web/default.jpeg') !!}"/><span class="rm_gallery">x</span></div>
				<div><img class='thumbnail null' src="{!! URL::asset('images/web/default.jpeg') !!}"/><span class="rm_gallery">x</span></div>
				@endif
			</div>
    		<div class="fileUpload btn btn-primary">
			    <span>Thêm hình ảnh phụ</span>
			    {!! Form::file('images[]', array('class'=>'upload','multiple'=>true, 'id'=>"uploadBtn")) !!}
			</div>
		</div>

		<div class="clearfix"></div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Mô tả ngắn gọn<abbr>*</abbr></label>
			<div class="col-sm-12">
				<textarea class="tinymce" name="desc_short">{!! $product->desc_short !!}</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Kho hàng</label>
			<div class="col-sm-12">
				<textarea class="tinymce" name="warehouse_info">{!! $product->warehouse_info !!}</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Giao hàng</label>
			<div class="col-sm-12">
				<textarea class="tinymce" name="ship_info">{!! $product->ship_info !!}</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Mô tả chi tiết<abbr>*</abbr></label>
			<div class="col-sm-12">
				<textarea class="tinymce" name="desc_main">{!! $product->desc_main !!}</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-12">Driver & Software</label>
			<div class="col-sm-12">
				<textarea class="tinymce" name="driversoftware_info">{!! $product->driversoftware_info !!}</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
			</div>
		</div>
{!! Form::close() !!}
@section('scripts')
<script type="text/javascript">

	// Tính giá tiền sản phẩm sau khi giảm giá
	/*$('#save_money').keyup(function(event) {
		var price = $('#price').val();
		var save_money = $(this).val();
		$('#sale').val(Math.ceil(save_money*100/price));
		$('#main_price').val(price-save_money);
	});*/

	// Preview Product THumbnail
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });

function handleFileSelect() {
    //Check File API support
    if($('#result div img').hasClass('null')){
    	$('#result div').remove();
    }
 
    if (window.File && window.FileList && window.FileReader) {

        var files = event.target.files; //FileList object
        var output = document.getElementById("result");

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            //Only pics
            if (!file.type.match('image')) continue;

            var picReader = new FileReader();
            picReader.addEventListener("load", function (event) {
                var picFile = event.target;
                var div = document.createElement("div");
                div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>";
                output.insertBefore(div, null);
            });
            //Read the image
            picReader.readAsDataURL(file);
        }
    } else {
        console.log("Your browser does not support File API");
    }
}

document.getElementById('uploadBtn').addEventListener('change', handleFileSelect, false);

$(document).on('click', '.rm_gallery',function(){
	$(this).parent('div').remove();
});
</script>
@stop
@stop