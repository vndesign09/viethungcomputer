@extends('layouts.admin_layout');
@section('contents')
@section('title_page')Danh sách trang @stop
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
<a href="{!!URL::route('pages.add')!!}" class="btn btn-success">Thêm Trang</a>
@include('layouts.notifications')
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size:13px;">
	<thead>
		<tr>
			<th>#</th>
			<th class="col-sm-7">Trang</th>
			<th class="col-sm-2">Ngày đăng</th>
			<th class="col-sm-2">Ngày sửa</th>
			<th class="col-sm-1">#</th>
		</tr>
	</thead>
	<tbody>
		@if(isset($pages))
		@foreach($pages as $key => $value)
			<tr>
				<td>{!! $key=$key+1 !!}</td>
				<td><a href="{!!URL::route('details.pages', ['alias'=>$value->alias])!!}" target="_blank">{!! $value->title !!}</a></td>
				<td>{!! $value->created_at !!}</td>
				<td>{!! $value->updated_at !!}</td>
				<td><a href='{!!URL::route('pages.edit', $value->id)!!}' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  <a href='{!!URL::route('pages.delete', $value->id)!!}' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td>
			</tr>
		@endforeach
		@endif
	</tbody>
</table>


@section('scripts')

@stop
@stop
