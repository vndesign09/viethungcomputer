@extends('layouts.admin_layout');
@section('contents')
@section('title_page')Thêm trang @stop
@include('layouts.notifications')
  {!! Form::open(array(
      'route' => 'pages.add_post',
      'class' => 'form-horizontal',
      'novalidate' => 'novalidate',
      'files' => true))!!}
    {!! csrf_field()!!}
    <div class="col-sm-12">

      <div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Chỉ mục<abbr>*</abbr></label>
				<div class="col-sm-8">
					<select name="category_id" id="input" class="form-control" required="required">
					<option value="2">Menu</option>
						@foreach($cates as $k => $v)
							<option value="{{$v['id']}}">{{$v['name']}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Trang<abbr>*</abbr></label>
				<div class="col-sm-8">
					<input type="text" name="title" id="input" class="form-control">
				</div>
			</div>
      <div class="form-group">
  			<label for="input-id" class="col-sm-12">Nội dung<abbr>*</abbr></label>
  			<div class="col-sm-12">
  				<textarea class="tinymce" name="contents"></textarea>
  			</div>
  		</div>
    </div>
    <div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
			</div>
		</div>
    {!! Form::close()!!}
@section('scripts')
<script type="text/javascript">
	// Preview Product THumbnail
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });


    $(".form-horizontal").ajaxForm({url: '{!! URL::route("prod.add_post") !!}', type: 'post'})
</script>
@stop
@stop
