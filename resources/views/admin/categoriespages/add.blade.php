@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Thêm chỉ mục trang @stop
@include('layouts.notifications')
<form action="{{URL::route('cat_pages.add_post')}}" method="POST" class="form-horizontal" role="form">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Tên</label>
			<div class="col-sm-5">
				<input type="text" name="name" id="input" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Vị trí</label>
			<div class="col-sm-5">
				<select name="parent_id" id="input" class="form-control">
					<?php getCategories($cates); ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
			</div>
		</div>
</form>
@stop
