@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Chỉnh sửa chỉ mục trang @stop
@include('layouts.notifications')
<form action="{{URL::route('cat_pages.edit_post', $cate->id)}}" method="POST" class="form-horizontal" role="form">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Tên</label>
			<div class="col-sm-5">
				<input type="text" name="name" id="input" class="form-control" value="{!! $cate->name !!}">
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Vị trí</label>
			<div class="col-sm-5">
				<select name="parent_id" id="input" class="form-control">
					<?php getCategories($cates,0,"",$cate->parent_id); ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
				<a class="btn btn-danger" data-toggle="modal" href='#modal-id'>Xóa</a>
			</div>
		</div>
		<div class="modal fade" id="modal-id">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="glyphicon glyphicon-warning-sign"></i>  WARNING</h4>
					</div>
					<div class="modal-body">
						<p class="alert alert-warning"><strong>Bạn đang thực hiện lệnh xóa một Chỉ mục, điều này dẫn đến các trang thuộc chỉ mục này sẽ bị xóa theo.</strong><br>Bạn có chắc ?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
						<a href="{!!URL::route('cat_pages.delete', $cate->id)!!}" class="btn btn-primary">Tiếp tục Xóa</a>

					</div>
				</div>
			</div>
		</div>
</form>
@section('scripts')
	<script type="text/javascript">
		function myFunction() {
		    var x;
		    if (confirm("Cảnh báo") == true) {
		        x = "You pressed OK!";
		    } else {
		        x = "You pressed Cancel!";
		    }
		    document.getElementById("demo").innerHTML = x;
		}
	</script>
@stop
@stop
