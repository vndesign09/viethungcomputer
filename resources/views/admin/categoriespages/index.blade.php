@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Chỉ mục trang @stop
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
<a href="{!!URL::route('cat_pages.add')!!}" class="btn btn-success">Thêm Chỉ mục</a>
@include('layouts.notifications')
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>#</th>
			<th class="col-sm-8">Chỉ mục</th>
			<th class="col-sm-2">Vị trí</th>
			<th class="col-sm-2">#</th>
		</tr>
	</thead>
	<tbody>
		@foreach($cates as $k => $v)
			<tr>
				<td>{!!$k+1!!}</td>
				<td>{!!$v['name']!!}</td>
				<td>@if($v['parent_id'] == 3)Footer @else Menu @endif</td>
				<td>
					<a href="{!!URL::route('cat_pages.edit_post', $v['id'])!!}" class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  
					<a href='{!!URL::route('cat_pages.delete', $v['id'])!!}' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>


@section('scripts')

@stop
@stop
