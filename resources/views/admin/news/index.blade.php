@extends('layouts.admin_layout');
@section('contents')
@section('title_page')Tin tức @stop
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
<a href="{!!URL::route('news.add')!!}" class="btn btn-success">Thêm Tin Tức</a>
@include('layouts.notifications')
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size:13px">
	<thead>
		<tr>
			<th>#</th>
			<th class="col-sm-1">Hình ảnh</th>
			<th class="col-sm-4">Tên tin tức</th>
			<th class="col-sm-4">Mô tả</th>
			<th class="col-sm-1">Ngày đăng</th>
			<th class="col-sm-1">Ngày sửa</th>
			<th class="col-sm-1">#</th>
		</tr>
	</thead>
	<tbody>
		@if(isset($news))
		@foreach($news as $key => $value)
			<tr>
				<td>{!! $key=$key+1 !!}</td>
				<td>{!! Html::image('images/news/'.$value->thumbnail, '', ['width' => '90px']) !!}</td>
				<td><b><a href="{!!URL::route('details.news', ['alias'=>$value->alias, 'id'=>$value->id])!!}" target="_blank">{!! $value->title !!}</a></b></td>
				<td>{!! $value->desc!!}</strong></td>
				<td>{!! $value->created_at !!}</td>
				<td>{!! $value->updated_at !!}</td>
				<td><a href='{!!URL::route('news.edit', $value->id)!!}' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  <a href='{!!URL::route('news.delete', $value->id)!!}' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td>
			</tr>
		@endforeach
		@endif
	</tbody>
</table>



@stop
