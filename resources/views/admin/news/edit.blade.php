@extends('layouts.admin_layout');
@section('contents')
@section('title_page')Chỉnh sửa tin tức @stop
@include('layouts.notifications')
{!! Form::open(array('route' => ['news.edit_post', $news->id], 'method'=> 'POST','class' => 'form-horizontal', 'novalidate' => 'novalidate', 'files' => true)) !!}
		{!! csrf_field() !!}
		<div class="col-sm-8">
      <div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Danh mục<abbr>*</abbr></label>
				<div class="col-sm-8">
					<select name="category_id" id="input" class="form-control" required="required">
						<option value="">Chọn danh mục</option>
						<?php getCategories($cates,0,'', $news->category_id); ?>
					</select>
				</div>
			</div>
      <div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Tên Bài Viết<abbr>*</abbr></label>
				<div class="col-sm-8">
					<input type="text" name="title" id="input" class="form-control" value="{!!$news->title!!}">
				</div>
			</div>
      <div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Mô tả<abbr>*</abbr></label>
				<div class="col-sm-8">
					<input type="text" name="desc" id="input" class="form-control" value="{!!$news->desc!!}">
				</div>
			</div>
      <div class="form-group">
  			<label for="input-id" class="col-sm-12">Nội dung bài viết<abbr>*</abbr></label>
  			<div class="col-sm-12">
  				<textarea class="tinymce" name="contents">{!!$news->contents!!}</textarea>
  			</div>
  		</div>
    </div>
    <div class="col-sm-4">
      <div class="news_img" style="height:260px;">
        {!! Html::image('images/news/'.$news->thumbnail, 'default_img',['id' => 'blah', 'style'=>'max-width:250px;']) !!}
      </div>
        <input type='file' id="imgInp" name="thumbnail"/>
    </div>
    <div class="form-group">
      <div class="col-sm-10 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Lưu</button>
      </div>
    </div>

{!! Form::close() !!}
@section('scripts')
<script type="text/javascript">

	// Preview Product THumbnail
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });


    $(".form-horizontal").ajaxForm({url: '{!! URL::route("prod.add_post") !!}', type: 'post'})
</script>
@stop
@stop
