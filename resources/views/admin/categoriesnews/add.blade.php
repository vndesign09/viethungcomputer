@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Thêm chuyên mục tin tức @stop
@include('layouts.notifications')
<form action="{{URL::route('cat_news.add_post')}}" method="POST" class="form-horizontal" role="form">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Tên</label>
			<div class="col-sm-5">
				<input type="text" name="name" id="input" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
			</div>
		</div>
</form>
@stop
