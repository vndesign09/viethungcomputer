@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Chuyên mục tin tức @stop
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
<a href="{!!URL::route('cat_news.add')!!}" class="btn btn-success">Thêm Chuyên Mục</a>
@include('layouts.notifications')
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>#</th>
			<th class="col-sm-10">Tên Danh Mục</th>
			<th class="col-sm-2">#</th>
		</tr>
	</thead>
	<tbody>
		<!-- <?php getCategoriesNewsTable($cates); ?> -->
		@foreach($cates as $cate)
		<tr>
			<td>{{$cate->id}}</td>
			<td>{{$cate->name}}</td>
			<td><a href='{!!URL::route('cat_news.edit', $cate->id)!!}' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  <a href='{!!URL::route('cat_news.delete', $cate->id)!!}' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td>
		</tr>
		@endforeach
	</tbody>
</table>


@section('scripts')

@stop
@stop
