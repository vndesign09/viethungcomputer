@extends('layouts.admin_layout')
@section('contents')
@section('title_page') Tổng quan @stop
<div id="sum_box" class="row mbl">
                            <div class="col-sm-6 col-md-3">
                            	<a href="{{URL::route('cat_pro.index')}}">
                                <div class="panel profit db mbm">
                                    <div class="panel-body">
                                        <p class="icon">
                                            <i class="icon fa fa-sitemap"></i>
                                        </p>
                                        <h4 class="value">
                                            <span data-counter="" data-start="10" data-end="50" data-step="1" data-duration="0">{{$categories}}</span></h4>
                                        <p class="description">
                                            Danh mục sản phẩm</p>
                                        <div class="progress progress-sm mbn">
                                            <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-success">
                                                <span class="sr-only">100% Complete (success)</span></div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                            	<a href="{{URL::route('prod.index')}}">
                                <div class="panel income db mbm">
                                    <div class="panel-body">
                                        <p class="icon">
                                            <i class="icon fa fa-gift fa-fw"></i>
                                        </p>
                                        <h4 class="value">
                                            <span>{{$products}}</span></h4>
                                        <p class="description">
                                            Sản phẩm</p>
                                        <div class="progress progress-sm mbn">
                                            <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 1000%;" class="progress-bar progress-bar-info">
                                                <span class="sr-only">60% Complete (success)</span></div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="{{URL::route('news.index')}}">
                                <div class="panel income db mbm">
                                    <div class="panel-body">
                                        <p class="icon">
                                            <i class="icon fa fa-file fa-fw"></i>
                                        </p>
                                        <h4 class="value">
                                            <span>{{$news}}</span></h4>
                                        <p class="description">
                                            Bài viết</p>
                                        <div class="progress progress-sm mbn">
                                            <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 1000%;" class="progress-bar progress-bar-danger">
                                                <span class="sr-only">60% Complete (success)</span></div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="{{URL::route('pages.index')}}">
                                <div class="panel income db mbm">
                                    <div class="panel-body">
                                        <p class="icon">
                                            <i class="icon fa fa-file-o fa-fw"></i>
                                        </p>
                                        <h4 class="value">
                                            <span>{{$pages}}</span></h4>
                                        <p class="description">
                                            Trang</p>
                                        <div class="progress progress-sm mbn">
                                            <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 1000%;" class="progress-bar progress-bar-warning">
                                                <span class="sr-only">60% Complete (success)</span></div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
@stop