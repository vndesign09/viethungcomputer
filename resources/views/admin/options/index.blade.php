@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Cài đặt trang @stop

	@include('layouts.notifications')
	<form action="{{URL::route('options.index.post', 'settings')}}" method="POST" class="form-horizontal" role="form">
	{!! csrf_field() !!}
			<legend>Cài đặt trang</legend>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Tiêu đề trang</label>
				<div class="col-sm-8">
					<input type="text" name="title" id="input" class="form-control" value="@if(isset($options['title'])){{$options['title']}}@endif">
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Mô tả trang</label>
				<div class="col-sm-8">
					<textarea class="form-control" name="descriptions">@if(isset($options['descriptions'])){{$options['descriptions']}}@endif</textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Từ khóa trang</label>
				<div class="col-sm-8">
					<input type="text" name="keywords" id="input" class="form-control" value="@if(isset($options['keywords'])){{$options['keywords']}}@endif">
				</div>
			</div>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">SĐT CKSH</label>
				<div class="col-sm-2">
					<input type="text" name="customer_care" id="input" class="form-control" value="@if(isset($options['customer_care'])){{$options['customer_care']}}@endif">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<button type="submit" class="btn btn-primary">Lưu</button>
				</div>
			</div>
	</form>




	<form action="{{URL::route('options.index.post','footer')}}" method="POST" class="form-horizontal" role="form">
	{!! csrf_field() !!}
			<legend>Footer</legend>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Nội dung</label>
				<div class="col-sm-8">
					<textarea class="form-control tinymce" name="footer">{{$options['footer']}}</textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<button type="submit" class="btn btn-primary">Lưu</button>
				</div>
			</div>
	</form>

	<form action="{{URL::route('options.index.post','header')}}" method="POST" class="form-horizontal" role="form" accept-charset="UTF-8" class="form-horizontal" novalidate="novalidate" enctype="multipart/form-data">
	{!! csrf_field() !!}
			<legend>Header</legend>
			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Logo</label>
				<div class="col-sm-1">
					<div class="product_img" style="">
						{!! Html::image('images/web/'.$options['logo'], 'default_img',['id' => 'blahlogo', 'style'=>'max-width:100%;']) !!}
					</div>
		    	</div>
		    	<div class="col-sm-4">
		    		<input type='file' id="imgInp" class="blahlogo" name="logo"/>
		    	</div>
			</div>

			<div class="form-group">
				<label for="input-id" class="col-sm-2 control-label">Banner</label>
				<div class="col-sm-8">
					<div class="product_img" style="">
						{!! Html::image('images/web/'.$options['banner'], 'default_img',['id' => 'blahbanner', 'style'=>'max-width:100%;']) !!}
					</div>
		    	</div>
		    	<div class="col-sm-10 col-sm-offset-2">
		    		<input type='file' id="imgInp" class="blahbanner" name="banner"/>
		    	</div>
			</div>

			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<button type="submit" class="btn btn-primary">Lưu</button>
				</div>
			</div>
	</form>

	<form action="{{URL::route('options.index.post','homepage')}}" method="POST" class="slider form-horizontal" role="form" accept-charset="UTF-8" class="form-horizontal" novalidate="novalidate" enctype="multipart/form-data">
	{!! csrf_field() !!}
			<legend>Homepage (Slider)</legend>
			@if(isset($sliders) && count($sliders) > 0 )
				@foreach($sliders as $key => $slider)	
					<div class="form-group item {{$key}}">
						<div class="col-sm-9">
							<div class="slider_img" style="">
								{!! Html::image('images/web/'.$slider, 'default_img',['id' => 'blahslider'.$key, 'style'=>'max-width:100%;']) !!}
							</div>
				    	</div>
				    	<div class="col-sm-3">
				    		<button type="button" class="btn btn-danger del">Xóa</button>	
				    		<input type='file' id="imgInp" class="blahslider{{$key}}" name="slider{{$key}}"/>
				    	</div>
					</div>
				@endforeach
			@else
			<div class="form-group item 0">
				<div class="col-sm-9">
					<div>
						{!! Html::image('images/web/default.jpeg', 'default_img',['id' => 'blahslider0', 'style'=>'max-width:100%;']) !!}
					</div>
		    	</div>
		    	<div class="col-sm-3">
		    		<input type='file' id="imgInp" class="blahslider0" name="slider0"/>
		    	</div>
			</div>
			@endif
			<div class="form-group last">
				<div class="col-sm-10 ">
					<button type="button" class="add_new_slider btn btn-success">Thêm hình</button>
					<button type="submit" class="btn btn-primary">Lưu</button>
				</div>
			</div>

	</form>

	<form action="{{URL::route('options.index.post','supports')}}" method="POST" class="supports form-horizontal" role="form">
	{!! csrf_field() !!}
			<legend>Supports</legend>
			<label for="input-id" class="col-sm-4">Tên</label>
			<label for="input-id" class="col-sm-2">SDT</label>
			<label for="input-id" class="col-sm-2">Yahoo</label>
			<label for="input-id" class="col-sm-2">Skype</label>
			@if(isset($support) && count($support) > 0)
				@foreach($support as $key => $supporter)
				
				<?php 
					
					$info = json_decode($supporter); 
				?>
				<div class="form-group item">
					<div class="col-sm-4">
						<input type="text" name="{{$key}}[]" id="{{$key}}" class="length form-control" value="{{$info->fullname}}" >
					</div>
					<div class="col-sm-2">
						<input type="text" name="{{$key}}[]" class="form-control" value="{{$info->phone}}" >
					</div>
					<div class="col-sm-2">
						<input type="text" name="{{$key}}[]" class="form-control" value="{{$info->yahoo}}" >
					</div>
					<div class="col-sm-2">
						<input type="text" name="{{$key}}[]" class="form-control" value="{{$info->skype}}" >
					</div>
					<div class="col-sm-2">
				    	<button type="button" class="btn btn-danger del_supporter">Xóa</button>	
				    </div>
				</div>
				@endforeach
			@else
			<div class="form-group item 0">
				<div class="col-sm-4">
					<input type="text" name="supporter0[]" id="supporter0" class="length form-control" value="" >
				</div>
				<div class="col-sm-2">
					<input type="text" name="supporter0[]" class="form-control" value="" >
				</div>
				<div class="col-sm-2">
					<input type="text" name="supporter0[]" class="form-control" value="" >
				</div>
				<div class="col-sm-2">
					<input type="text" name="supporter0[]" class="form-control" value="" >
				</div>
			</div>
			<div class="form-group item 1">
				<div class="col-sm-4">
					<input type="text" name="supporter1[]" id="supporter1" class="length form-control" value="" >
				</div>
				<div class="col-sm-2">
					<input type="text" name="supporter1[]" class="form-control" value="" >
				</div>
				<div class="col-sm-2">
					<input type="text" name="supporter1[]" class="form-control" value="" >
				</div>
				<div class="col-sm-2">
					<input type="text" name="supporter1[]" class="form-control" value="" >
				</div>
			</div>
			@endif
			<div class="form-group last">
				<div class="col-sm-10">
					<button type="button" class="add_supporter btn btn-success">Thêm</button>
					<button type="submit" class="btn btn-primary">Lưu</button>
				</div>
			</div>
	</form>
@section('scripts')
<script type="text/javascript">

   
	// Preview Product THumbnail
	function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#'+id).attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $('body').on('change', '#imgInp', function(){ 
    	var id = $(this).attr('class');
        readURL(this, id);
    });


    String.prototype.filename=function(extension){
	    var s= this.replace(/\\/g, '/');
	    s= s.substring(s.lastIndexOf('/')+ 1);
	    return extension? s.replace(/[?#].+$/, ''): s.split('.')[0];
	}

	 $('.add_new_slider').click(function() {
	 	var sliders = [];
	 	$('.slider_img img').each(function(){
	 		var slider = $(this).attr('src').filename();
	 			slider = slider.replace('slider', '');
	 		sliders.push(slider);
	 	});
		var count_item = Math.floor((Math.random() * 100) + 1);
		if(jQuery.inArray( count_item, sliders ) < 0 ){
			$('.slider .last').before('<div class="form-group item '+count_item+'"><div class="col-sm-12"><div class="product_img" style=""><img src="../images/web/default.jpeg" id="blahslider'+count_item+'" style="max-width:100%;" alt="default_img"></div></div><div class="col-sm-12"><input type="file" id="imgInp" class="blahslider'+count_item+'" name="slider'+count_item+'"/></div></div>');	
		}
	});

	$('.add_supporter').click(function(){
		var supports = [];
	 	$('.supports .length').each(function(){
	 		var support = $(this).attr('id');
	 			support = support.replace('supporter', '');
	 		supports.push(support);
	 	});
		var count_item = Math.floor((Math.random() * 100) + 1);
		if(jQuery.inArray( count_item, supports ) < 0 ){
		$('.supports .last').before('<div class="form-group item '+count_item+'"><div class="col-sm-4"><input type="text" name="supporter'+count_item+'[]" id="inputInfo[]" class="form-control" value="" ></div><div class="col-sm-2"><input type="text" name="supporter'+count_item+'[]" id="inputInfo[]" class="form-control" value="" ></div><div class="col-sm-2"><input type="text" name="supporter'+count_item+'[]" id="inputInfo[]" class="form-control" value="" ></div><div class="col-sm-2"><input type="text" name="supporter'+count_item+'[]" id="inputInfo[]" class="form-control" value="" ></div></div>');
		}
	});


    $('body').on('click', '.del', function(){ 
    	var slider = $(this).parents('.form-group').find('img').attr('src').filename();
    	$(this).parents('.form-group').hide();
    	$(this).after('<input type="hidden" name="del[]" id="hidden" class="form-control" value="'+slider+'">');
    });


    $('body').on('click', '.del_supporter', function(){ 
    	var supporter = $(this).parents('.form-group').find('input[type="text"]').attr('name');
    	supporter = supporter.replace('[]','');
    	$(this).parents('.form-group').hide();
    	console.log(supporter);
    	$(this).after('<input type="hidden" name="del[]" id="hidden" class="form-control" value="'+supporter+'">');
    });

    setTimeout(function(){
    	$('.tinymce').closest('.form-group').find('.col-sm-12').hide();
    },1000);
    
</script>
@stop	
@stop