@extends('layouts.admin_layout')
@section('contents')
@include('layouts.notifications')
@section('title_page')Thêm hãng sản xuất @stop
<form action="{{URL::route('brand.add_post')}}" method="POST" class="form-horizontal" role="form">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Tên hãng <abbr>*</abbr></label>
			<div class="col-sm-5">
				{{Form::text('name', '', ['class'=>"form-control"])}}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
			</div>
		</div>
</form>
@stop