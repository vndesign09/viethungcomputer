@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Danh sách hãng sản xuất @stop
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
<a href="{!!URL::route('brand.add')!!}" class="btn btn-success">Thêm Hãng</a>
@include('layouts.notifications')
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>#</th>
			<th class="col-sm-10">Tên hãng</th>
			<th class="col-sm-2">#</th>
		</tr>
	</thead>
	<tbody>
		@if(isset($brands))
		@foreach($brands as $brand)
		<tr>
			<td>{{$brand->id}}</td>
			<td>{{$brand->name}}</td>
			<td><a href='{{URL::route("brand.edit", $brand->id)}}' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  
				<a href='{{URL::route("brand.delete", $brand->id)}}' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>


@section('scripts')

@stop
@stop
