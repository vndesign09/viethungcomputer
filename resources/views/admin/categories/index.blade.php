@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Danh mục sản phẩm @stop
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable( {
		    "iDisplayLength": 100
		});
	} );
</script>
<a href="{!!URL::route('cat_pro.add')!!}" class="btn btn-success">Thêm Danh Mục</a>
@include('layouts.notifications')
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>#</th>
			<th class="col-sm-10">Tên Danh Mục</th>
			<th class="col-sm-2">#</th>
		</tr>
	</thead>
	<tbody>
		<?php getCategoriesTable($cates); ?>
	</tbody>
</table>


@section('scripts')

@stop
@stop
