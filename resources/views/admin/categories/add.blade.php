@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Thêm danh mục sản phẩm @stop
@include('layouts.notifications')
{!! Form::open(
    array(
        'route' => 'cat_pro.add_post', 
        'class' => 'form-horizontal', 
        'novalidate' => 'novalidate', 
        'files' => true)) !!}
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Tên</label>
			<div class="col-sm-5">
				<input type="text" name="name" id="input" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Mã số</label>
			<div class="col-sm-5">
				<input type="text" name="code" id="input" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Hình ảnh</label>
			<div class="col-sm-5">
				<div class="product_img" style="height:30px;">
					{!! Html::image('images/web/default.jpeg', 'default_img',['id' => 'blah', 'style'=>'max-width:30px;']) !!}
					{{Form::file('thumbnail',['id'=>"imgInp",'style'=>'display:inline-block;margin-left:10px;'] )}}
				</div>
			</div>
		</div>	
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Danh mục cha</label>
			<div class="col-sm-5">
				<select name="parent_id" id="input" class="form-control">
					<option value="0">Danh mục</option>
					<?php getCategories($cates); ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
			</div>
		</div>
{!!Form::close()!!}
@section('scripts')
<script type="text/javascript">
	// Preview Product THumbnail
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
        document.getElementById("uploadFile").value = this.value;
    });

</script>
@stop
@stop