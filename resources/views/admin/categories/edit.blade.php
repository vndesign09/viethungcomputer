@extends('layouts.admin_layout')
@section('contents')
@section('title_page')Chỉnh sửa danh mục sản phẩm @stop
@include('layouts.notifications')
{!! Form::open(array('route' => ['cat_pro.edit_post', $cate->id], 'method'=> 'POST','class' => 'form-horizontal', 'novalidate' => 'novalidate', 'files' => true)) !!}
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Tên</label>
			<div class="col-sm-5">
				<input type="text" name="name" id="input" class="form-control" value="{!! $cate->name !!}">
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Mã số</label>
			<div class="col-sm-5">
				<input type="text" name="code" id="input" class="form-control" value="{!! $cate->code !!}">
			</div>
		</div>
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Hình ảnh</label>
			<div class="col-sm-5">
				<div class="product_img" style="height:30px;">
					{!! Html::image('images/categories/'.$cate->thumbnail, '',['id' => 'blah', 'style'=>'max-width:30px;']) !!}
					{{Form::file('thumbnail',['id'=>"imgInp",'style'=>'display:inline-block;margin-left:10px;'] )}}
				</div>
			</div>
		</div>	
		<div class="form-group">
			<label for="input-id" class="col-sm-2 control-label">Danh mục cha</label>
			<div class="col-sm-5">
				<select name="parent_id" id="input" class="form-control">
					<option>Danh mục</option>
					<?php getCategories($cates,0,"",$cate->parent_id); ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Lưu</button>
				<a class="btn btn-danger" data-toggle="modal" href='#modal-id'>Xóa</a>
			</div>
		</div>
		<div class="modal fade" id="modal-id">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><i class="glyphicon glyphicon-warning-sign"></i>  WARNING</h4>
					</div>
					<div class="modal-body">
						<p class="alert alert-warning"><strong>Bạn đang thực hiện lệnh xóa một Danh Mục, điều này dẫn đến các sản phẩm thuộc danh mục này sẽ bị xóa theo.</strong><br>Bạn có chắc ?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
						<a href="{!!URL::route('cat_pro.delete', $cate->id)!!}" class="btn btn-primary">Tiếp tục Xóa</a>

					</div>
				</div>
			</div>
		</div>
{!!Form::close()!!}
@section('scripts')
	<script type="text/javascript">
		// Preview Product THumbnail
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
        document.getElementById("uploadFile").value = this.value;
    });
	</script>
@stop
@stop