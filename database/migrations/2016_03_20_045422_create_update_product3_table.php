<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateProduct3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table){
            $table->string('main_price')->after('price');
            $table->integer('is_best_sale')->after('is_public');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Shema::table('products', function(Blueprint $table){
            $table->dropColumn('main_price');
            $table->dropColumn('is_best_sale');
        });
    }
}
