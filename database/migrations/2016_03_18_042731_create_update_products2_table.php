<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateProducts2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ($table) {
            $table->string('name', 500)->change();
            $table->string('ship_info', 500)->change();
            $table->string('warehouse_info', 500)->change();
            $table->string('desc_short', 5000)->change();
            $table->string('desc_main', 5000)->change();
            $table->string('driversoftware_info', 5000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
