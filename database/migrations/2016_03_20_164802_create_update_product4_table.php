<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateProduct4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ($table) {
            $table->string('name', 5000)->change();
            $table->string('alias', 5000)->change();
            $table->string('ship_info', 50000)->change();
            $table->string('warehouse_info', 50000)->change();
            $table->string('desc_short', 500000)->change();
            $table->string('desc_main', 500000)->change();
            $table->string('driversoftware_info', 500000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
