<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('alias');
            $table->string('code');
            $table->string('desc_short');
            $table->string('desc_main');
            $table->string('price');
            $table->string('sale');
            $table->integer('category_id');
            $table->string('thumbnail');
            $table->string('guarantee_info');
            $table->string('ship_info');
            $table->string('warehouse_info');
            $table->string('driversoftware_info');
            $table->integer('counter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
