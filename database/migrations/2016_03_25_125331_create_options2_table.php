<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptions2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function(Blueprint $table){
            $table->increments('id');
            $table->string('name', 5000);
            $table->string('value', 50000);
            $table->timestamps();
        });
    }

    /**co
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('options');
    }
}
