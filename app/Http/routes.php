<?php
use App\CatProd;
use App\Options;
use App\Pages;
use App\CatPages;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
// Lấy tất cả Category
$categories = CatProd::where('parent_id', 0)->get();


$cat_arr = CatProd::get()->toArray();
$cat = $categories->toArray();
function getParent($arrData, $parent, &$result){
	if (count($arrData) > 0) {
    	foreach ($arrData as $key => $val) {
        	if ($parent == $val['parent_id']) {
                $result[] = $val;
            	$_parent = $val['id'];
                unset($arrData[$key]);
                getParent($arrData, $_parent, $result);
            }
        }
    }
}
$list_cate = array();
if(count($cat) > 0){
	foreach ($cat as $k => $v) {
		$arr = array();
		$products_id = array();
		getParent($cat_arr,$v['id'], $result);
		if($result != null){
			foreach ($result as $key => $value) {
				if($value['parent_id'] == $v['id']){
			    	$arr[] =  $value;
			        $products_id[] = $value['id'];
			    }
			}
			$list_cate[$v['name']] =  array('alias'=>$v['alias'], 'id'=> $v['id'], 'cat_child'=>$arr);
		}else{
			$list_cate = null;
		}
	}
}else{
	$list_cate = null;
}


// Lấy cài đặt trang
$options = Options::lists('value','name');

// Lấy danh sách người hỗ trợ trực tuyến 
$supports  = Options::where('name', 'LIKE' , '%supporter%')->orderBy('name', 'asc')->lists('value', 'name')->toArray();
sort($supports);

// Lấy danh sách page dưới Footer
$footer = CatPages::where('parent_id',0)->get();
foreach ($footer as $key => $position) {
	$pages = CatPages::where('parent_id', $position->id)->get();
	$positons_footer = array();
	foreach ($pages as $key => $page) {
		$list_page = Pages::where('category_id', $page->id)->get();
		$positons_footer[$page->name] = array('pages'=> $list_page);
	}
}

// Lấy Pages trên Menu
$positons_menu = Pages::where('category_id', 2)->get();


// Xuất ra view
view()->share('categories', $categories);
view()->share('list_cate', $list_cate);
view()->share('cat_arr', $cat_arr);
view()->share('options', $options);
view()->share('positons_menu', $positons_menu);
view()->share('positons_footer', $positons_footer);
view()->share('supports', $supports);


// Admin Page
//Route::group(['prefix' => 'admin'], function(){
	Route::group(['middleware' => 'web'], function () {
		/*Route::get('/register', 'Auth\AuthController@getRegister');
		Route::post('/register', 'Auth\AuthController@postRegister');*/
		Route::get('/login', 'Auth\AuthController@getLogin');
		Route::post('/login', 'Auth\AuthController@postLogin');
		Route::get('/logout', 'Auth\AuthController@logout');

    	Route::group(['middleware' => 'auth'], function () {
			Route::group(['prefix'=>'admin','namespace'=>'Admin'], function(){
				Route::controller('filemanager','FilemanagerController', array(
					'getIndex' => 'get.filemanager',
					'getFeaturedImage' => 'get.featured.image',
					'getGalleryImages' => 'get.gallery.images',
					'postIndex' => 'post.filemanager',
				));

				Route::get('/', [
					'as' => 'home.admin', 'uses' => 'HomeController@index'
				]);

				// Route Category
				Route::controller('category','CategoriesProductsController', array(
					'getIndex' => 'cat_pro.index',
					'getAdd' => 'cat_pro.add',
					'postAdd' => 'cat_pro.add_post',
					'getEdit' => 'cat_pro.edit',
					'postEdit' => 'cat_pro.edit_post',
					'getDelete' => 'cat_pro.delete',
				));

				// Route Brands
				Route::controller('brand','BrandsController', array(
					'getIndex' => 'brand.index',
					'getAdd' => 'brand.add',
					'postAdd' => 'brand.add_post',
					'getEdit' => 'brand.edit',
					'postEdit' => 'brand.edit_post',
					'getDelete' => 'brand.delete',
				));


				Route::get('/option', [
				    'as' => 'options.index', 'uses' => 'OptionsController@index'
				]);
				Route::post('/option/{action}', [
				    'as' => 'options.index.post', 'uses' => 'OptionsController@post'
				]);

				// Route Products
				Route::controller('product','ProductController', array(
					'getIndex' => 'prod.index',
					'getAdd' => 'prod.add',
					'postAdd' => 'prod.add_post',
					'getEdit' => 'prod.edit',
					'postEdit' => 'prod.edit_post',
					'getDelete' => 'prod.delete',
				));

			    //Route News
			    Route::controller('news', 'NewsController', array(
			        'getIndex' => 'news.index',
			        'getAdd' => 'news.add',
			        'postAdd' => 'news.add_post',
			        'getEdit' => 'news.edit',
			        'postEdit' => 'news.edit_post',
			        'getDelete' => 'news.delete'
			    ));

          //Route News
			    Route::controller('categorynews', 'CategoriesNewsController', array(
			        'getIndex' => 'cat_news.index',
			        'getAdd' => 'cat_news.add',
			        'postAdd' => 'cat_news.add_post',
			        'getEdit' => 'cat_news.edit',
			        'postEdit' => 'cat_news.edit_post',
			        'getDelete' => 'cat_news.delete'
			    ));

			    Route::controller('categorypages', 'CategoriesPagesController', array(
			        'getIndex' => 'cat_pages.index',
			        'getAdd' => 'cat_pages.add',
			        'postAdd' => 'cat_pages.add_post',
			        'getEdit' => 'cat_pages.edit',
			        'postEdit' => 'cat_pages.edit_post',
			        'getDelete' => 'cat_pages.delete'
			    ));

          		Route::controller('pages', 'PagesController', array(
			        'getIndex' => 'pages.index',
			        'getAdd' => 'pages.add',
			        'postAdd' => 'pages.add_post',
			        'getEdit' => 'pages.edit',
			        'postEdit' => 'pages.edit_post',
			        'getDelete' => 'pages.delete'
			    ));

			});
		});
		Route::get('/{alias}/{id}', [
		    'as' => 'details.product', 'uses' => 'DetailsController@index'
		]);


		Route::get('/search', [
		    'as' => 'search', 'uses' => 'SearchController@search'
		]);

		Route::get('/danh-muc/{alias}/{id}', [
		    'as' => 'category.product', 'uses' => 'CategoryController@product'
		]);

		Route::get('/n/{alias}/{id}', [
		    'as' => 'category.news', 'uses' => 'CategoryNewsController@news'
		]);
		 
		Route::get('/', [
		   	'as' => 'home', 'uses' => 'HomeController@index'
		]);


		Route::get('/tin-tuc', [
		   	'as' => 'news', 'uses' => 'NewsController@index'
		]);
		Route::get('/newsdetails', [
		   	'as' => 'newsdetails', 'uses' => 'NewsDetailsController@index'
		]);

		Route::get('/tin-tuc/{alias}/{id}', [
		    'as' => 'details.news', 'uses' => 'NewsDetailsController@index'
		]);
		Route::get('/xay-dung-may-tinh', [
		    'as' => 'build.computer', 'uses' => 'BuildComputerController@index'
		]);
		Route::get('/xem-may-tinh-da-tao', [
		    'as' => 'built.computer', 'uses' => 'BuildComputerController@built'
		]);
		Route::post('/xem-may-tinh-da-tao', [
		    'as' => 'save.order', 'uses' => 'BuildComputerController@saveOrder'
		]);
		Route::get('/{alias}', [
		    'as' => 'details.pages', 'uses' => 'PagesDetailsController@index'
		]);
		

	});
//});

