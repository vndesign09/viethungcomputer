<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
set_time_limit(0);
use App\Http\Requests;
use App\Product;
use App\CatProd;
use Jenssegers\Agent\Agent;
class DetailsController extends Controller
{
    public function index($alias, $id){
    	$product = Product::findOrFail($id);
    	$product->counter = $product->counter+1;
    	$product->save();
    	$list_category = CatProd::where('level', $product->category->level)->where('parent_id', $product->category->parent_id)->where('type', 'product')->get();
    	$related_product = Product::where('category_id',$product->category_id)->orderBy('counter', 'desc')->take(3)->get();


        /*$directory  = 'images/products';
        $files = \File::allFiles($directory);
        foreach ($files as $oldPath)
        {
            //$oldPath = 'images/1.jpg'; // publc/images/1.jpg
            //var_dump(getimagesize($oldPath)); die();
            $fileName = \File::name($oldPath);

            if (strpos($fileName, 'large') == false) {
                
                $fileExtension = \File::extension($oldPath);
            
                $newName = 'large-'.$fileName.'.'.$fileExtension;
                $newPathWithName = 'images/products/'.$newName;
                \File::copy($oldPath , $newPathWithName);
                \Image::make($newPathWithName)
                  ->resize(800, 700)
                  ->save($newPathWithName);
            }
            
            
            //echo (string)\File::name($oldPath), "\n";
        }
      */
    	$agent = new Agent();
    	if($agent->isMobile() || $agent->isTablet()){
    		return view('sp.details', compact('product', 'list_category', 'related_product'));
    	}else{
    		return view('details', compact('product', 'list_category', 'related_product'));
    	}
    }
}
