<?php

namespace App\Http\Controllers\Sp;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\CatProd;
class SPHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = CatProd::where('parent_id', 0)->get();
        $list_cate = array();
        foreach ($cats as $key => $cat) {
            $child = CatProd::where('parent_id', $cat->id)->get();
            $list_cate1 = array();
            $products = array();
            foreach ($child as $keys => $value) {
                $list_cate1[] = $value;

            }
            $list_cate[$cat->name] = $list_cate1;    
            
        }
        /*foreach ($list_cate as $key => $value) {
            foreach ($value as $key1 => $value1) {
                var_dump($value1->products[0]->name); die();
            }
        }
        die();*/
        return view('sp.home', compact('list_cate'));
    }
}
