<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CatProd;
use App\Product;
use Jenssegers\Agent\Agent;
use Validator;
class BuildComputerController extends Controller
{
    public function index(Request $request){
    	$list_cate = array(98, 99,100, 139, 97, 146, 140, 144, 147, 141, 142, 120);
    	$cat_id = 98;
        $cate = CatProd::find($cat_id);
    	$list_products = Product::where('category_id', $cat_id)->orderBy('id', 'desc')->paginate(30);
    	if ($request->ajax()) {
    		$data = $request->all();
            if($data['sort'] == 'sort_min'){
                $list_products = Product::where('category_id', $list_cate[$data['id']])->orderBy('main_price', 'asc')->get();    
            }else if($data['sort'] == 'sort_max'){
                $list_products = Product::where('category_id', $list_cate[$data['id']])->orderBy('main_price', 'desc')->get();    
            }else{
                $list_products = Product::where('category_id', $list_cate[$data['id']])->orderBy('id', 'desc')->get();    
            }
            
        	
            $cate = CatProd::find($list_cate[$data['id']]);
        	return $json = array('cate'=>$cate->toJson(),'list_products' => $list_products->toJson());
        } else {
                
        }
        return view('build_computer', compact('list_products', 'cate'));	
    }

    public function built(Request $request){
        $data = $request->all();
        if(isset($data['parts'])){
            $products = $data['parts'];
            $list_products = Product::whereIn('id', explode(",",$products))->get();    
        }else{
            $list_products = null;
        }
        return view('built_computer', compact('list_products'));    
    }


    public function saveOrder(Request $request){
        $input = $request->all();
        $rule = [
            'c_fullname' => 'required',
            'c_address' => 'required',
            'c_phone' => 'required|numeric',
        ];
        $messages = [
            'c_fullname.required'  => 'Vui lòng nhập tên của Quý khách',
            'c_address.required'  => 'Vui lòng nhập Địa chỉ nhận hàng',
            'c_phone.required'  => 'Vui lòng nhập Số điện thoại của quý khách',
            'c_phone.numeric'  => 'Vui lòng nhập SỐ',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            return redirect()->back()->with('success', 'Cám ơn quý khách đã đăng ký đơn hàng.');
        }
    }

}
