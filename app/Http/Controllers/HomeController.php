<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\Pages;
use App\CatProd;
use App\CatPages;
use App\News;
use App\Options;
use Jenssegers\Agent\Agent;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $sliders = Options::where('name', 'LIKE' , '%slider%')->lists('value', 'name')->toArray();
        sort($sliders);
        $cat_arr = CatProd::get()->toArray();
        $cat = CatProd::where('parent_id', 0)->get()->toArray();
        function getParent($arrData, $parent, &$result){
            if (count($arrData) > 0) {
                foreach ($arrData as $key => $val) {
                    if ($parent == $val['parent_id']/* && $val['level'] <= 1*/) {
                        $result[] = $val;
                        $_parent = $val['id'];
                        unset($arrData[$key]);
                        getParent($arrData, $_parent, $result);
                    }
                }
            }
        }
        $list_cate = array();
        foreach ($cat as $k => $v) {
            $arr = array();
            $products_id = array();
            getParent($cat_arr,$v['id'], $result);
            
            if($result != null){
                foreach ($result as $key => $value) {
                    if($value['parent_id'] == $v['id']){
                        $arr[] =  $value;
                        $products_id[] = $value['id'];
                    }
                }
                $products = Product::whereIn('category_id',$products_id)->orderByRaw("RAND()")->take(12)->get();
                $list_cate[$v['name']] =  array('alias'=>$v['alias'], 'id'=> $v['id'], 'child'=>array('cat_child'=>$arr, 'products'=>$products));
            }else{
                $products = $list_cate = null;
            }
        }
        
        // Sản phẩm khuyến mãi
        $product_km = Product::where('sale', '>', 0)->orderBy('sale', 'desc')->take(5)->get();
        $product_best_sale = Product::where('is_best_sale', 1)->take(5)->get();
        $product_new = Product::orderBy('created_at', 'desc')->take(5)->get();
        $news_new = News::orderBy('created_at', 'desc')->take(2)->get();
        $agent = new Agent();
        if($agent->isMobile() || $agent->isTablet()){
            return view('sp.home', compact('news_new','list_cate', 'product_km', 'product_best_sale', 'product_new'));    
        }else{
            return view('home', compact('news_new','list_cate', 'product_km', 'product_best_sale', 'product_new','sliders'));    
        }
        
    }
}
