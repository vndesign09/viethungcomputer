<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\News;
use App\CatNews;
use App\Pages;
use App\CatPages;
use Jenssegers\Agent\Agent;
class NewsDetailsController extends Controller
{
    public function index($alias, $id){
      $categories_new = CatNews::where('parent_id', 0)->get();
    	$news = News::where('id',$id)->where('alias', $alias)->first();
    	$news->counter = $news->counter+1;
    	$news->save();
    	$related_news = News::where('category_id',$news->category_id)->orderBy('counter', 'desc')->take(5)->get();
      $new_news = News::orderBy('created_at', 'desc')->take(5)->get();
      $news_count = News::orderBy('counter', 'desc')->take(5)->get();
      $agent = new Agent();
      if($agent->isMobile() || $agent->isTablet()){
       return view('sp.newsdetails', compact('news', 'related_news', 'categories_new', 'new_news', 'news_count'));
      }else{
          return view('newsdetails', compact('news', 'related_news', 'categories_new', 'new_news', 'news_count'));
      }

    }
}
