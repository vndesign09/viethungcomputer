<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pages;
use App\CatPages;
use App\Product;
use Jenssegers\Agent\Agent;
class PagesDetailsController extends Controller
{
    public function index($alias){
    	$page = Pages::where('alias', $alias)->first();
    	if($page){
	      $counter = $page->counter;
	    	$page->counter = $counter+1;
	    	$page->save();

	      $cat = CatPages::where('parent_id', 0)->get()->toArray();
	      $list_cate = array();
	      foreach ($cat as $k => $v) {
	          $pages_id = array();
	          $pages_id[] = $cat[$k]['id'];
	          $pages_list = Pages::whereIn('category_id', $pages_id)->orderBy('created_at', 'desc')->take(5)->get();
	          $list_cate[$v['name']] =  array('alias'=>$v['alias'], 'id'=> $v['id'], 'child'=>array('pages' => $pages_list));
	      }
	      $news_count  = Pages::orderBy('counter', 'desc')->take(5)->get();
	      $product_best_sale = Product::where('is_best_sale', 1)->take(5)->get();
	      $agent = new Agent();
	      if($agent->isMobile() || $agent->isTablet()){
	        return view('sp.page', compact('page', 'related_pages', 'list_cate', 'news_count', 'product_best_sale'));
	      }else{
	        return view('page', compact('page', 'related_pages', 'list_cate', 'news_count', 'product_best_sale'));
	      }
	    }else{
	    	return view('errors.404');
	    }
    }
}
