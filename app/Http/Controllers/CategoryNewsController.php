<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CatNews;
use App\News;

use Jenssegers\Agent\Agent;
class CategoryNewsController extends Controller
{
    public function news($alias, $id, Request $request){
    	$input = $request->all();

        $categories_new = CatNews::where('parent_id', 0)->get();
        
        $cat = CatNews::where('id', $id)->where('alias', $alias)->first();
    	$list_news = News::where('category_id', $cat->id)->orderBy('created_at', 'desc')->paginate(15);
        $news_count = News::orderBy('counter', 'desc')->take(5)->get();
        $agent = new Agent();
        if($agent->isMobile() || $agent->isTablet()){
    	   return view('sp.category_news',compact('cat', 'categories_new', 'list_news', 'news_count'));
        }else{
            return view('category_news',compact('cat', 'categories_new', 'list_news' ,'news_count'));
        }
    }
}
