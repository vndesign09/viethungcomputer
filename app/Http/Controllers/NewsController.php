<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\News;
use App\CatNews;
use App\Pages;
use App\CatPages;
use Jenssegers\Agent\Agent;
class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $cat_arr = CatNews::get()->toArray();
      $cat = CatNews::where('parent_id', 0)->get()->toArray();
      function getParent($arrayData, $parent, &$result){
          if (count($arrayData) > 0) {
              foreach ($arrayData as $key => $val) {
                  if ($parent == $val['parent_id']) {
                      $result[] = $val;
                      $_parent = $val['id'];
                      unset($arrayData[$key]);
                      getParent($arrayData, $_parent, $result);
                  }
              }
          }
      }

      $list_cate = array();
      foreach ($cat as $k => $v) {
          // $arr = array();
          // $news_id = array();
          // getParent($cat_arr,$v['id'], $result);
          // foreach ($result as $key => $value) {
          //     if($value['parent_id'] == $v['id']){
          //         $arr[] =  $value;
          //         $news_id[] = $value['id'];
          //     }
          // }
          $news_id = array();
          $news_id[] = $cat[$k]['id'];
          $news = News::whereIn('category_id', $news_id)->orderBy('created_at', 'desc')->take(5)->get();
          $list_cate[$v['name']] =  array('alias'=>$v['alias'], 'id'=> $v['id'], 'child'=>array('news' => $news));
      }

        /*foreach ($list_cate as $key => $value) {
            foreach ($value as $key1 => $value1) {
                var_dump($value1->products[0]->name); die();
            }
        }
        die();*/
        $news_count = News::orderBy('counter', 'desc')->take(5)->get();
        $news_new = News::orderBy('created_at', 'desc')->take(2)->get();
        
        $list_news_sp = News::orderBy('created_at', 'desc')->take(15)->get();
        //-- endpage
        $agent = new Agent();
        if($agent->isMobile() || $agent->isTablet()){
    	   return view('sp.news', compact('list_cate', 'news_new', 'news_count', 'list_news_sp'));
        }else{
            return view('news', compact('list_cate', 'news_new', 'news_count'));
        }

    }
}
