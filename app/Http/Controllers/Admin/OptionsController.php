<?php
 namespace App\Http\Controllers\Admin;

  use App\Http\Requests;
  use Illuminate\Http\Request;
  use App\Http\Controllers\Controller;
  use App\Options;
  use Validator;
class OptionsController extends Controller
{
    public function index(){
     
      $options = Options::lists('value','name');
      $sliders = Options::where('name', 'LIKE' , '%slider%')->lists('value', 'name')->toArray();
      $support  = Options::where('name', 'LIKE' , '%supporter%')->orderBy('name', 'asc')->lists('value', 'name')->toArray();
      sort($sliders);
    	return view('admin.options.index', compact('options', 'sliders', 'support'));
    }
    public function post(Request $request, $action){
    	$input = $request->all();
      if($action == 'settings'){
        Options::where('name','title')->delete();
        Options::where('name','descriptions')->delete();
        Options::where('name','keywords')->delete();
        Options::where('name','customer_care')->delete();
        $settings = Options::insert([
            array('name'=>'title', 'value' => ''.$input['title'].''),
            array('name'=>'descriptions', 'value' => ''.$input['descriptions'].''),
            array('name'=>'keywords', 'value' => ''.$input['keywords'].''),
            array('name'=>'customer_care', 'value' => ''.$input['customer_care'].'')
        ]);
        if($settings){
          return redirect()->back()->with('success', 'Lưu thành công');
        }
      }
      else if($action == 'footer'){
        Options::where('name','footer')->delete();
        $footer = Options::insert([
            array('name'=>'footer', 'value' => ''.$input['footer'].''),
        ]);
        if($footer){
          return redirect()->back()->with('success', 'Lưu thành công');
        }
      }
      else if($action == 'header'){

        if(isset($input['logo']) && $input['logo'] != ''){
          Options::where('name','logo')->delete();
          
          $logo = 'logo.' .$request->file('logo')->getClientOriginalExtension();
          $request->file('logo')->move(
            base_path() . '/images/web/', $logo
          );  
          $ise_logo = Options::insert([
              array('name'=>'logo', 'value' => ''.$logo.''),
          ]);
        }
        if(isset($input['banner']) && $input['banner'] != ''){
          Options::where('name','banner')->delete();
          $banner = 'banner.' .$request->file('banner')->getClientOriginalExtension();
          $request->file('banner')->move(
            base_path() . '/images/web/', $banner
          );
          $ise_banner = Options::insert([
              array('name'=>'banner', 'value' => ''.$banner.''),
          ]);
        }
        return redirect()->back()->with('success', 'Lưu thành công');
      }
      else if($action == 'homepage'){
        $slider = $request->all();        
        if(isset($slider['del']) && count($slider['del']) > 0){
            foreach ($slider['del'] as $k => $v) {
                $del = Options::where('name', $v)->first();
                \File::delete(base_path() . '/images/web/'.$del->value);
                $del->delete();
            }
        }
        foreach ($slider as $key => $value) {
            if($key != '_token' && $key != 'del'){
                Options::where('name',$key)->delete();
                $sl = $key.'.'.$request->file($key)->getClientOriginalExtension();
                $request->file($key)->move(
                  base_path() . '/images/web/', $sl
                );
                $ise_sl = Options::insert([
                    array('name'=>$key, 'value' => ''.$sl.''),
                ]);
            }
          }
          return redirect()->back()->with('success', 'Lưu thành công');
      }
      elseif ($action == 'supports') {
          $supports = $request->all();
          if(count($supports) > 1){
            foreach ($supports as $key => $value) {
              if($key != '_token' && $key != 'del'){
                  Options::where('name',$key)->delete();
                  $info = array('fullname'=>$value[0], 'phone'=>$value[1], 'yahoo'=>$value[2],'skype'=>$value[3]);
                  $insert = Options::insert([
                    array('name'=>$key, 'value' => json_encode($info))
                ]);
              }
            }
          }
          if(isset($supports['del']) && count($supports['del']) > 0){
              Options::whereIn('name',$supports['del'])->delete();
          } 
          return redirect()->back()->with('success', 'Lưu thành công');
      }
    }
}
