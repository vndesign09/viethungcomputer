<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\CatProd;
use App\Product;

class CategoriesProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $cates = CatProd::select('id','name','parent_id')->get()->toArray();
        view()->share('cates', $cates);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('admin.categories.index');
    }

    public function getAdd()
    {
        return view('admin.categories.add');
    }

    public function postAdd(Request $request)
    {
        $input = $request->all();
        $rule = [
            'name' => 'required',
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Tên danh mục',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $input = $request->all();
            if($input['parent_id'] == 0){
                $level = 0;
            }else{
                $parent = CatProd::findOrFail($input['parent_id']);
                $level = $parent->level + 1;
            }
            $cate = new CatProd;
            $cate->name = $input['name'];
            $cate->alias = str_slug($input['name']);
            $cate->code = $input['code'];
            $cate->parent_id = $input['parent_id'];
            $cate->type = 'product';
            $cate->level = $level;
            if($cate->save()){
                $thumbnail = CatProd::find($cate->id);
                // Insert Thumbnai
                $imageName = $thumbnail->id.'-'.$thumbnail->alias . '.' .
                $request->file('thumbnail')->getClientOriginalExtension();
                $thumbnail->thumbnail = $imageName;                
                $request->file('thumbnail')->move(
                    base_path() . '/images/categories/', $imageName
                );
                $thumbnail->save();       
                return redirect()->back()->with('success', 'Lưu thành công');
            }else{
                 return redirect()->back()->withInput()->withErrors('Lưu không thành công');
            }
        }
    } 

    public function getEdit($id){
        $cate = CatProd::findOrFail($id);
        return view('admin.categories.edit', compact('cate'));
    }

    public function postEdit($id, Request $request){
        $input = $request->all();
        if($input['parent_id'] == 0){
            $level = 0;
        }else{
            $parent = CatProd::findOrFail($input['parent_id'])->first();
            $level = $parent->level + 1;
        }    
        $cate = CatProd::findOrFail($id);
        $cate->name = $input['name'];
        $cate->alias = str_slug($input['name']);
        $cate->code = $input['code'];
        $cate->parent_id = $input['parent_id'];
        $cate->level = $level;
        if(isset($input['thumbnail'])){
            \File::delete(base_path() . '/images/categories/'.$cate->thumbnail);
            $imageName = $cate->id.'-'.$cate->alias . '.' .$request->file('thumbnail')->getClientOriginalExtension();
            $request->file('thumbnail')->move(base_path() . '/images/categories/', $imageName);
            $cate->thumbnail = $imageName;
        }
        if($cate->save()){
            return redirect()->back()->with('success', 'Lưu thành công');
        }else{
             return redirect()->back()->withInput()->withErrors('Lưu không thành công');
        }
    }

    public function getDelete($id){
        $cat_arr = CatProd::get()->toArray();
        $cat = CatProd::find($id);
        function getParent($arrData, $parent, &$result){
            if (count($arrData) > 0) {
                foreach ($arrData as $key => $val) {
                    if ($parent == $val['parent_id']) {
                        $result[] = $val;
                        $_parent = $val['id'];
                        unset($arrData[$key]);
                        getParent($arrData, $_parent, $result);
                    }
                }
            }
        }
        getParent($cat_arr, $id, $result);

        $cat_id = $list_child = array();
        if($result != null){
            foreach ($result as $key => $value) {
                $cat_id[] = $value['id'];
            }
        }else{
            $cat_id[] = $id; 
        }
        $del_products = Product::whereIn('category_id', $cat_id)->delete();
        if($cat->delete()){
            return redirect()->back()->with('success', 'Xóa thành công');
        }else{
             return redirect()->back()->withInput()->withErrors('Xóa không thành công');
        }
    }   
}
