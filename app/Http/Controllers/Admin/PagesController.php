<?php
  namespace App\Http\Controllers\Admin;

  use App\Http\Requests;
  use Illuminate\Http\Request;
  use App\Http\Controllers\Controller;
  use App\CatPages;
  use App\Pages;
  use Validator;

  class PagesController extends Controller{

    public function __construct(){
      $cates = CatPages::where('parent_id', '>', '0')->get()->toArray();
      view()->share('cates', $cates);
    }

    public function getIndex(){
      $pages = Pages::all();
      return view('admin.pages.index', compact('pages'));
    }

    //render
    public function getAdd(){
      return view('admin.pages.add');
    }

    public function postAdd(Request $request){
      $input = $request->all();
      $rule = [
        'category_id' => 'required',
        'title' => 'required',
        'contents' => 'required',
      ];

      $messages = [
        'category_id.required' => 'Vui lòng chọn danh mục',
        'title.required' => 'Vui lòng nhập tiêu đề',
        'contents.required' => 'Vui lòng nhập nội dung bài viết',
      ];

      $validator = Validator::make($input, $rule, $messages);
      if($validator->fails()){
        return redirect()->back()->withInput()->withErrors($validator);
      }else{
        $pages = new Pages;
        $pages->category_id = $input['category_id'];
        $pages->title = $input['title'];
        $pages->alias = str_slug($input['title']);
        $pages->contents = $input['contents'];
        if($pages->save()){
            return redirect()->back()->with('success', 'Thêm thành công');
        }
      }

    }

    public function getEdit($id){
      $pages = Pages::find($id);
      return view('admin.pages.edit', compact('pages'));
    }

    public function postEdit($id, Request $request){
      $input = $request->all();
      $pages = Pages::find($id);
      $rule = [
        'category_id' => 'required',
        'title' => 'required',
        'contents' => 'required',
      ];

      $messages = [
        'category_id.required' => 'Vui lòng chọn danh mục',
        'title.required' => 'Vui lòng nhập tiêu đề',
        'contents.required' => 'Vui lòng nhập nội dung bài viết',
      ];

      $validator = Validator::make($input, $rule, $messages);
      if($validator->fails()){
        return redirect()->back()->withInput()->withErrors($validator);
      }else{

        $pages->category_id = $input['category_id'];
        $pages->title = $input['title'];
        $pages->alias = str_slug($input['title']);
        $pages->contents = $input['contents'];
        if($pages->save()){
          return redirect()->back()->with('success', 'Cập nhật Trang thành công');
        }
      }
    }

    public function getDelete($id){
        $delete = Pages::find($id);
        if($delete->delete()){
            return redirect()->back()->with('success', 'Trang đã được xóa thành công');
        }
    }
  }
