<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\CatPages;
use App\Pages;

class CategoriesPagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $cates = CatPages::select('id','name','parent_id')->where('parent_id', '>',0)->get()->toArray();
        return view('admin.categoriespages.index', compact('cates'));
    }

    public function getAdd()
    {
        $cates = CatPages::select('id','name','parent_id')->where('parent_id',0)->get()->toArray();
        return view('admin.categoriespages.add', compact('cates'));
    }

    public function postAdd(Request $request)
    {
        $input = $request->all();
        $rule = [
            'name' => 'required|unique:categories_posts',
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Tên chỉ mục',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $input = $request->all();
            $cate = new CatPages;
            $cate->name = $input['name'];
            $cate->alias = str_slug($input['name']);
            $cate->parent_id = $input['parent_id'];
            if($cate->save()){
                return redirect()->back()->with('success', 'Lưu thành công');
            }else{
                 return redirect()->back()->withInput()->withErrors('Lưu không thành công');
            }
        }
    }

    public function getEdit($id){
        $cates = CatPages::select('id','name','parent_id')->where('parent_id',0)->get()->toArray();
        $cate = CatPages::find($id);
        return view('admin.categoriespages.edit', compact('cate', 'cates'));
    }

    public function postEdit($id, Request $request){
        $input = $request->all();
        $cate = CatPages::find($id);
        $cate->name = $input['name'];
        $cate->alias = str_slug($input['name']);
        $cate->parent_id = $input['parent_id'];
        if($cate->save()){
            return redirect()->back()->with('success', 'Lưu thành công');
        }else{
             return redirect()->back()->withInput()->withErrors('Lưu không thành công');
        }
    }

    public function getDelete($id){
        $delete = CatPages::find($id);
        $delete_page = Pages::where('category_id', $id)->delete();
        if($delete->delete()){
            return redirect()->back()->with('success', 'Trang đã được xóa thành công');
        }
    }
}
