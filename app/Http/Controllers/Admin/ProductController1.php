<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CatProd;
use App\Product;
use App\Brands;
use Validator;

class ProductController extends Controller
{
	public function __construct()
  {
      //$this->middleware('auth');
      $cates = CatProd::select('id','name','parent_id')->get()->toArray();
      $brands = Brands::lists('name', 'id');
      view()->share('cates', $cates);
      view()->share('brands', $brands);
  }

    public function getIndex(){
      $products = Product::where('is_public', 0)->get();
    	return view('admin.products.index', compact('products'));
    }

    public function getAdd(){
    	return view('admin.products.add');
    }

    public function postAdd(Request $request){
    	$input = $request->all();
        
        if(!isset($input['is_best_sale'])){
            $input['is_best_sale'] = 0;
        }
        $rule = [
            'name' => 'required',
            'category_id' => 'required|numeric',
            'code' => 'required|unique:products',
            'price' => 'required|numeric',
            'save_money' => 'numeric',
            'desc_short' => 'required',
            'desc_main' => 'required',
            'thumbnail' => 'required',
        ];
        $messages = [
            'name.required'         => 'Vui lòng nhập tên Sản phẩm',
            'category_id.required'  => 'Vui lòng chọn Danh mục',
            'code.required'         => 'Vui lòng nhập Mã sản phẩm',
            'code.unique'           => 'Mã sản phẩm đã tồn tại, vui lòng nhập Mã khác',
            'price.required'        => 'Vui lòng nhập Giá niêm yết',
            'price.numeric'         => 'Giá niêm yết phải là SỐ (không chứa bất kỳ ký tự nào)',
            'save_money.numeric'    => 'Giảm giá(%) phải là SỐ (không chứa bất kỳ ký tự nào)',
            'desc_short.required'   => 'Vui lòng nhập Mô tả ngắn gọn',
            'desc_main.required'    => 'Vui lòng nhập Mô tả chi tiết',
            'thumbnail.required'    => 'Vui lòng chọn hình ảnh cho Sản phẩm',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
        	$product = new Product;
            $product->name = $input['name'];
            $product->alias = str_slug($input['name']);
            $product->code = $input['code'];
            $product->desc_short = $input['desc_short'];
            $product->desc_main = $input['desc_main'];
            $product->price = $input['price'];
            $product->save_money = $input['save_money'];
            $product->sale = ceil($input['save_money']*100/$input['price']);
            $product->main_price = $input['price']-$input['save_money'];
            $product->category_id = $input['category_id'];
            $product->guarantee_info = $input['guarantee_info'];
            $product->ship_info = $input['ship_info'];
            $product->is_best_sale = $input['is_best_sale'];
            $product->warehouse_info = $input['warehouse_info'];
            $product->brand_id = $input['brand_id'];
            $product->thumbnail = $input['thumbnail'];
            $product->driversoftware_info = $input['driversoftware_info'];
            if($product->save()){
                
                /*// Insert Gallery
                $gallery = array();
                foreach ($input['images'] as $key => $value) {
                    $galleryName = $thumbnail->id.'-'.$thumbnail->alias .'-g'.$key. '.' .
                    $value->getClientOriginalExtension();
                    $value->move(
                        base_path() . '/images/products/', $galleryName
                    );
                    $gallery[] = $galleryName;
                }

                $thumbnail->gallery = json_encode($gallery);
                $thumbnail->save();       */
                return redirect()->back()->with('success', 'Thêm Sản phẩm mới thành công');
            }
        }
    }


    public function getEdit($id){
        $product = Product::where('is_public', 0)->where('id', $id)->first();
        return view('admin.products.edit', compact('product'));
    }



    public function postEdit($id, Request $request){
        $input = $request->all();
        $product = Product::find($id);
        if(!isset($input['is_best_sale'])){
            $input['is_best_sale'] = 0;
        }
        if(!isset($input['gallery'])){
            $input['gallery']= null;
        }
        $rule = [
            'name' => 'required',
            'category_id' => 'required|numeric',
            'code' => 'required|unique:products,id,'.$product->id,'code,'.$product->code,
            'price' => 'required|numeric',
            'save_money' => 'numeric',
            'desc_short' => 'required',
            'desc_main' => 'required',
        ];
        $messages = [
            'name.required'         => 'Vui lòng nhập tên Sản phẩm',
            'category_id.required'  => 'Vui lòng chọn Danh mục',
            'code.required'         => 'Vui lòng nhập Mã sản phẩm',
            'code.unique'           => 'Mã sản phẩm đã tồn tại, vui lòng nhập Mã khác',
            'price.required'        => 'Vui lòng nhập Giá niêm yết',
            'price.numeric'         => 'Giá niêm yết phải là SỐ (không chứa bất kỳ ký tự nào)',
            'save_money.numeric'    => 'Giảm giá(%) phải là SỐ (không chứa bất kỳ ký tự nào)',
            'desc_short.required'   => 'Vui lòng nhập Mô tả ngắn gọn',
            'desc_main.required'    => 'Vui lòng nhập Mô tả chi tiết',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $product->name = $input['name'];
            $product->alias = str_slug($input['name']);
            $product->code = $input['code'];
            $product->desc_short = $input['desc_short'];
            $product->desc_main = $input['desc_main'];
            $product->price = $input['price'];
            $product->save_money = $input['save_money'];
            $product->main_price = $input['price']-$input['save_money'];
            $product->sale = ceil($input['save_money']*100/$input['price']);
            $product->category_id = $input['category_id'];
            $product->guarantee_info = $input['guarantee_info'];
            $product->ship_info = $input['ship_info'];
            $product->is_best_sale = $input['is_best_sale'];
            $product->warehouse_info = $input['warehouse_info'];
            $product->brand_id = $input['brand_id'];
            $product->driversoftware_info = $input['driversoftware_info'];
            // Insert Thumbnai

            if(isset($input['thumbnail'])){

                \File::delete(base_path() . '/images/products/'.$product->thumbnail);
	            $imageName = $product->id.'-'.$product->alias . '.' . 
	            $request->file('thumbnail')->getClientOriginalExtension();
	            $request->file('thumbnail')->move(
	                base_path() . '/images/products/', $imageName
	            );
                $product->thumbnail = $imageName;
        	}
            if(isset($input['images']) && count($input['images'][0]) != null){
                 // Insert Gallery
                $gallery = array();
                foreach ($input['images'] as $key => $value) {
                    $galleryName = str_random(11).".".$value->getClientOriginalExtension();
                    $value->move(
                        base_path() . '/images/products/', $galleryName
                    );
                    $gallery[] = $galleryName;
                }
                if($input['gallery'] != null){
                    $product->gallery = json_encode(array_merge($gallery,$input['gallery']));
                }else{
                    $product->gallery = json_encode($gallery);
                }
            }else{
                $product->gallery = json_encode($input['gallery']);
            }
            if($product->save()){
                return redirect()->back()->with('success', 'Cập nhật Sản phẩm thành công');
            }
        }
    }

    public function getDelete($id){
        $delete = Product::find($id);
        \File::delete(base_path() . '/images/products/'.$delete->thumbnail);
        if($delete->delete()){
            return redirect()->back()->with('success', 'Sản phẩm #'.$id.' đã được xóa thành công');
        }
    }


}
