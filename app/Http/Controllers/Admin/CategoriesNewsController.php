<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\CatNews;
use App\News;

class CategoriesNewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $cates = CatNews::all();
        view()->share('cates', $cates);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('admin.categoriesnews.index');
    }

    public function getAdd()
    {
        return view('admin.categoriesnews.add');
    }

    public function postAdd(Request $request)
    {
        $input = $request->all();
        $rule = [
            'name' => 'required|unique:categories_posts',
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Tên danh mục',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $input = $request->all();
            $cate = new CatNews;
            $cate->name = $input['name'];
            $cate->alias = str_slug($input['name']);
            $cate->parent_id = 0;
            if($cate->save()){
                return redirect()->back()->with('success', 'Lưu thành công');
            }else{
                 return redirect()->back()->withInput()->withErrors('Lưu không thành công');
            }
        }
    }

    public function getEdit($id){
        $cate = CatNews::find($id);
        return view('admin.categoriesnews.edit', compact('cate'));
    }

    public function postEdit($id, Request $request){
        $input = $request->all();
        $cate = CatNews::find($id);
        $cate->name = $input['name'];
        $cate->alias = str_slug($input['name']);
        $cate->parent_id = $input['parent_id'];
        if($cate->save()){
            return redirect()->back()->with('success', 'Lưu thành công');
        }else{
             return redirect()->back()->withInput()->withErrors('Lưu không thành công');
        }
    }

    public function getDelete($id){
        $delete = CatNews::find($id);
        $del_news = News::where('category_id', $id)->delete();
        if($delete->delete()){
            return redirect()->back()->with('success', 'Xóa chuyên mục thành công');
        }else{
            return redirect()->back()->withInput()->withErrors('Xóa không thành công');
        }
    }
}
