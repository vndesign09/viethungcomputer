<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CatProd;
use App\Product;
use App\News;
use App\Pages;
class HomeController extends Controller
{
    public function index(){
    	$categories = CatProd::count();
    	$products = Product::count();
    	$news = News::count();
    	$pages = Pages::count();
    	return view('admin.dashboard', compact('categories', 'products', 'news', 'pages'));
    }
}
