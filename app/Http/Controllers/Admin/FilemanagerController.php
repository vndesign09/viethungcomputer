<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Media;
use App\Folder;
use Validator;

class FilemanagerController extends Controller
{
    public function __construct()
    {
        $folders = Folder::all();
        $media = Media::orderBy('updated_at', 'desc')->paginate(8);
        view()->share('folders', $folders);
        view()->share('media', $media);
    }
    public function getIndex(){
    	return view('filemanager', compact('folders','media'));
    }
     public function getFeaturedImage(){
        return view('featured_image', compact('folders','media'));
    }
    public function getGalleryImages(){
        return view('gallery_images', compact('folders','media'));
    }
    public function postIndex(Request $request){
    	$input = $request->all();
    	//var_dump($input['action']); die();
    	if(isset($input['action']) && $input['action'] =='create_folder'){
    		return $this->createFolder($request);
    	}else if(isset($input['action']) && $input['action'] =='delete_file'){
            return $this->deleteFile($request);
        }else{
    		return $this->insertFile($request);
    	}
    }
    public function createFolder(Request $request){
    		$input = $request->all();
    		$respond['has'] = false;
	    	$rule = [
	    		'folder' => 'required',
	    	];
	    	$messages = [
	    		'folder.required' => 'Vui lòng nhập tên thư mục',
	    	];
	    	$validator = Validator::make($input, $rule, $messages);
	        if($validator->fails()){
	        	$respond['message'] = $validator->getMessageBag()->toJson();
				return \Response::json($respond);
	        }else{
	        	$path = public_path().'/images/'.str_slug($input['folder']);
	        	if(! \File::exists($path)) {
	        		$create = Folder::firstOrCreate([
	        			'name' => $input['folder'],
	        			'alias' => str_slug($input['folder'])
	        		]);
		        	$folder =  \File::makeDirectory($path);
		        	$respond['has'] = true;
		        	return \Response::json(['has'=>true, 'name'=> $input['folder'], 'alias' => str_slug($input['folder']) ]);
	        	}else{
	        		$respond['message'] = 'Thư mục đã tồn tại';
					return \Response::json($respond);
	        	}
	        }
    }
    public function insertFile(Request $request){
    	$input = $request->all(); 	  
        /*echo "<pre>";
                echo $input;
                echo "</pre>";*/
    	if(isset($input['images'])){
            foreach ($input['images'] as $key => $value) {
                $file_name = str_random(11).".".$value->getClientOriginalExtension();
                $file_type = $value->getMimeType();
                $media = new Media;
                if(isset($input['folders']) && $input['folders'] != ''){
                	$value->move(base_path() . '/public/images/'.$input['folders'], $file_name);
                	list($width, $height) = getimagesize(base_path() . '/public/images/'.$input['folders'].'/'.$file_name);
                	$file = $input['folders'].'/'.$file_name;
            	}else{
            		$value->move(base_path() . '/public/images/', $file_name);
            		list($width, $height) = getimagesize(base_path() . '/public/images/'.$file_name);
            		$file = $file_name;
            	}
            	
                $uploaded_images[] = $file;
                $media->name = $file_name;
                $media->file = $file;
                $media->type = $file_type;
                $media->info = json_encode(array('width'=>$width, 'height' => $height));
                $media->save();
            }
            return \Response::json(['has'=>true, 'message' => 'Uploaded', 'images'=> $uploaded_images]);
        }
    }


    public function deleteFile(Request $request){
        $input = $request->all();
        $id = $input['id'];
        $file = Media::find($id);
        \File::delete(base_path() . 'public/images/'.$file->file);
        if($file->delete()){
            return \Response::json(['has' => true, 'id' => $file->id]);
        }
    }
}
