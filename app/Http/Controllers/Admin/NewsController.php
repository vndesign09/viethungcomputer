<?php
  namespace App\Http\Controllers\Admin;

  use App\Http\Requests;
  use Illuminate\Http\Request;
  use App\Http\Controllers\Controller;
  use App\CatNews;
  use App\News;
  use Validator;

  class NewsController extends Controller{

    public function __construct(){
      $cates = CatNews::select('id', 'name', 'parent_id')->get()->toArray();
      view()->share('cates', $cates);
    }

    public function getIndex(){
      $news = News::all();
      return view('admin.news.index', compact('news'));
    }

    //render
    public function getAdd(){
      return view('admin.news.add');
    }

    public function postAdd(Request $request){
      $input = $request->all();
      $rule = [
        'category_id' => 'required',
        'title' => 'required',
        'contents' => 'required',
        'thumbnail' => 'required'
      ];

      $messages = [
        'category_id.required' => 'Vui lòng chọn danh mục',
        'title.required' => 'Vui lòng nhập tiêu đề',
        'contents.required' => 'Vui lòng nhập nội dung bài viết',
        'thumbnail.required' => 'Vui lòng chọn hình ảnh cho sản phẩm',
        'thumbnail.image' => 'Hình ảnh cho sản phẩm chỉ được chấp nhận các loại jpeg, png, bmp, gif, or svg'
      ];

      $validator = Validator::make($input, $rule, $messages);
      if($validator->fails()){
        return redirect()->back()->withInput()->withErrors($validator);
      }else{
        $news = new News;
        $news->category_id = $input['category_id'];
        $news->title = $input['title'];
        $news->alias = str_slug($input['title']);
        $news->desc = $input['desc'];
        $news->contents = $input['contents'];
        $news->desc = $input['title'];
        if($news->save()){
            $thumbnail = News::find($news->id);
            // Insert Thumbnai
            $thumbnail->thumbnail = ''.$thumbnail->id.'-'.$thumbnail->alias . '.' . $request->file('thumbnail')->getClientOriginalExtension().'';
            $thumbnail->save();

            $imageName = $thumbnail->id.'-'.$thumbnail->alias . '.' .
            $request->file('thumbnail')->getClientOriginalExtension();
            $request->file('thumbnail')->move(
                base_path() . '/images/news/', $imageName
            );

            return redirect()->back()->with('success', 'Thêm thành công');
        }
      }

    }

    public function getEdit($id){
      $news = News::find($id);
      return view('admin.news.edit', compact('news'));
    }

    public function postEdit($id, Request $request){
      $input = $request->all();
      $news = News::find($id);
      $rule = [
        'category_id' => 'required',
        'title' => 'required',
        'contents' => 'required',
      ];

      $messages = [
        'category_id.required' => 'Vui lòng chọn danh mục',
        'title.required' => 'Vui lòng nhập tiêu đề',
        'contents.required' => 'Vui lòng nhập nội dung bài viết',
      ];

      $validator = Validator::make($input, $rule, $messages);
      if($validator->fails()){
        return redirect()->back()->withInput()->withErrors($validator);
      }else{

        $news->category_id = $input['category_id'];
        $news->title = $input['title'];
        $news->alias = str_slug($input['title']);
        $news->desc = $input['desc'];
        $news->contents = $input['contents'];
        if(isset($input['thumbnail'])){
          \File::delete(base_path() . '/images/news/', $news->thumbnail);
          $imageName = $news->id.'-'.$news->alias . '.' .
          $request->file('thumbnail')->getClientOriginalExtension();

          $request->file('thumbnail')->move(
              base_path() . '/images/news/', $imageName
            );
          $news->thumbnail = $imageName;
        }
        if($news->save()){
          return redirect()->back()->with('success', 'Cập nhật bài viết thành công');
        }
      }
    }

    public function getDelete($id){
        $delete = News::find($id);
        if($delete->delete()){
            return redirect()->back()->with('success', 'Bài viết đã được xóa thành công');
        }
    }
  }
