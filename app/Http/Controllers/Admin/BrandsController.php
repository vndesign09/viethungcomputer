<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Brands;

class BrandsController extends Controller
{
    public function getIndex(){
    	$brands = Brands::all();
    	return view('admin.brands.index', compact('brands'));
    }
    public function getAdd(){
    	return view('admin.brands.add');
    }
    public function postAdd(Request $request)
    {
        $input = $request->all();
        $rule = [
            'name' => 'required|unique:brands',
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Tên hãng sản xuất',
            'name.unique' => 'Tên hãng sản xuất đã tồn tại',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
        	$brand = new Brands;
        	$brand->name = $input['name'];
        	if($brand->save()){
                return redirect()->back()->with('success', 'Lưu thành công');
            }else{
                 return redirect()->back()->withInput()->withErrors('Lưu không thành công');
            }
        }

    }
    public function getEdit($id){
    	$brand = Brands::findOrFail($id);
    	return view('admin.brands.edit', compact('brand'));
    }
    public function postEdit($id, Request $request){
    	$input = $request->all();
        $rule = [
            'name' => 'required|unique:brands,name,'.$id
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Tên hãng sản xuất',
            'name.unique' => 'Tên hãng sản xuất đã tồn tại',
        ];
        $validator = Validator::make($input, $rule, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
        	$brand = Brands::findOrFail($id);
        	$brand->name = $input['name'];
        	if($brand->save()){
                return redirect()->back()->with('success', 'Lưu thành công');
            }else{
                 return redirect()->back()->withInput()->withErrors('Lưu không thành công');
            }
        }
    }
    public function getDelete($id){
    	$del_brand = Brands::findOrFail($id);
        if($del_brand->delete()){
            return redirect()->back()->with('success', 'Xóa thành công');
        }else{
             return redirect()->back()->withInput()->withErrors('Xóa không thành công');
        }
    }
}
