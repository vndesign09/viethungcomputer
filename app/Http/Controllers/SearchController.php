<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CatProd;
use App\Product;
use Jenssegers\Agent\Agent;
class SearchController extends Controller
{
    public function search(Request $request){
    	$input = $request->all();

    	// kiểm tra SORT
    	if(isset($input['sort'])){
    		$sort = $input['sort']; 	
    		$sort_m = 'desc';
    		switch ($sort) {
    			case 'price-asc':
    				$sort = 'main_price';
    				$sort_m = 'asc';
    				break;
    			case 'price-desc':
    				$sort = 'main_price';
    				break;
    			case 'new':
    				$sort = 'created_at';
    				break;
    			case 'view':
    				$sort = 'counter';
    				break;
    		}
    	}else{
    		$sort = 'created_at';
    		$sort_m = 'desc';
    	}

    	// Kiểm tra tồn tại Keyword
    	if(isset($input['k'])) {
    		$keyword = $input['k'];
    	}else{
    		$keyword = '';
    	}

        if(isset($input['min'])) {
            $min = $input['min'];
        }else{
            $min = 0;
        }


        if(isset($input['cat'])){
            $cat_s = $input['cat'];
            $cat_arr = CatProd::get()->toArray();
            $cat = CatProd::where('id', $cat_s)->first();
            function getParent($arrData, $parent, &$result){
                if (count($arrData) > 0) {
                    foreach ($arrData as $key => $val) {
                        if ($parent == $val['parent_id']) {
                            $result[] = $val;
                            $_parent = $val['id'];
                            unset($arrData[$key]);
                            getParent($arrData, $_parent, $result);
                        }
                    }
                }
            }
            getParent($cat_arr, $cat_s, $result);
            $cat_id = $list_child =  array();
            if($result != null){
                    foreach ($result as $key => $value) {
                        $cat_id[] = $value['id'];
                    }
                    $list_child = $result;
            
                if(isset($cat_s) && $cat_s == 0){
                    $cat_arr = CatProd::whereIn('level', [0,1])->get();
                    foreach ($cat_arr as $key => $value) {
                        $cat_id[] = $value->id;
                    }
                    $list_child = CatProd::get()->toArray();
                }
            }else{
                $cat_id[] = $cat_s;
                $list_child = null;
            }
        }else{
           $cat_s = 0;
        }

        if(isset($input['max'])){
            $max = $input['max'];
        }
        // Kiểm tra tồn tại Category ID
        if(isset($input['cat']) && !isset($input['max']) ){
            $list_products = Product::whereIn('category_id', $cat_id)->where('name', 'LIKE', "%$keyword%")->where('main_price', '>=', $min)->orderBy($sort, $sort_m)->paginate(30);
        }
        else if(isset($input['cat']) && isset($input['max'])){
            $list_products = Product::whereBetween('main_price', [$min, $input['max']])->whereIn('category_id', $cat_id)->orderBy($sort, $sort_m)->paginate(30);
        }
        else{
            $list_products = Product::where('name', 'LIKE',"%$keyword%")->orderBy($sort, $sort_m)->paginate(30);
        }

        $agent = new Agent();
        if($agent->isMobile() || $agent->isTablet()){
    	   return view('sp.result_search', compact('list_products', 'keyword','cat', 'list_child', 'cat_arr', 'cat_s'));
        }else{
            return view('result_search', compact('list_products', 'keyword','cat', 'list_child', 'cat_arr', 'cat_s', 'min', 'max'));
        }
    }
}
