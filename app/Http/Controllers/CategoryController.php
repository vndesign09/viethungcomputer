<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CatProd;
use App\Product;
use Jenssegers\Agent\Agent;
class CategoryController extends Controller
{
    public function product($alias, $id, Request $request){
    	$input = $request->all();
    	if(isset($input['sort'])){
    		$sort = $input['sort']; 	
    		$sort_m = 'desc';
    		switch ($sort) {
    			case 'price-asc':
    				$sort = 'main_price';
    				$sort_m = 'asc';
    				break;
    			case 'price-desc':
    				$sort = 'main_price';
    				break;
    			case 'new':
    				$sort = 'created_at';
    				break;
    			case 'view':
    				$sort = 'counter';
    				break;
    		}
    	}else{
    		$sort = 'created_at';
    		$sort_m = 'desc';
    	}

        $cat_arr = CatProd::get()->toArray();
        $cat = CatProd::where('id', $id)->where('alias', $alias)->first();
        function getParent($arrData, $parent, &$result){
            if (count($arrData) > 0) {
                foreach ($arrData as $key => $val) {
                    if ($parent == $val['parent_id']) {
                        $result[] = $val;
                        $_parent = $val['id'];
                        unset($arrData[$key]);
                        getParent($arrData, $_parent, $result);
                    }
                }
            }
        }
        getParent($cat_arr, $id, $result);

        $cat_id = $list_child = array();
        if($result != null){
            foreach ($result as $key => $value) {
                $cat_id[] = $value['id'];
            }
            foreach ($result as $key => $value) {
                if($value['level'] == $cat->level+1){
                    $list_child[] = $value;
                }
            }
        }else{
            $cat_id[] = $id;
            $list_child = CatProd::where('parent_id', $cat->parent_id)->get();
        }
    	$list_products = Product::whereIn('category_id', $cat_id)->orderBy($sort, $sort_m)->paginate(30);
        $agent = new Agent();
        if($agent->isMobile() || $agent->isTablet()){
    	   return view('sp.category_product',compact('cat', 'list_products', 'list_child'));
        }else{
            return view('category_product',compact('cat', 'list_products', 'list_child'));
        }
    }
}
