<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


// Admin Page
//Route::group(['prefix' => 'admin'], function(){
	Route::group(['middleware' => 'web'], function () {

    	Route::group(['middleware' => 'auth', 'namespace'=>'Admin'], function () {
    		Route::get('/dashboard', 'CategoriesProductsController@index');
    	});

		Route::group(['prefix'=>'admin','namespace'=>'Admin'], function(){
			// Route Category
			Route::controller('category','CategoriesProductsController', array(
				'getIndex' => 'cat_pro.index',
				'getAdd' => 'cat_pro.add',
				'postAdd' => 'cat_pro.add_post',
				'getEdit' => 'cat_pro.edit',
				'postEdit' => 'cat_pro.edit_post',
				'getDelete' => 'cat_pro.delete',
			));

			// Route Products
			Route::controller('product','ProductController', array(
				'getIndex' => 'prod.index',
				'getAdd' => 'prod.add',
				'postAdd' => 'prod.add_post',
				'getEdit' => 'prod.edit',
				'postEdit' => 'prod.edit_post',
				'getDelete' => 'prod.delete',
			));

      //Route News
      Route::controller('news', 'NewsController', array(
        'getIndex' => 'news.index',
        'getAdd' => 'news.add',
        'postAdd' => 'news.add_post',
        'getEdit' => 'news.edit',
        'postEdit' => 'news.edit_post',
        'getDelete' => 'news.delete'
      ));
		});
	});
//});
Route::get('/{alias}/{id}', [
    'as' => 'details.product', 'uses' => 'DetailsController@index'
]);

Route::get('/search', [
    'as' => 'search', 'uses' => 'SearchController@search'
]);

Route::get('/c/{alias}/{id}', [
    'as' => 'category.product', 'uses' => 'CategoryController@product'
]);