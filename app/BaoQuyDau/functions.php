<?php


function getCategories($data,$parent= 0,$space="",$select=0){
	foreach ($data as $key => $value) {
		$id = $value['id'];
		$name = $value['name'];
		if($value['parent_id'] == $parent){
			if($select != 0 && $id == $select){
				echo "<option value='$id' selected='selected'>$space $name</option>";
			}else{
				echo "<option value='$id'>$space $name</option>";
			}
			getCategories($data,$id, $space."------", $select);
		}
	}
}
function getParentthong($arrData, $parent, $space, &$list_cats){
            if (count($arrData) > 0) {
                foreach ($arrData as $key => $val) {
                    if ($parent == $val['parent_id']) {
                        $list_cats[$val['id']] = $space.$val['name'];
                        $_parent = $val['id'];
                        unset($arrData[$key]);
                        getParentthong($arrData, $_parent, $space."----", $list_cats);
                    }
                }
            }
        }
function getMenu($menus, $id_parent = 0) 
{
    # BƯỚC 1: LỌC DANH SÁCH MENU VÀ CHỌN RA NHỮNG MENU CÓ ID_PARENT = $id_parent
     
    // Biến lưu menu lặp ở bước đệ quy này
    $menu_tmp = array();
 
    foreach ($menus as $key => $item) {
        // Nếu có parent_id bằng với parrent id hiện tại
        if ((int) $item['parent_id'] == (int) $id_parent) {
            $menu_tmp[] = $item;
            // Sau khi thêm vào biên lưu trữ menu ở bước lặp
            // thì unset nó ra khỏi danh sách menu ở các bước tiếp theo
            unset($menus[$key]);
        }
    }
 
    # BƯỚC 2: lẶP MENU THEO DANH SÁCH MENU Ở BƯỚC 1
     
    // Điều kiện dừng của đệ quy là cho tới khi menu không còn nữa
    if ($menu_tmp)  
    {
        echo '<ul>';
        foreach ($menu_tmp as $item) 
        {
        	if($item['thumbnail'] != ''){
        		echo '<li style="background-image: url(/images/categories/' . $item['thumbnail'] . '); background-size:10%;">';	
        	}else{
        		echo '<li style="background-image: url(/images/categories/default.png); background-size:10%;">';
        	}
            
            echo '<a href="/danh-muc/' . $item['alias'] . '/' .$item['id'] . '">' . $item['name'] . '</a>';
            // Gọi lại đệ quy
            // Truyền vào danh sách menu chưa lặp và id parent của menu hiện tại
            getMenu($menus, $item['id']);
            echo '</li>';
        }
        echo '</ul>';
    }
}

function getCategoriesTable($data,$parent = 0, $space=""){
	foreach ($data as $key => $value) {
		$id = $value['id'];
		$name = $value['name'];
		if($value['parent_id'] == $parent){
			echo "<tr><td>$id</td><td>$space $name</td><td><a href='category/edit/".$id."' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  <a href='category/delete/".$id."'' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td></tr>";
			getCategoriesTable($data,$id, $space."------");
		}
	}
}


function getCategoriesNewsTable($data,$parent = 0, $space=""){
	foreach ($data as $key => $value) {
		$id = $value['id'];
		$name = $value['name'];
		if($value['parent_id'] == $parent){
			echo "<tr><td>$id</td><td>$space $name</td><td><a href='categorynews/edit/".$id."' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  <a href='' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td></tr>";
			getCategoriesTable($data,$id, $space."------");
		}
	}
}


function getCategoriesPagesTable($data,$parent = 0, $space=""){
	foreach ($data as $key => $value) {
		$id = $value['id'];
		$name = $value['name'];
		if($value['parent_id'] == $parent){
			echo "<tr><td>$id</td><td>$space $name</td><td><a href='categorypages/edit/".$id."' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>  <a href='' class='btn btn-danger btn-xs'><i class='glyphicon glyphicon-trash'></i></a></td></tr>";
			getCategoriesTable($data,$id, $space."------");
		}
	}
}


function adddotNumber($strNum) {

        $len = strlen($strNum);
        $counter = 3;
        $result = "";
        while ($len - $counter >= 0)
        {
            $con = substr($strNum, $len - $counter , 3);
            $result = '.'.$con.$result;
            $counter+= 3;
        }
        $con = substr($strNum, 0 , 3 - ($counter - $len) );
        $result = $con.$result;
        if(substr($result,0,1)=='.'){
            $result=substr($result,1,$len+1);
        }
        return $result;
}

function getMultiCategory($data, $idParent = 0){

}


?>
