<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatProd extends Model
{
    protected $table = 'categories_products';
    protected $fillable = ['name', 'code', 'alias', 'parent_id', 'type', 'level'];

    public function products()
    {
        return $this->hasMany('App\Product', 'category_id');
    }

    public function category(){
    	return $this->hasMany('App\CatProd', 'parent_id');
    }
}
