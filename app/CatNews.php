<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatNews extends Model
{
    protected $table = 'categories_posts';
    protected $fillable = ['name', 'alias', 'parent_id'];

    public function pages()
    {
        return $this->hasMany('App\Pages', 'category_id');
    }

    public function category(){
    	return $this->hasMany('App\CatPages', 'parent_id');
    }
}
