<?php
  namespace App;
  use Illuminate\Database\Eloquent\Model;
  class News extends Model{
    protected $table = 'posts';
    protected $fillable = ["title", "alias", "desc", "contents", "thumbnail", "category_id"];

    public function category(){
    	return $this->belongsTo('App\CatNews', 'category_id');
    }
  }
