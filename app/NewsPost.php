<?php
  namespace App;

  use App\Illuminate\Eloquent\Model;

  class NewsPost extends Model{
    protected $table = 'categories_posts';
    protected $fillable = ['name', 'alias', 'parent_id'];
  }
