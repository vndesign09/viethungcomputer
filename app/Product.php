<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name,alias,code,desc_short,desc_main,price,sale,category_id,thumbnail,guarantee_info,ship_info,warehouse_info,driversoftware_info,counter','brand_id'];

    public function category(){
    	return $this->belongsTo('App\CatProd', 'category_id');
    }

    public function brand(){
    	return $this->belongsTo('App\Brands', 'brand_id');
    }
}
