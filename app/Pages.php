<?php
  namespace App;
  use Illuminate\Database\Eloquent\Model;
  class Pages extends Model{
    protected $table = 'pages';
    protected $fillable = ["title", "alias", "desc", "contents", "thumbnail", "category_id", "counter"];

    public function category(){
    	return $this->belongsTo('App\CatPages', 'category_id');
    }
  }
